// UART routines
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#ifndef _UART_H
#define _UART_H

void uart_enable(uint8_t un, uint32_t baud);
void uart_disable(uint8_t un);

void uart_interrupt_enable(uint8_t un);
void uart_interrupt_disable(uint8_t un);

uint8_t uart_read(uint8_t un, volatile uint8_t *byte);
uint8_t uart_read_wait(uint8_t un, volatile uint8_t *byte);
uint8_t uart_read_ready(uint8_t un);
void uart_read_pkt(uint8_t un, struct bl_pkt *pkt);

void uart_write(uint8_t un, uint8_t byte);
void uart_write_pkt(uint8_t un, struct bl_pkt *pkt, uint8_t *data);
void uart_write_string(uint8_t un, char *s);
void uart_write_string_P(uint8_t un, const char *s);
void uart_write_binchar(uint8_t un, uint8_t byte);
void uart_write_binchar_inv(uint8_t un, uint8_t byte);
void uart_write_binval(uint8_t un, uint8_t *byte, int length);

#endif // _UART_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
