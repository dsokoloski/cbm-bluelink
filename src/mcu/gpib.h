// GPIB routines
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#ifndef _GPIB_H
#define _GPIB_H

#define GPIB_ACG			0x00
#define GPIB_UCG			0x10
#define GPIB_LAG_L			0x20
#define GPIB_LAG_H			0x30
#define GPIB_TAG_L			0x40
#define GPIB_TAG_H			0x50
#define GPIB_SCG_L			0x60
#define GPIB_SCG_H			0x70
#define GPIB_SCG_CLOSE		0xE0
#define GPIB_SCG_OPEN_SAVE	0xF0

#define GPIB_ACG_GTL		0x01
#define GPIB_ACG_SDC		0x04
#define GPIB_ACG_PPC		0x05
#define GPIB_ACG_GET		0x08
#define GPIB_ACG_TCT		0x09

#define GPIB_UCG_LLO		0x01
#define GPIB_UCG_DCL		0x04
#define GPIB_UCG_PPU		0x05
#define GPIB_UCG_SPE		0x08
#define GPIB_UCG_SPD		0x09

#define GPIB_LAG_UNL		0x0F
#define GPIB_TAG_UNT		0x0F

#define GPIB_GROUP(x)       (x & 0xF0)
#define GPIB_ADDR(x)        (x & 0x0F)

// Device structure
typedef struct gpib_device_t
{
    uint8_t disk:1;
    uint8_t internal:1;
    uint8_t registered:1;
    uint8_t uart:1;
    char filename[BL_GPIB_FILELEN + 1];
} gpib_device;

extern gpib_device device[BL_GPIB_DEVICES];

void gpib_init(void);
void gpib_talk_enable(uint8_t enable);
uint8_t gpib_read(uint8_t *data);
uint8_t gpib_write(uint8_t data);
void gpib_event(void);
void gpib_talk(uint8_t paddr);
void gpib_talk_internal(uint8_t id, uint8_t saddr);
void gpib_talk_external(uint8_t id, uint8_t saddr);
void gpib_listen(uint8_t paddr);
uint8_t gpib_validate_device_id(uint8_t id);

#ifdef BL_DEBUG
void gpib_print_ctrl(uint8_t ctrl);
void gpib_print_data(uint8_t data);
#endif

#endif // _GPIB_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
