// IEEE-488 to Bluetooth Adapter firmware
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h> 
#include <avr/sleep.h>
#include <avr/wdt.h>

#include <cbm-bluelink.h>

#include "gpib.h"
#include "ports.h"
#include "state.h"
#include "strings.h"
#include "uart.h"
#include "util.h"

// UART0 CTS interrupt
ISR(PCINT0_vect)
{
    state.uart0_cts = (UART0_CTS_PIN & _BV(UART0_CTS_BIT)) ? 1 : 0;
}

// Reset to programming mode?
ISR(PCINT1_vect)
{
    state.program = (PROG_PIN & _BV(PROG_BIT)) ? 1 : 0;
}

// Control port interrupt
ISR(PCINT2_vect) 
{
    state.ctrl = CTRL_PIN;
}

// Bluetooth status change and CTS interrupt
ISR(PCINT3_vect)
{
    state.uart1_cts = (UART1_CTS_PIN & _BV(UART1_CTS_BIT)) ? 1 : 0;
    uint8_t value = (BTS_PIN & _BV(BTS_BIT)) ? 1 : 0;
    state.bluetooth_changed = (value != state.bluetooth) ? 1 : 0;
    state.bluetooth = value;
}

// UART0 RX interrupt
ISR(USART0_RX_vect)
{
    uint8_t *ptr = (uint8_t *)&state.uart0_pkt;
    *(ptr + state.uart0_data_length) = UDR0;
    if (state.uart0_data_length == 0 &&
        !(state.uart0_pkt.op & BLOP_ID_MASK)) return;
    if (++state.uart0_data_length == sizeof(struct bl_pkt))
        UART0_RTS_PORT |= _BV(UART0_RTS_BIT);
}

// UART1 RX interrupt
ISR(USART1_RX_vect)
{
    uint8_t *ptr = (uint8_t *)&state.uart1_pkt;
    *(ptr + state.uart1_data_length) = UDR1;
    if (state.uart1_data_length == 0 &&
        !(state.uart1_pkt.op & BLOP_ID_MASK)) return;
    if (++state.uart1_data_length == sizeof(struct bl_pkt))
        UART1_RTS_PORT |= _BV(UART1_RTS_BIT);
}

// Function declarations
static void process_packet(uint8_t un);

// Hello, world!
int main(void)
{
    // Clear MCU status register
    MCUSR = 0;

    // Disable global interrupts
    cli();

    // Ensure clock prescaler is off
    CLKPR = _BV(CLKPCE), CLKPR = 0;

    // Power down analog comparator.
    ACSR |= ACD;

    // Enable output on LED1 pin and turn it off
    LED1_DDR |= _BV(LED1_BIT);
    LED1_PORT &= ~_BV(LED1_BIT);

    // Enable output on LED1 pin and turn it off
    LED2_DDR |= _BV(LED2_BIT);
    LED2_PORT &= ~_BV(LED2_BIT);

    // Enable input on bluetooth status pin
    BTS_DDR &= ~_BV(BTS_BIT);

    // Enable input on programming reset pin
    PROG_DDR &= ~_BV(PROG_BIT);

    // Set UART0 baud and bits for debug output
    uart_enable(0, UART0_BAUD);
    uart_interrupt_enable(0);

    // Set UART1 baud and bits for bluetooth I/O
    uart_enable(1, UART1_BAUD);
    uart_interrupt_enable(1);

    // Initialize global state structure
    state_init();

    // Initialize GPIB ports
    gpib_init();

    // Enable pin-change interrupt for control port
    PCMSK2 =
        _BV(CTRL_REN_INT) | _BV(CTRL_IFC_INT) | _BV(CTRL_NDAC_INT) |
        _BV(CTRL_NRFD_INT) | _BV(CTRL_DAV_INT) | _BV(CTRL_EOI_INT) |
        _BV(CTRL_ATN_INT) | _BV(CTRL_SRQ_INT);
    PCICR |= _BV(PCIE2);

    // Enable pin-change interrupt for bluetooth status pin,
    // and UART1 RTS/CTS
    PCMSK3 = _BV(BTS_INT) | _BV(UART1_CTS_INT);
    PCICR |= _BV(PCIE3);

    // Enable pin-change interrupt for programming reset pin
    PCMSK1 = _BV(PROG_INT);
    PCICR |= _BV(PCIE1);

    // Set sleep mode
    set_sleep_mode(SLEEP_MODE_IDLE);

    // Application loop...
    for ( ;; ) {
        LED1_PORT &= ~_BV(LED1_BIT);
        LED2_PORT &= ~_BV(LED2_BIT);

        sleep_enable();

        sei();

        // Zzzz...
        sleep_cpu();
        sleep_disable();
        // Wake-up!

        cli();

        LED1_PORT |= _BV(LED1_BIT);
        LED2_PORT |= _BV(LED2_BIT);

        if (state.ctrl) {
            // Raise RTS lines.
            UART0_RTS_PORT |= _BV(UART0_RTS_BIT);
            UART1_RTS_PORT |= _BV(UART1_RTS_BIT);

            gpib_event();
            gpib_talk_enable(0);

            state.ctrl = 0;

            // Lower RTS lines.
            if (state.uart0_data_length != sizeof(struct bl_pkt))
                UART0_RTS_PORT &= ~_BV(UART0_RTS_BIT);
            if (state.uart1_data_length != sizeof(struct bl_pkt))
                UART1_RTS_PORT &= ~_BV(UART1_RTS_BIT);
        }

        if (state.bluetooth_changed) {
            state.uart1_data_length = 0;
            state.bluetooth_changed = 0;
        }

        if (state.uart0_data_length == sizeof(struct bl_pkt))
            process_packet(0);

        if (state.uart1_data_length == sizeof(struct bl_pkt))
            process_packet(1);

        // Reset via watchdog timer.
	    if (!state.program) reset();
    }

    return 0;
}

static void process_packet(uint8_t un)
{
    struct bl_pkt *pkt;

    switch (un) {
    case 0:
        pkt = &state.uart0_pkt;
        break;

    case 1:
        pkt = &state.uart1_pkt;
        break;

    default:
        return;
    }

    switch ((pkt->op &
        ~(BLOP_ID_MASK | BLOP_FLAG_EOF | BLOP_FLAG_RES1))) {
    case BLOP_VERSION:
        pkt->paddr = FIRMWARE_VERSION;
        pkt->saddr = HARDWARE_VERSION;
        pkt->length = 0;
        uart_write_pkt(un, pkt, NULL);
        break;

    case BLOP_STATUS:
        pkt->paddr = 0;
        pkt->saddr = 0;
        pkt->length = 0;
        if (state.controller)
            pkt->paddr |= BL_STATUS_CTRL;
        uart_write_pkt(un, pkt, NULL);
        break;

    case BLOP_REGISTER:
        if (pkt->paddr < 4 || pkt->paddr > BL_GPIB_DEVICES - 2) {
            pkt->op = (BLOP_ERROR | BLOP_ID_MASK);
            pkt->paddr = BLERR_INV_DEVID;
            pkt->saddr = 0;
            pkt->length = 0;
            uart_write_pkt(un, pkt, NULL);
        }
        device[pkt->paddr].uart = un;
        device[pkt->paddr].registered = 1;
        break;

    case BLOP_DEBUG:
        if (pkt->paddr) {
            if (state.debug_enable) break;
            state.debug_enable = 1;
            state.debug_uart = un;
            debug_P(TRUE, _T(DEBUG_ENABLED));
        }
        else {
            if (!state.debug_enable) break;
            else if (state.debug_uart != un) break;
            state.debug_enable = 0;
        }
        break;

    default:
        pkt->paddr = BLERR_INV_OP;
        pkt->saddr = pkt->op;
        pkt->length = 0;

        pkt->op = (BLOP_ERROR | BLOP_ID_MASK);
        uart_write_pkt(un, pkt, NULL);
    }

    switch (un) {
    case 0:
        state.uart0_data_length = 0;
        UART0_RTS_PORT &= ~_BV(UART0_RTS_BIT);
        break;
    case 1:
        state.uart1_data_length = 0;
        UART1_RTS_PORT &= ~_BV(UART1_RTS_BIT);
        break;
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
