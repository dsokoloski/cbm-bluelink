// GPIB routines
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#include <stdint.h>
#include <stdlib.h>

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include <cbm-bluelink.h>

#include "gpib.h"
#include "ports.h"
#include "state.h"
#include "strings.h"
#include "uart.h"
#include "util.h"

#include "testprg.h"
#include "hello.h"

// Global device structure
gpib_device device[BL_GPIB_DEVICES];

// [rinfdeas]: Control bits
//
// r: REN   Remote enable.
// i: IFC   Interface clear.
// n: NDAC  Not data accepted.
// f: NRFD  Not ready for data.
// d: DAV   Data valid.
// e: EOI   End-or-identify.
// a: ATN   Attention.
// s: SRQ   Service request.

void gpib_init()
{
    CTRL_DC_DDR |= _BV(CTRL_DC_BIT);
    CTRL_TE_DDR |= _BV(CTRL_TE_BIT);
    DATA_PE_DDR |= _BV(DATA_PE_BIT);
    DATA_TE_DDR |= _BV(DATA_TE_BIT);

    gpib_talk_enable(0);

    // Check if a controller is present
    _delay_us(100);
    state.ctrl = CTRL_PIN;
    state.controller = (state.ctrl & _BV(CTRL_REN_BIT)) ? 0 : 1;

    // Initialize device flags
    for (uint8_t i = 0; i < BL_GPIB_DEVICES; i++) {
        device[i].uart = 0;
        device[i].disk = 0;
        device[i].internal = 0;
        device[i].registered = 0;
        device[i].filename[0] = '\0';
    }

    // Internal devices
    device[10].disk = 1;
    device[10].internal = 1;
    device[10].registered = 1;
}

void gpib_talk_enable(uint8_t enable)
{
    if (enable) {
        CTRL_DC_PORT &= ~_BV(CTRL_DC_BIT);
        CTRL_TE_PORT |= _BV(CTRL_TE_BIT);

        DATA_PE_PORT |= _BV(DATA_PE_BIT);
        DATA_TE_PORT |= _BV(DATA_TE_BIT);

        CTRL_DDR = 0;

        CTRL_DDR |= _BV(CTRL_ATN_BIT);
        CTRL_PORT |= _BV(CTRL_ATN_BIT);
        CTRL_DDR |= _BV(CTRL_DAV_BIT);
        CTRL_PORT |= _BV(CTRL_DAV_BIT);
        CTRL_DDR |= _BV(CTRL_EOI_BIT);
        CTRL_PORT |= _BV(CTRL_EOI_BIT);
        CTRL_DDR |= _BV(CTRL_IFC_BIT);
        CTRL_PORT |= _BV(CTRL_IFC_BIT);
        CTRL_DDR |= _BV(CTRL_REN_BIT);
        CTRL_PORT &= ~_BV(CTRL_REN_BIT);

        DATA_DDR = 0xFF;
    }
    else {
        CTRL_DC_PORT |= _BV(CTRL_DC_BIT);
        CTRL_TE_PORT &= ~_BV(CTRL_TE_BIT);

        DATA_PE_PORT |= _BV(DATA_PE_BIT);
        DATA_TE_PORT &= ~_BV(DATA_TE_BIT);

        CTRL_DDR = 0;

        CTRL_DDR |= _BV(CTRL_NDAC_BIT);
        CTRL_PORT &= ~_BV(CTRL_NDAC_BIT);
        CTRL_DDR |= _BV(CTRL_NRFD_BIT);
        CTRL_PORT |= _BV(CTRL_NRFD_BIT);
        CTRL_DDR |= _BV(CTRL_SRQ_BIT);
        CTRL_PORT |= _BV(CTRL_SRQ_BIT);

        DATA_DDR = 0;
    }
}

uint8_t gpib_read(uint8_t *data)
{
    uint8_t retry = 0;

    LED1_PORT ^= _BV(LED1_BIT);

    // Set NDAC low, NRFD high
    CTRL_PORT &= ~_BV(CTRL_NDAC_BIT);
    CTRL_PORT |= _BV(CTRL_NRFD_BIT);

    // Wait for talker to pull DAV low
    for (retry = 0; retry < 255; retry++) {
        state.ctrl = CTRL_PIN;
        if (!(state.ctrl & _BV(CTRL_DAV_BIT))) break;
        _delay_us(10);
    }

    if (retry == 255) {
        debug_P(TRUE, _T(TIMEOUT_DAV_LOW));
        return 1;
    }

    // Set NRFD low
    CTRL_PORT &= ~_BV(CTRL_NRFD_BIT);
    // Read data
    *data = ~reverse_bits(DATA_PIN);
    // Set NDAC high
    CTRL_PORT |= _BV(CTRL_NDAC_BIT);

    // Wait for talker to drive DAV high
    for (retry = 0; retry < 255; retry++) {
        state.ctrl = CTRL_PIN;
        if (state.ctrl & _BV(CTRL_DAV_BIT)) break;
        _delay_us(10);
    }

    if (retry == 255) {
        debug_P(TRUE, _T(TIMEOUT_DAV_HIGH));
        return 1;
    }

    // Set NDAC low
    CTRL_PORT &= ~_BV(CTRL_NDAC_BIT);
    // Set NRFD high
    CTRL_PORT |= _BV(CTRL_NRFD_BIT);
    // Update ctrl register
    state.ctrl = CTRL_PIN;

#if BL_DEBUG_HEAVY
    uart_write_string(0, "> ");
    uart_write_string_P(0, _T(CONTROL));
    uart_write(0, ' ');
    uart_write_binchar(0, state.ctrl);
    gpib_print_ctrl(state.ctrl);
    uart_write_string(0, ", ");
    uart_write_string_P(0, _T(DATA));
    uart_write_string(0, ": ");
    gpib_print_data(*data);
    uart_write_string_P(0, _T(CRLF));
#endif
    return 0;
}

uint8_t gpib_write(uint8_t data)
{
    uint8_t retry = 0;

    LED2_PORT ^= _BV(LED2_BIT);

    // Set DAV high
    CTRL_PORT |= _BV(CTRL_DAV_BIT);

    // Is NDAC and NRFD high?
    for (retry = 0; retry < 255; retry++) {
        state.ctrl = CTRL_PIN;
        if ((state.ctrl & _BV(CTRL_NDAC_BIT)) &&
            (state.ctrl & _BV(CTRL_NRFD_BIT))) {
            _delay_us(10);
            continue;
        }
        break;
    }

    if (retry == 255) {
        debug_P(TRUE, _T(DEVICE_NOT_PRESENT));
        return 1;
    }

    // Write data
    DATA_PORT = ~reverse_bits(data);

    // Wait for listener to drive NRFD high
    for (retry = 0; retry < 255; retry++) {
        state.ctrl = CTRL_PIN;
        if (state.ctrl & _BV(CTRL_NRFD_BIT)) break;
        _delay_ms(1);
    }

    if (retry == 255) {
        debug_P(TRUE, _T(TIMEOUT_NRFD_HIGH));
        return 1;
    }

    // Set DAV low
    CTRL_PORT &= ~_BV(CTRL_DAV_BIT);

    // Wait for listener to drive NDAC high
    for (retry = 0; retry < 65; retry++) {
        state.ctrl = CTRL_PIN;
        if (state.ctrl & _BV(CTRL_NDAC_BIT)) break;
        _delay_ms(1);
    }

    if (retry == 65) {
        debug_P(TRUE, _T(TIMEOUT_NDAC_HIGH));
        return 1;
    }

    // Set DAV high
    CTRL_PORT |= _BV(CTRL_DAV_BIT);
    // Update ctrl register
    state.ctrl = CTRL_PIN;

#if BL_DEBUG_HEAVY
    uart_write_string(0, "< ");
    uart_write_string_P(0, _T(CONTROL));
    uart_write(0, ' ');
    uart_write_binchar(0, state.ctrl);
    gpib_print_ctrl(state.ctrl);
    uart_write_string(0, ", ");
    uart_write_string_P(0, _T(DATA));
    uart_write_string(0, ": ");
    gpib_print_data(data);
    uart_write_string_P(0, _T(CRLF));
#endif
    DATA_PORT = 0xFF;

    return 0;
}

void gpib_event(void)
{
    uint8_t paddr;
#ifdef BL_DEBUG
    uart_write_string(0, "> ");
    uart_write_string_P(0, _T(CONTROL));
    uart_write(0, ' ');
    uart_write_binchar(0, state.ctrl);
    gpib_print_ctrl(state.ctrl);
    uart_write_string_P(0, _T(CRLF));
#endif
    if (state.ctrl & _BV(CTRL_REN_BIT)) {
        if (state.controller)
            state.controller = 0;
        return;
    }

    if (!(state.ctrl & _BV(CTRL_IFC_BIT))) {
        struct bl_pkt pkt = { BLOP_RESET | BLOP_ID_MASK, 0, 0, 0 };
        uart_write_pkt(0, &pkt, NULL);
        if (state.bluetooth)
            uart_write_pkt(1, &pkt, NULL);
        return;
    }

    if (state.ctrl & _BV(CTRL_ATN_BIT)) return;

    debug_P(TRUE, _T(GPIB_ATN));

    if (gpib_read(&paddr) != 0) return;

    if (GPIB_GROUP(paddr) == GPIB_LAG_H &&
        GPIB_ADDR(paddr) == GPIB_LAG_UNL) return;
    else if (GPIB_GROUP(paddr) == GPIB_TAG_H &&
        GPIB_ADDR(paddr) == GPIB_TAG_UNT) return;

    switch (GPIB_GROUP(paddr)) {
    case GPIB_LAG_L:
    case GPIB_LAG_H:
        debug_P(TRUE, _T(GPIB_LAG));
        gpib_listen(paddr);

        return;
    case GPIB_TAG_L:
    case GPIB_TAG_H:
        debug_P(TRUE, _T(GPIB_TAG));
        gpib_talk(paddr);

        return;
    }

    debug_P(TRUE, _T(GPIB_INVALID_GROUP));
}

void gpib_talk(uint8_t paddr)
{
    char dec[4];
    uint8_t id, saddr;

    // Get device ID
    id = (GPIB_GROUP(paddr) == GPIB_TAG_L) ?
        GPIB_ADDR(paddr) : 16 + GPIB_ADDR(paddr);

    // Is the device ID valid?
    if (!gpib_validate_device_id(id)) {
        debug_P(TRUE, _T(INVALID_DEVICE_ID));
        return;
    }

    // Look for secondary address
    saddr = GPIB_SCG_L;
    state.ctrl = CTRL_PIN;
    if (!(state.ctrl & _BV(CTRL_ATN_BIT))) {
        if (gpib_read(&saddr) != 0) return;
    }

    debug(FALSE, "Device ");
    bin2dec(id, dec);
    debug(FALSE, dec);
    debug(FALSE, ",");
    bin2dec(GPIB_ADDR(saddr), dec);
    debug(TRUE, dec);

    // Which command group?
    if (GPIB_GROUP(saddr) == GPIB_ACG)
        debug_P(TRUE, _T(GPIB_ACG));
    else if (GPIB_GROUP(saddr) == GPIB_UCG)
        debug_P(TRUE, _T(GPIB_UCG));
    else if (GPIB_GROUP(saddr) == GPIB_SCG_L ||
        GPIB_GROUP(saddr) == GPIB_SCG_H)
        debug_P(TRUE, _T(GPIB_SCG));
    else if (GPIB_GROUP(saddr) == GPIB_SCG_OPEN_SAVE)
        debug_P(TRUE, _T(GPIB_SCG_OPEN_SAVE));
    else
        debug(TRUE, "Unknown command group");

    // Internal or external device?
    if (!device[id].internal)
        gpib_talk_external(id, saddr);
    else
        gpib_talk_internal(id, saddr);
}

void gpib_talk_internal(uint8_t id, uint8_t saddr)
{
    if (device[id].filename[0] == '$') {
        gpib_talk_enable(1);
#define LIST_LEN 36
        uint8_t list[LIST_LEN] = {
            0x01, 0x04,         // Starting memory address
            0x21, 0x04,         // Pointer to next statement
            0x00, 0x00,         // Line number
            0x12, '"',          // Reverse on
            ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
            ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
            '"', ' ',
            '0', '0', ' ', '2', 'A', ' ',
            0x12,               // Reverse off
            0x00,               // End of line

            0x00, 0x00          // End of program
        };
        uint8_t name[] = { PACKAGE_NAME };
        for (uint8_t i = 0; i < BL_GPIB_FILELEN + 1 && name[i]; i++) {
            list[i + 8] = (name[i] > 0x60 && name[i] < 0x7B) ?
                (name[i] - 0x20) : name[i];
        }
        char hex[3];
        bin2hex(FIRMWARE_VERSION, hex);
        list[26] = hex[0];
        list[27] = hex[1];
        for (uint8_t i = 0; i < LIST_LEN; i++) {
            if (i == LIST_LEN - 1)
                CTRL_PORT &= ~_BV(CTRL_EOI_BIT);
            if (gpib_write(list[i]) != 0) break;
        }

        CTRL_PORT |= _BV(CTRL_EOI_BIT);
    }
    else if (
        device[id].filename[0] == 'T' &&
        device[id].filename[1] == 'E' &&
        device[id].filename[2] == 'S' &&
        device[id].filename[3] == 'T' &&
        device[id].filename[4] == '\0') {
        gpib_talk_enable(1);

        for (uint16_t i = 0; i < testprg_len; i++) {
            if (i == testprg_len - 1)
                CTRL_PORT &= ~_BV(CTRL_EOI_BIT);
            if (gpib_write(pgm_read_byte(&testprg[i])) != 0) break;
        }

        CTRL_PORT |= _BV(CTRL_EOI_BIT);
    }
    else if (
        device[id].filename[0] == 'H' &&
        device[id].filename[1] == 'E' &&
        device[id].filename[2] == 'L' &&
        device[id].filename[3] == 'L' &&
        device[id].filename[4] == 'O' &&
        device[id].filename[5] == '\0') {
        gpib_talk_enable(1);

        for (uint16_t i = 0; i < hello_len; i++) {
            if (i == hello_len - 1)
                CTRL_PORT &= ~_BV(CTRL_EOI_BIT);
            if (gpib_write(pgm_read_byte(&hello[i])) != 0) break;
        }

        CTRL_PORT |= _BV(CTRL_EOI_BIT);
    }
}

void gpib_talk_external(uint8_t id, uint8_t saddr)
{
    //char hex[3];

    gpib_talk_enable(1);

    struct bl_pkt pkt = { BLOP_READ | BLOP_ID_MASK, id, saddr };

    uint8_t length = 0;
    while (device[id].filename[length++] != '\0');
    pkt.length = length - 1;

    uart_write_pkt(device[id].uart, &pkt,
        (pkt.length) ? (uint8_t *)device[id].filename : NULL);

    for ( ;; ) {

        if (device[id].uart == 0)
            UART0_RTS_PORT &= ~_BV(UART0_RTS_BIT);
        else
            UART1_RTS_PORT &= ~_BV(UART1_RTS_BIT);

        uart_read_pkt(device[id].uart, &pkt);

        if (device[id].uart == 0)
            UART0_RTS_PORT |= _BV(UART0_RTS_BIT);
        else
            UART1_RTS_PORT |= _BV(UART1_RTS_BIT);

        //debug_pkt(&pkt);

        if ((pkt.op &
            ~(BLOP_ID_MASK | BLOP_FLAG_EOF | BLOP_FLAG_RES1)) == BLOP_READ) {

            if (pkt.op & BLOP_FLAG_EOF)
                CTRL_PORT &= ~_BV(CTRL_EOI_BIT);

            for (uint8_t i = 0; i < pkt.length; i++) {

                if (device[id].uart == 0)
                    UART0_RTS_PORT &= ~_BV(UART0_RTS_BIT);
                else
                    UART1_RTS_PORT &= ~_BV(UART1_RTS_BIT);

                uart_read_wait(device[id].uart, &state.data);

                if (device[id].uart == 0)
                    UART0_RTS_PORT |= _BV(UART0_RTS_BIT);
                else
                    UART1_RTS_PORT |= _BV(UART1_RTS_BIT);

                //bin2hex(state.data, hex);
                //debug(TRUE, hex);

                if (gpib_write(state.data) != 0) {
                    pkt.op = BLOP_ERROR | BLOP_ID_MASK;
                    pkt.paddr = BLERR_GPIB_WRITE;
                    pkt.saddr = 0x00;
                    pkt.length = 0x00;

                    uart_write_pkt(device[id].uart, &pkt, NULL);

                    return;
                }
            }

            if (pkt.op & BLOP_FLAG_EOF) {
                debug(TRUE, "EOF");
                break;
            }
        }
        else if ((pkt.op &
            ~(BLOP_ID_MASK | BLOP_FLAG_EOF | BLOP_FLAG_RES1)) == BLOP_ERROR) {
            debug(TRUE, "Error reply");
            break;
        }
        else if ((pkt.op &
            ~(BLOP_ID_MASK | BLOP_FLAG_EOF | BLOP_FLAG_RES1)) == BLOP_STATUS) {
            debug(TRUE, "Discard status...");
        }
        else {
            debug(TRUE, "Unexpected reply");
            debug_pkt(&pkt);
            break;
        }
    }
}

void gpib_listen(uint8_t paddr)
{
    char dec[4];
    uint8_t id, saddr;

    // Get device ID
    id = (GPIB_GROUP(paddr) == GPIB_LAG_L) ?
        GPIB_ADDR(paddr) : 16 + GPIB_ADDR(paddr);

    // Is the device ID valid?
    if (!gpib_validate_device_id(id)) {
        debug_P(TRUE, _T(INVALID_DEVICE_ID));
        return;
    }

    // Look for secondary address
    saddr = GPIB_SCG_L;
    state.ctrl = CTRL_PIN;
    if (!(state.ctrl & _BV(CTRL_ATN_BIT))) {
        if (gpib_read(&saddr) != 0) return;
    }

    debug(FALSE, "Device ");
    bin2dec(id, dec);
    debug(FALSE, dec);
    debug(FALSE, ",");
    bin2dec(GPIB_ADDR(saddr), dec);
    debug(TRUE, dec);

    // Secondary group command?
    if (GPIB_GROUP(saddr) == GPIB_SCG_L ||
        GPIB_GROUP(saddr) == GPIB_SCG_H) {
        debug(TRUE, "TODO: secondary command group");
        for (state.data = 0;
            state.data != (GPIB_LAG_H | GPIB_LAG_UNL); ) {
            if (gpib_read(&state.data) != 0) break;
        }
        return;
    }

    // Secondary group command: CLOSE?
    if (GPIB_GROUP(saddr) == GPIB_SCG_CLOSE) {
        debug(TRUE, "Close");
        for (state.data = 0;
            state.data != (GPIB_LAG_H | GPIB_LAG_UNL); ) {
            if (gpib_read(&state.data) != 0) break;
        }
        device[id].filename[0] = '\0';
        return;
    }

    // Secondary group command, not OPEN/SAVE?
    if (GPIB_GROUP(saddr) != GPIB_SCG_OPEN_SAVE) {
        debug(TRUE, "Invalid secondary group");
        return;
    }

    // Copy LOAD/OPEN/SAVE filename
    debug(TRUE, "Load/Open/Save");

    for (uint8_t i = 0; i < BL_GPIB_FILELEN; i++) {
        if (gpib_read(&state.data) != 0) return;
        device[id].filename[i] = state.data;
        if (!(state.ctrl & _BV(CTRL_EOI_BIT))) {
            device[id].filename[i + 1] = '\0';
            break;
        }
    }

    for (state.data = 0;
        state.data != (GPIB_LAG_H | GPIB_LAG_UNL); ) {
        if (gpib_read(&state.data) != 0) break;
    }

    debug(FALSE, "Filename: ");
    debug(TRUE, device[id].filename);
}

uint8_t gpib_validate_device_id(uint8_t id)
{
    if (id < 4 || id > BL_GPIB_DEVICES - 2) return 0;
    if (!device[id].registered) return 0;
    return 1;
}

#ifdef BL_DEBUG
void gpib_print_ctrl(uint8_t ctrl)
{
    char bits[9] = { '-', '-', '-', '-', '-', '-', '-', '-', '\0' };

    if (!(ctrl & _BV(CTRL_REN_BIT)))
        bits[7] = 'r';
    if (!(ctrl & _BV(CTRL_IFC_BIT)))
        bits[6] = 'i';
    if (!(ctrl & _BV(CTRL_NDAC_BIT)))
        bits[5] = 'n';
    if (!(ctrl & _BV(CTRL_NRFD_BIT)))
        bits[4] = 'f';
    if (!(ctrl & _BV(CTRL_DAV_BIT)))
        bits[3] = 'd';
    if (!(ctrl & _BV(CTRL_EOI_BIT)))
        bits[2] = 'e';
    if (!(ctrl & _BV(CTRL_ATN_BIT)))
        bits[1] = 'a';
    if (!(ctrl & _BV(CTRL_SRQ_BIT)))
        bits[0] = 's';

    uart_write(0, '[');
    uart_write_string(0, bits);
    uart_write(0, ']');
}

void gpib_print_data(uint8_t data)
{
    char hex[3];

    uart_write_binchar(0, data);
    uart_write_string(0, " (0x");
    bin2hex(data, hex);
    uart_write_string(0, hex);
    if ((data & 0x7F) >= 0x20 && (data & 0x7F) < 0x7F) {
        uart_write_string(0, " '");
        uart_write(0, (data & 0x7F));
        uart_write(0, '\'');
    }
    uart_write(0, ')');
}
#endif

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
