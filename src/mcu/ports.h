// Project port and address declarations
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#ifndef _PORTS_H
#define _PORTS_H

#define LED1_DDR        DDRB
#define LED1_PORT       PORTB
#define LED1_BIT        PB0
#define LED1_PIN        PINB

#define LED2_DDR        DDRD
#define LED2_PORT       PORTD
#define LED2_BIT        PD7
#define LED2_PIN        PIND

#define BTS_DDR         DDRD
#define BTS_PORT        PORTD
#define BTS_PIN         PIND
#define BTS_BIT         PD6
#define BTS_INT         PCINT30

#define PROG_DDR        DDRB
#define PROG_PORT       PORTB
#define PROG_BIT        PB5
#define PROG_PIN        PINB
#define PROG_INT        PCINT13

#define UART0_CTS_DDR   DDRB
#define UART0_CTS_PORT  PORTB
#define UART0_CTS_PIN   PINB
#define UART0_CTS_BIT   PB7
#define UART1_CTS_INT   PCINT28

#define UART0_RTS_DDR   DDRB
#define UART0_RTS_PORT  PORTB
#define UART0_RTS_BIT   PB6
#define UART0_RTS_PIN   PINB

#define UART1_CTS_DDR   DDRD
#define UART1_CTS_PORT  PORTD
#define UART1_CTS_BIT   PD4
#define UART1_CTS_PIN   PIND
#define UART1_CTS_INT   PCINT28

#define UART1_RTS_DDR   DDRD
#define UART1_RTS_PORT  PORTD
#define UART1_RTS_BIT   PD5
#define UART1_RTS_PIN   PIND
#define UART1_RTS_INT   PCINT29

#define CTRL_PIN        PINC
#define CTRL_DDR        DDRC
#define CTRL_PORT       PORTC

#define CTRL_TE_DDR     DDRB
#define CTRL_TE_PORT    PORTB
#define CTRL_TE_BIT     PB4

#define CTRL_DC_DDR     DDRB
#define CTRL_DC_PORT    PORTB
#define CTRL_DC_BIT     PB3

#define CTRL_REN_DDR    DDRC
#define CTRL_REN_PORT   PORTC
#define CTRL_REN_BIT    PC0
#define CTRL_REN_INT    PCINT16

#define CTRL_IFC_DDR    DDRC
#define CTRL_IFC_PORT   PORTC
#define CTRL_IFC_BIT    PC1
#define CTRL_IFC_INT    PCINT17

#define CTRL_NDAC_DDR   DDRC
#define CTRL_NDAC_PORT  PORTC
#define CTRL_NDAC_BIT   PC2
#define CTRL_NDAC_INT   PCINT18

#define CTRL_NRFD_DDR   DDRC
#define CTRL_NRFD_PORT  PORTC
#define CTRL_NRFD_BIT   PC3
#define CTRL_NRFD_INT   PCINT19

#define CTRL_DAV_DDR    DDRC
#define CTRL_DAV_PORT   PORTC
#define CTRL_DAV_BIT    PC4
#define CTRL_DAV_INT    PCINT20

#define CTRL_EOI_DDR    DDRC
#define CTRL_EOI_PORT   PORTC
#define CTRL_EOI_BIT    PC5
#define CTRL_EOI_INT    PCINT21

#define CTRL_ATN_DDR    DDRC
#define CTRL_ATN_PORT   PORTC
#define CTRL_ATN_BIT    PC6
#define CTRL_ATN_INT    PCINT22

#define CTRL_SRQ_DDR    DDRC
#define CTRL_SRQ_PORT   PORTC
#define CTRL_SRQ_BIT    PC7
#define CTRL_SRQ_INT    PCINT23

#define DATA_DDR        DDRA
#define DATA_PIN        PINA
#define DATA_PORT       PORTA

#define DATA_TE_DDR     DDRB
#define DATA_TE_PORT    PORTB
#define DATA_TE_BIT     PB2

#define DATA_PE_DDR     DDRB
#define DATA_PE_PORT    PORTB
#define DATA_PE_BIT     PB1

#define DATA_D1_DDR     DDRA
#define DATA_D1_PORT    PORTA
#define DATA_D1_BIT     PA7

#define DATA_D2_DDR     DDRA
#define DATA_D2_PORT    PORTA
#define DATA_D2_BIT     PA6

#define DATA_D3_DDR     DDRA
#define DATA_D3_PORT    PORTA
#define DATA_D3_BIT     PA5

#define DATA_D4_DDR     DDRA
#define DATA_D4_PORT    PORTA
#define DATA_D4_BIT     PA4

#define DATA_D5_DDR     DDRA
#define DATA_D5_PORT    PORTA
#define DATA_D5_BIT     PA3

#define DATA_D6_DDR     DDRA
#define DATA_D6_PORT    PORTA
#define DATA_D6_BIT     PA2

#define DATA_D7_DDR     DDRA
#define DATA_D7_PORT    PORTA
#define DATA_D7_BIT     PA1

#define DATA_D8_DDR     DDRA
#define DATA_D8_PORT    PORTA
#define DATA_D8_BIT     PA0

#endif // _PORTS_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
