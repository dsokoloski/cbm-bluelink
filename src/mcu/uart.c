// UART routines
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#include <stdlib.h>
#include <stdint.h>

#include <avr/io.h>
#include <avr/power.h>
#include <avr/pgmspace.h>

#include <cbm-bluelink.h>

#include "uart.h"
#include "ports.h"

void uart_enable(uint8_t un, uint32_t baud)
{
    switch (un) {
    case 0:
        power_usart0_enable();

        UBRR0L = (uint8_t)(F_CPU / (baud * 16UL) - 1UL);
        UBRR0H = (F_CPU / (baud * 16L) - 1) >> 8;
        UCSR0C = (_BV(UCSZ01) | _BV(UCSZ00));
        UCSR0B = (_BV(RXEN0) | _BV(TXEN0));

        UART0_CTS_DDR &= ~_BV(UART0_CTS_BIT);
        UART0_RTS_DDR |= _BV(UART0_RTS_BIT);

        break;
    case 1:
        power_usart1_enable();

        UBRR1L = (uint8_t)(F_CPU / (baud * 16UL) - 1UL);
        UBRR1H = (F_CPU / (baud * 16UL) - 1) >> 8;
        UCSR1C = (_BV(UCSZ11) | _BV(UCSZ10));
        UCSR1B = (_BV(RXEN1) | _BV(TXEN1));

        UART1_CTS_DDR &= ~_BV(UART1_CTS_BIT);
        UART1_RTS_DDR |= _BV(UART1_RTS_BIT);

        break;
    }
}

void uart_disable(uint8_t un)
{
    switch (un) {
    case 0:
        power_usart0_disable();

        break;
    case 1:
        power_usart1_disable();

        break;
    }
}

void uart_interrupt_enable(uint8_t un)
{
    switch (un) {
    case 0:
        UCSR0B |= _BV(RXCIE0);

        break;
    case 1:
        UCSR1B |= _BV(RXCIE1);

        break;
    }
}

void uart_interrupt_disable(uint8_t un)
{
    switch (un) {
    case 0:
        UCSR0B &= ~_BV(RXCIE0);

        break;
    case 1:
        UCSR1B &= ~_BV(RXCIE1);

        break;
    }
}

uint8_t uart_read(uint8_t un, volatile uint8_t *byte)
{
    uint8_t status = 0x00;

    switch (un) {
    case 0:
        if ((UCSR0A & _BV(RXC0))) {
            status = UCSR0A;
            *byte = UDR0;
        }

        break;
    case 1:
        if ((UCSR1A & _BV(RXC1))) {
            status = UCSR1A;
            *byte = UDR1;
        }

        break;
    }

    return status;
}

uint8_t uart_read_wait(uint8_t un, volatile uint8_t *byte)
{
    switch (un) {
    case 0:
        loop_until_bit_is_set(UCSR0A, RXC0);
        return uart_read(un, byte);

        break;
    case 1:
        loop_until_bit_is_set(UCSR1A, RXC1);
        return uart_read(un, byte);

        break;
    }

    return 0x00;
}

void uart_read_pkt(uint8_t un, struct bl_pkt *pkt)
{
    uint8_t i;
    uint8_t *ptr = (uint8_t *)pkt;

    switch (un) {
    case 0:
        for (i = 0; i < sizeof(struct bl_pkt); i++, ptr++) {
            loop_until_bit_is_set(UCSR0A, RXC0);
            *ptr = UDR0;
        }

        break;
    case 1:
        for (i = 0; i < sizeof(struct bl_pkt); i++, ptr++) {
            loop_until_bit_is_set(UCSR1A, RXC1);
            *ptr = UDR1;
        }

        break;
    }
}

uint8_t uart_read_ready(uint8_t un)
{
    switch (un) {
    case 0:
        return (UCSR0A & _BV(RXC0));

    case 1:
        return (UCSR1A & _BV(RXC1));
    }

    return 0x00;
}

void uart_write(uint8_t un, uint8_t byte)
{
    switch (un) {
    case 0:
        loop_until_bit_is_set(UCSR0A, UDRE0);
        UDR0 = byte;

        break;
    case 1:
        loop_until_bit_is_set(UCSR1A, UDRE1);
        UDR1 = byte;

        break;
    }
}

void uart_write_pkt(uint8_t un, struct bl_pkt *pkt, uint8_t *data)
{
    uint8_t i, length = pkt->length;
    uint8_t *ptr = (uint8_t *)pkt;

    switch (un) {
    case 0:
        for (i = 0; i < sizeof(struct bl_pkt); i++) {
            loop_until_bit_is_set(UCSR0A, UDRE0);
            UDR0 = *ptr++;
        }
        if (data == NULL) break;
        for (i = 0; i < length; i++) {
            loop_until_bit_is_set(UCSR0A, UDRE0);
            UDR0 = data[i];
        }

        break;
    case 1:
        for (i = 0; i < sizeof(struct bl_pkt); i++) {
            loop_until_bit_is_set(UCSR1A, UDRE1);
            UDR1 = *ptr++;
        }
        if (data == NULL) break;
        for (i = 0; i < length; i++) {
            loop_until_bit_is_set(UCSR1A, UDRE1);
            UDR1 = data[i];
        }

        break;
    }
}

void uart_write_string(uint8_t un, char *s)
{
    while (*s) uart_write(un, *s++);
}

void uart_write_string_P(uint8_t un, const char *s)
{
    uint16_t i = 0;
    while (pgm_read_byte(&s[i])) {
        uart_write(un, pgm_read_byte(&s[i]));
        i++;
    }
}

void uart_write_binchar(uint8_t un, uint8_t byte)
{
    for (int i = 7; i >= 0; i--) {
        if ((byte & _BV((uint8_t)i)))
            uart_write(un, '1');
        else
            uart_write(un, '0');
    }
}

void uart_write_binchar_inv(uint8_t un, uint8_t byte)
{
    for (int i = 7; i >= 0; i--) {
        if ((byte & _BV((uint8_t)i)))
            uart_write(un, '0');
        else
            uart_write(un, '1');
    }
}

void uart_write_binval(uint8_t un, uint8_t *byte, int length)
{
    for (int c = 0; c < length; c++) {
        for (int i = 7; i >= 0; i--) {
            if ((byte[c] & _BV((uint8_t)i)))
                uart_write(un, '1');
            else
                uart_write(un, '0');
        }

        uart_write(un, ' ');
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
