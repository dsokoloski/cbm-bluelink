// Global state data and routines
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#ifndef _STATE_H
#define _STATE_H

typedef struct app_state_t {
    struct bl_pkt uart0_pkt;
    uint8_t uart0_data_length;

    struct bl_pkt uart1_pkt;
    uint8_t uart1_data_length;

    uint8_t ctrl;
    uint8_t data;

    uint32_t bluetooth:1;
    uint32_t bluetooth_changed:1;
    uint32_t controller:1;
    uint32_t program:1;

    uint32_t debug_enable:1;
    uint32_t debug_uart:1;

    uint32_t uart0_cts:1;
    uint32_t uart1_cts:1;

} app_state;

extern app_state state;

void state_init(void);

#endif // _STATE_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
