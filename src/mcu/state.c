// Global state data and routines
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#include <stdint.h>

#include <cbm-bluelink.h>

#include "state.h"

// Global state structure
app_state state;

void state_init(void)
{
    state.uart0_pkt.op = 0xFF;
    state.uart0_pkt.paddr = 0xFF;
    state.uart0_pkt.saddr = 0xFF;
    state.uart0_pkt.length = 0x00;

    state.uart1_pkt.op = 0xFF;
    state.uart1_pkt.paddr = 0xFF;
    state.uart1_pkt.saddr = 0xFF;
    state.uart1_pkt.length = 0x00;

    state.ctrl = 0;
    state.data = 0;

    state.bluetooth = 0;
    state.bluetooth_changed = 0;
    state.controller = 0;
    state.program = 1;

    state.debug_enable = 0;
    state.debug_uart = 0;

    state.uart0_cts = 0;
    state.uart1_cts = 0;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
