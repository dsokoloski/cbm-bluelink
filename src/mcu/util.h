// Utilitiy macros and routines
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#ifndef _UTIL_H
#define _UTIL_H

#define reset() { wdt_enable(WDTO_15MS); for ( ;; ); }

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

uint8_t reverse_bits(uint8_t byte);
void bin2hex(uint8_t dec, char hex[3]);
void bin2dec(uint8_t bin, char dec[4]);
void debug(uint8_t send_eof, char *message);
void debug_P(uint8_t send_eof, const char *message);
void debug_pkt(struct bl_pkt *pkt);

#endif // _UTIL_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
