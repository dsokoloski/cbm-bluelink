#!/usr/bin/env python
# Process files of text strings in to a C files.

import os
import sys
import re

class Strings(object):
    def __init__(self):
        self.tags = { }
        self.rx_string = re.compile(r'^(\w+):\s+(.*),?')
        self.fmt_const = "const char app_text_%s[] PROGMEM = \"%s\";"

    def parse(self, filename):
        fh_input = open(filename, 'r')
        lines = fh_input.readlines()
        fh_input.close()
        for line in lines:
            for k, v in self.rx_string.findall(line):
                self.tags[k] = v

    def write_enum(self):
        print "enum {"
        for k in sorted(self.tags.iterkeys()):
            print "    _T_%s," %k
        print "};\n"

    def write_const(self):
        for k in sorted(self.tags.iterkeys()):
            print self.fmt_const %(k, self.tags[k])
        print

    def write_array(self):
        print "PGM_P const app_text[] PROGMEM = {"
        for k in sorted(self.tags.iterkeys()):
            print "    app_text_%s," %k
        print "};"

def main():
    if len(sys.argv) < 3:
        raise Exception('Required parameter missing.')
  
    strings = Strings()

    for filename in sys.argv[2:]:
        if os.path.exists(filename):
            strings.parse(filename)
        else:
            raise Exception('File not found: ' + filename)

    if sys.argv[1] == 'enum':
        strings.write_enum()
    elif sys.argv[1] == 'const':
        strings.write_const()
    elif sys.argv[1] == 'array':
        strings.write_array()
    else:
        raise Exception('Invalid mode: ' + sys.argv[1])

if __name__ == "__main__":
    try:
        main()
    except Exception, msg:
        print "Error: %s" %msg

# vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
