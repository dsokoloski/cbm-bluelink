#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "gpib.h"

static void parse_addr(uint8_t addr)
{
	uint8_t addr_hi = GPIB_GROUP(addr);
	uint8_t addr_lo = GPIB_ADDR(addr);

	fputc('\r', stdout);

	switch (addr_hi) {
	case GPIB_ACG:
		fprintf(stdout, "$%hhX - Addressed Command Group (ACG): ", addr_hi >> 4);
		switch (addr_lo) {
		case GPIB_ACG_GTL:
			fprintf(stdout, "GTL\n");
			break;
		case GPIB_ACG_SDC:
			fprintf(stdout, "SDC\n");
			break;
		case GPIB_ACG_PPC:
			fprintf(stdout, "PPC\n");
			break;
		case GPIB_ACG_GET:
			fprintf(stdout, "GET\n");
			break;
		case GPIB_ACG_TCT:
			fprintf(stdout, "TCT\n");
			break;
		default:
			fprintf(stdout, "Invalid (%hhu)\n", addr_lo);
		}
		break;
	case GPIB_UCG:
		fprintf(stdout, "$%hhX - Universal Command Group (UCG): ", addr_hi >> 4);
		switch (addr_lo) {
		case GPIB_UCG_LLO:
			fprintf(stdout, "LLO\n");
			break;
		case GPIB_UCG_DCL:
			fprintf(stdout, "DCL\n");
			break;
		case GPIB_UCG_PPU:
			fprintf(stdout, "PPU\n");
			break;
		case GPIB_UCG_SPE:
			fprintf(stdout, "SPE\n");
			break;
		case GPIB_UCG_SPD:
			fprintf(stdout, "SPD\n");
			break;
		default:
			fprintf(stdout, "Invalid (%hhu)\n", addr_lo);
		}
		break;
	case GPIB_LAG_L:
	case GPIB_LAG_H:
		fprintf(stdout, "$%hhX - Listen Address Group (LAG): ", addr_hi >> 4);
		if (addr_hi == GPIB_LAG_H && addr_lo == GPIB_LAG_UNL)
			fprintf(stdout, "Unlisten (UNL)\n");
		else if (addr_hi == GPIB_LAG_L)
			fprintf(stdout, "%hhu\n", addr_lo);
		else if (addr_hi == GPIB_LAG_H)
			fprintf(stdout, "%hhu\n", 16 + addr_lo);
		break;
	case GPIB_TAG_L:
	case GPIB_TAG_H:
		fprintf(stdout, "$%hhX - Talk Address Group (TAG): ", addr_hi >> 4);
		if (addr_hi == GPIB_TAG_H && addr_lo == GPIB_TAG_UNT)
			fprintf(stdout, "Untalk (UNT)\n");
		else if (addr_hi == GPIB_TAG_L)
			fprintf(stdout, "%hhu\n", addr_lo);
		else if (addr_hi == GPIB_TAG_H)
			fprintf(stdout, "%hhu\n", 16 + addr_lo);
		break;
	case GPIB_SCG_L:
	case GPIB_SCG_H:
		fprintf(stdout, "$%hhX - Secondary Command Group (SCG): %hhu\n",
			addr_hi >> 4, (addr_hi == GPIB_SCG_L) ? addr_lo : 16 + addr_lo);
		break;
	case GPIB_SCG_CLOSE:
		fprintf(stdout, "$%hhX - Secondary Command Group (SCG - CLOSE): %hhu\n",
			addr_hi >> 4, addr_lo);
		break;
	case GPIB_SCG_OPEN_SAVE:
		fprintf(stdout, "$%hhX - Secondary Command Group (SCG - OPEN/SAVE): %hhu\n",
			addr_hi >> 4, addr_lo);
		break;
	default:
		fprintf(stdout, "$%hhX - Invalid GPIB/CBM Command Group: 0x%hhu\n",
			addr_hi >> 4, addr);
	}
}

int main(int argc, char *argv[])
{
	uint8_t addr = 0;
	long page_size = sysconf(_SC_PAGESIZE);
	char input[page_size];

	for ( ;; ) {
		fprintf(stdout, "\r> ");
		switch (fscanf(stdin, "%02hhx", &addr)) {
		case 0:
			if (fgets(input, page_size, stdin) == NULL)
				fprintf(stderr, "Error reading input.\n");
			else
				fprintf(stderr, "Invalid input: %s", input);
			break;
		case EOF:
			fputc('\r', stdout);
			return 0;
		default:
			parse_addr(addr);
		}
	}

	return 0;
}

// vi: ts=4
