#!/bin/bash

killall -9 -q cbm-bluelinkd
while pidof cbm-bluelinkd; do sleep 1; done
echo -n "Ready to program? "; read key
make install
