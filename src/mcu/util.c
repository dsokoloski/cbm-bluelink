// Utilitiy macros and routines
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#include <stdlib.h>
#include <stdint.h>

#include <avr/pgmspace.h>

#include <cbm-bluelink.h>

#include "util.h"
#include "uart.h"
#include "state.h"

uint8_t reverse_bits(uint8_t byte)
{
    byte = (byte & 0xF0) >> 4 | (byte & 0x0F) << 4;
    byte = (byte & 0xCC) >> 2 | (byte & 0x33) << 2;
    byte = (byte & 0xAA) >> 1 | (byte & 0x55) << 1;
    return byte;
}

void bin2hex(uint8_t bin, char hex[3])
{
    // High nibble and we add 48 to convert 0..9 to '0'..'9'
    hex[0] = (bin / 16) + 48;
    // Low nibble and we add 48 to convert 0..9 to '0'..'9'
    hex[1] = (bin % 16) + 48;
    // If value is > ASCII '9' then it should be 'A'..'F' so we add 7
    if (hex[0] > '9') hex[0] += 7;
    // If value is > ASCII '9' then it should be 'A'..'F' so we add 7
    if (hex[1] > '9') hex[1] += 7;

    hex[2] = '\0';
}

void bin2dec(uint8_t bin, char dec[4])
{
    uint8_t i = 0;

    // Hundreds...
    if (bin >= 100) {
        dec[i++] = (bin / 100) | 0x30;
        bin = bin % 100;
    }
    // Tens...
    if (bin >= 10 || i != 0)
        dec[i++] = (bin / 10) | 0x30;
    // Ones...
    dec[i++] = (bin % 10) | 0x30;

    dec[i] = '\0';
}

void debug(uint8_t send_eof, char *message)
{
    if (!state.debug_enable) return;

    uint8_t length = 0;
    while (message[length++] != '\0');
    if (length - 1 == 0) return;

    struct bl_pkt pkt = { BLOP_DEBUG | BLOP_ID_MASK, 0, 0, length - 1 };
    if (send_eof) pkt.op |= BLOP_FLAG_EOF;

    uart_write_pkt(state.debug_uart, &pkt, NULL);
    uart_write_string(state.debug_uart, message);
}

void debug_P(uint8_t send_eof, const char *message)
{
    if (!state.debug_enable) return;

    uint8_t length = 0;
    while (pgm_read_byte(&message[length++]) != '\0');
    if (length - 1 == 0) return;

    struct bl_pkt pkt = { BLOP_DEBUG | BLOP_ID_MASK, 0, 0, length - 1 };
    if (send_eof) pkt.op |= BLOP_FLAG_EOF;

    uart_write_pkt(state.debug_uart, &pkt, NULL);
    uart_write_string_P(state.debug_uart, message);
}

void debug_pkt(struct bl_pkt *pkt)
{
    char hex[3];

    bin2hex(pkt->op, hex);
    debug(FALSE, hex);
    debug(FALSE, ":");

    bin2hex(pkt->paddr, hex);
    debug(FALSE, hex);
    debug(FALSE, ":");

    bin2hex(pkt->saddr, hex);
    debug(FALSE, hex);
    debug(FALSE, ":");

    bin2hex(pkt->length, hex);
    debug(TRUE, hex);
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
