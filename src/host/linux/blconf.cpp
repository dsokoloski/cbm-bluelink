// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "aconfig.h"
#endif

#include <string>
#include <vector>
#include <map>
#include <stdexcept>

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <expat.h>
#include <regex.h>
#include <pwd.h>
#include <grp.h>

#include <cbm-bluelink/blexception.h>
#include <cbm-bluelink/blconf.h>
#include <cbm-bluelink/bllog.h>
#include <cbm-bluelink/blutil.h>

blXmlTag::blXmlTag(const char *name, const char **attr)
    : name(name), text(""), data(NULL)
{
    for (int i = 0; attr[i]; i += 2)
        param[attr[i]] = attr[i + 1];
}

bool blXmlTag::ParamExists(const string &key)
{
    map<string, string>::iterator i;
    i = param.find(key);
    return (bool)(i != param.end());
}

string blXmlTag::GetParamValue(const string &key)
{
    map<string, string>::iterator i;
    i = param.find(key);
    if (i == param.end())
        throw blXmlKeyNotFound(key.c_str());
    return i->second;
}

bool blXmlTag::operator==(const char *tag)
{
    if (!strcasecmp(tag, name.c_str())) return true;
    return false;
}

bool blXmlTag::operator!=(const char *tag)
{
    if (!strcasecmp(tag, name.c_str())) return false;
    return true;
}

static void blXmlElementOpen(
    void *data, const char *element, const char **attr)
{
    blXmlParser *csp = (blXmlParser *)data;

    blXmlTag *tag = new blXmlTag(element, attr);
#ifdef _BL_DEBUG
    blLog::Log(blLog::Debug, "Element open: %s", tag->GetName().c_str());
#endif
    csp->ParseElementOpen(tag);
    csp->stack.push_back(tag);
}

static void blXmlElementClose(void *data, const char *element)
{
    blXmlParser *csp = (blXmlParser *)data;
    blXmlTag *tag = csp->stack.back();
#ifdef _BL_DEBUG
    blLog::Log(blLog::Debug, "Element close: %s", tag->GetName().c_str());
#endif
#if 0
    string text = tag->GetText();
    if (text.size()) {
        blLog::Log(blLog::Debug, "Text[%d]:", text.size());
        //blHexDump(stderr, text.c_str(), text.size());
    }
#endif
    csp->stack.pop_back();
    csp->ParseElementClose(tag);
    delete tag;
}

static void blXmlText(void *data, const char *txt, int length)
{
    if (length == 0) return;

    blXmlParser *csp = (blXmlParser *)data;

    blXmlTag *tag = csp->stack.back();
    string text = tag->GetText();
    for (int i = 0; i < length; i++) {
        if (txt[i] == '\n' || txt[i] == '\r' ||
            !isprint(txt[i])) continue;
        text.append(1, txt[i]);
    }
    tag->SetText(text);
}

blXmlParser::blXmlParser(void)
    : p(NULL), conf(NULL), fh(NULL), buffer(NULL)
{
    Reset();
}

blXmlParser::~blXmlParser()
{
    Reset();
    if (p != NULL) XML_ParserFree(p);
    if (buffer != NULL) delete [] buffer;
}

void blXmlParser::Reset(void)
{
    if (p != NULL) XML_ParserFree(p);
    p = XML_ParserCreate(NULL);
    XML_SetUserData(p, (void *)this);
    XML_SetElementHandler(p, blXmlElementOpen, blXmlElementClose);
    XML_SetCharacterDataHandler(p, blXmlText);

    if (buffer != NULL) delete [] buffer;
    page_size = ::blGetPageSize();
    buffer = new uint8_t[page_size];

    for (vector<blXmlTag *>::iterator i = stack.begin();
        i != stack.end(); i++) delete (*i);

    if (fh) {
        fclose(fh);
        fh = NULL;
    }
}

void blXmlParser::Parse(void)
{
    if (conf == NULL)
        throw blException(EINVAL, "Configuration not set.");

    if (fh != NULL) Reset();
    if (!(fh = fopen(conf->GetFilename(), "r")))
        throw blException(errno, conf->GetFilename());

    for (;;) {
        size_t length;
        length = fread(buffer, 1, page_size, fh);
        if (ferror(fh))
            throw blException(errno, conf->GetFilename());
        int done = feof(fh);

        if (!XML_Parse(p, (const char *)buffer, length, done))
            ParseError(XML_ErrorString(XML_GetErrorCode(p)));
        if (done) break;
    }
}

void blXmlParser::ParseError(const string &what)
{
    throw blXmlParseException(
        what.c_str(),
        XML_GetCurrentLineNumber(p),
        XML_GetCurrentColumnNumber(p),
        buffer[XML_GetCurrentByteIndex(p)]);
}

blConf::blConf(const char *filename, blXmlParser *parser,
    int argc, char *argv[])
    : filename(filename), parser(parser), argc(argc), argv(argv)
{
}

blConf::~blConf()
{
    if (parser != NULL) delete parser;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
