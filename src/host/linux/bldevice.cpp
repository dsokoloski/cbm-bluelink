// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "aconfig.h"
#endif

#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <sstream>

#include <sys/types.h>
#include <sys/stat.h>

#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <pthread.h>
#include <dlfcn.h>

#include <cbm-bluelink/blexception.h>
#include <cbm-bluelink/bllog.h>
#include <cbm-bluelink/blevent.h>
#include <cbm-bluelink/blthread.h>
#include <cbm-bluelink/bldevice.h>

blDevice::blDevice(uint8_t id,
    blEventClient *parent, size_t stack_size)
    : blThread(stack_size), id(id), parent(parent), fh_state(NULL) { }

blDevice::~blDevice()
{
    SaveState();
    if (fh_state != NULL) fclose(fh_state);
    map<string, struct blDeviceStateValue *>::iterator i;
    for (i = state.begin(); i != state.end(); i++) {
        if (i->second->value) delete [] i->second->value;
        delete i->second;
    }
    Log(blLog::Debug, "destroyed");
}

void blDevice::Log(blLog::Level level, const string &message)
{
    stringstream format_stream("Device #");
    format_stream.seekp(0, ios::end);
    format_stream << (uint32_t)id << ": " << message;;

    blLog::Log(level, format_stream.str());
}

void blDevice::Log(blLog::Level level, const char *format, ...)
{
    va_list ap;

    stringstream format_stream("Device #");
    format_stream.seekp(0, ios::end);
    format_stream << (uint32_t)id << ": " << format;;

    va_start(ap, format);

    blLog::Log(level, format_stream.str().c_str(), ap);

    va_end(ap);
}

void blDevice::SetStateFile(const string &state_file)
{
    if (fh_state != NULL) fclose(fh_state);
    char mode[3] = { "r+" };
    struct stat state_stat;
    if (stat(state_file.c_str(), &state_stat) < 0)
        mode[0] = 'w';
    if ((fh_state = fopen(state_file.c_str(), mode)) == NULL) {
        Log(blLog::Warning, "Error opening state: %s: %s",
            state_file.c_str(), strerror(errno));
    }
    else LoadState();
}

void blDevice::LoadState(void)
{
    if (fh_state == NULL) return;
    rewind(fh_state);

    map<string, struct blDeviceStateValue *>::iterator i;
    for (i = state.begin(); i != state.end(); i++) {
        if (i->second->value != NULL) delete i->second->value;
        delete i->second;
    }
    state.clear();

    size_t records;
    if (fread((void *)&records, sizeof(size_t), 1, fh_state) != 1) {
        if (!feof(fh_state))
            Log(blLog::Error, "error reading state records");
        return;
    }

    Log(blLog::Debug, "state records: %lu", records);

    if (records == 0) return;

    for (size_t v = 0; v < records; v++) {
        size_t length;
        if (fread((void *)&length, sizeof(size_t), 1, fh_state) != 1) {
            Log(blLog::Error, "error reading record length");
            return;
        }

        if (length == 0) {
            Log(blLog::Error, "zero-length state record");
            return;
        }

        char *buffer = new char[length];
        if (fread((void *)buffer,
            sizeof(char), length, fh_state) != length) {
            Log(blLog::Error, "error reading record key");
            delete [] buffer;
            return;
        }

        string key;
        key.assign(buffer, length);
        delete [] buffer;

        struct blDeviceStateValue *var = new blDeviceStateValue;
        if (fread((void *)&var->length, sizeof(size_t), 1, fh_state) != 1) {
            Log(blLog::Error, "error reading data length");
            delete var;
            return;
        }

        if (var->length == 0)
            var->value = NULL;
        else {
            var->value = new uint8_t[var->length];
            if (fread((void *)var->value,
                sizeof(uint8_t), var->length, fh_state) != var->length) {
                Log(blLog::Error, "error reading data");
                delete [] var->value;
                delete var;
                return;
            }
        }

        state[key] = var;
    }
}

void blDevice::SaveState(void)
{
    if (fh_state == NULL) return;
    rewind(fh_state);

    size_t length = state.size();
    if (fwrite((const void *)&length, sizeof(size_t), 1, fh_state) != 1) {
        Log(blLog::Error, "error writing state record count");
        return;
    }

    map<string, struct blDeviceStateValue *>::iterator i;
    for (i = state.begin(); i != state.end(); i++) {
        length = i->first.size();
        if (!length) continue;
        if (fwrite((const void *)&length, sizeof(size_t), 1, fh_state) != 1) {
            Log(blLog::Error, "error writing key length");
            return;
        }
        if (fwrite((const void *)i->first.c_str(),
            sizeof(char), length, fh_state) != length) {
            Log(blLog::Error, "error writing key data");
            return;
        }

        length = i->second->length;
        if (fwrite((const void *)&length, sizeof(size_t), 1, fh_state) != 1) {
            Log(blLog::Error, "error writing data length");
            return;
        }
        if (fwrite((const void *)i->second->value,
            sizeof(uint8_t), length, fh_state) != length) {
            Log(blLog::Error, "error writing data");
            return;
        }
    }
}

bool blDevice::GetStateVar(const string &key, unsigned long &value)
{
    map<string, struct blDeviceStateValue *>::iterator i;
    i = state.find(key);
    if (i == state.end()) return false;
    if (i->second->length != sizeof(unsigned long)) return false;
    value = *(reinterpret_cast<unsigned long *>(i->second->value));
    return true;
}

bool blDevice::GetStateVar(const string &key, float &value)
{
    map<string, struct blDeviceStateValue *>::iterator i;
    i = state.find(key);
    if (i == state.end()) return false;
    if (i->second->length != sizeof(float)) return false;
    value = *(reinterpret_cast<float *>(i->second->value));
    return true;
}

bool blDevice::GetStateVar(const string &key, string &value)
{
    map<string, struct blDeviceStateValue *>::iterator i;
    i = state.find(key);
    if (i == state.end()) return false;
    if (i->second->length == 0) value = "";
    else {
        value.assign(
            reinterpret_cast<char *>(i->second->value), i->second->length);
    }
    return true;
}

bool blDevice::GetStateVar(const string &key, size_t &length, uint8_t *value)
{
    map<string, struct blDeviceStateValue *>::iterator i;
    i = state.find(key);
    if (i == state.end()) return false;
    length = (length > i->second->length) ? i->second->length : length;
    memcpy((void *)value, (const void *)i->second->value, length);
    return true;
}

void blDevice::SetStateVar(const string &key, const unsigned long &value)
{
    struct blDeviceStateValue *var = new struct blDeviceStateValue;

    var->length = sizeof(unsigned long);
    var->value = new uint8_t[sizeof(unsigned long)];
    memcpy((void *)var->value, (const void *)&value, var->length);

    SetStateVar(key, var);
}

void blDevice::SetStateVar(const string &key, const float &value)
{
    struct blDeviceStateValue *var = new struct blDeviceStateValue;

    var->length = sizeof(float);
    var->value = new uint8_t[sizeof(float)];
    memcpy((void *)var->value, (const void *)&value, var->length);

    SetStateVar(key, var);
}

void blDevice::SetStateVar(const string &key, const string &value)
{
    struct blDeviceStateValue *var = new struct blDeviceStateValue;

    var->length = value.size();
    if (var->length == 0)
        var->value = NULL;
    else {
        char *buffer = new char[var->length];
        value.copy(buffer, var->length);
        var->value = reinterpret_cast<uint8_t *>(buffer);
    }

    SetStateVar(key, var);
}

void blDevice::SetStateVar(
    const string &key, size_t length, const uint8_t *value)
{
    struct blDeviceStateValue *var = new struct blDeviceStateValue;

    var->length = length;
    if (var->length == 0)
        var->value = NULL;
    else {
        uint8_t *buffer = new uint8_t[var->length];
        memcpy((void *)buffer, (const void *)value, var->length);
        var->value = buffer;
    }

    SetStateVar(key, var);
}

void blDevice::SetStateVar(const string &key, struct blDeviceStateValue *var)
{
    map<string, struct blDeviceStateValue *>::iterator i;
    i = state.find(key);
    if (i != state.end()) {
        if (i->second->value) delete [] i->second->value;
        delete i->second;
    }
    state[key] = var;
}

blDeviceLoader::blDeviceLoader(const string &so_name,
    uint8_t id, blEventClient *parent, size_t stack_size)
    : so_name(so_name), so_handle(NULL)
{
    char *dlerror_string;
    typedef blDevice *(*blDeviceInit_t)(uint8_t, blEventClient *, size_t);

    so_handle = dlopen(so_name.c_str(), RTLD_NOW);
    if (so_handle == NULL) throw blException(dlerror());

    dlerror();
    blDeviceInit_t blDeviceInit = (blDeviceInit_t)dlsym(so_handle, "blDeviceInit");

    if ((dlerror_string = dlerror()) != NULL) {
        dlclose(so_handle);
        so_handle = NULL;
        blLog::Log(blLog::Warning,
            "Device #%hhu initialization failed: %s",
            id, dlerror_string);
        throw blException(dlerror_string);
    }

    device = (*blDeviceInit)(id, parent, stack_size);
    if (device == NULL) {
        dlclose(so_handle);
        so_handle = NULL;
        blLog::Log(blLog::Warning,
            "Device #%hhu initialization failed: %s",
            id, so_name.c_str());
        throw blException("blDeviceInit");
    }

    blLog::Log(blLog::Debug, "Device #%hhu loaded: %s",
        id, so_name.c_str());
}

blDeviceLoader::~blDeviceLoader()
{
    if (so_handle != NULL) dlclose(so_handle);
    blLog::Log(blLog::Debug, "Device dereferenced: %s",
        so_name.c_str());
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
