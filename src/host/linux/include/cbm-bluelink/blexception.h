// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _BLEXCEPTION_H
#define _BLEXCEPTION_H

#define _BL_THROW_DEBUG(e, s) \
    throw blDebugException(e, s, __LINE__, __FILE__)

using namespace std;

class blException : public runtime_error
{
public:
    explicit blException(void)
        : runtime_error("blException"),
        eint(-1), estring("blException") { };
    explicit blException(const char *s)
        : runtime_error("blException"),
        eint(-1), estring(s) { };
    explicit blException(int e)
        : runtime_error(strerror(e)), eint(e), estring("blException") { };
    explicit blException(int e, const char *s)
        : runtime_error(strerror(e)), eint(e), estring(s) { };
    explicit blException(const char *s, const char *es)
        : runtime_error(s), eint(0), estring(es) { };

    virtual ~blException() throw() { };

    const int eint;
    const string estring;
};

class blDebugException : public blException
{
public:
    explicit blDebugException(int e, const char *s, long l, const char *f)
        : blException(e, s), efile(f), eline(l) { };

    virtual ~blDebugException() throw() { };

    const char *efile;
    long eline;
};

#endif // _BLEXCEPTION_H
// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
