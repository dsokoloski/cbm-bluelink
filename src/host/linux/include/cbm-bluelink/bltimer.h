// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _BLTIMER_H
#define _BLTIMER_H

using namespace std;

typedef unsigned long cstimer_id_t;

class blThreadTimer;
class blTimer
{
public:
    blTimer(cstimer_id_t id,
        time_t value, time_t interval, blEventClient *target = NULL);
    virtual ~blTimer();

    inline cstimer_id_t GetId(void) { return id; };
    void Start(void);
    void Stop(void);
    void SetValue(time_t value);
    void SetInterval(time_t value);
    void Extend(time_t value);
    time_t GetInterval(void);
    time_t GetRemaining(void);
    inline blEventClient *GetTarget(void) { return target; };

protected:
    friend class blThreadTimer;

    bool running;
    cstimer_id_t id;
    time_t value;
    time_t interval;
    blEventClient *target;

    pthread_mutex_t *timer_mutex;
};

class blEventTimer : public blEvent
{
public:
    blEventTimer(blTimer *timer)
        : blEvent(blEVENT_TIMER), timer(timer) { };

    inline blTimer *GetTimer(void) { return timer; };

protected:
    blTimer *timer;
};

class blThreadTimer : public blThread
{
public:
    blThreadTimer(blEventClient *parent, const sigset_t &signal_set);
    virtual ~blThreadTimer();

    virtual void *Entry(void);

    void AddTimer(blTimer *timer);
    void RemoveTimer(blTimer *timer);

    static blThreadTimer *GetInstance(void) { return instance; };

protected:
    blEventClient *parent;
    sigset_t signal_set;
    timer_t timer_id;
    struct itimerspec it_spec;
    struct sigevent sev;

    static blThreadTimer *instance;
    static pthread_mutex_t *vector_mutex;
    static vector<blTimer *> timer_vector;

    void Tick(void);
};

#endif // _BLTIMER_H
// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
