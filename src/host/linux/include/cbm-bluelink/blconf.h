// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _BLCONF_H
#define _BLCONF_H

using namespace std;

class blXmlTag
{
public:
    blXmlTag(const char *name, const char **attr);

    inline string GetName(void) const { return name; };
    bool ParamExists(const string &key);
    string GetParamValue(const string &key);
    inline string GetText(void) const { return text; };
    inline void SetText(const string &text) { this->text = text; };
    void *GetData(void) { return data; };
    inline void SetData(void *data) { this->data = data; };

    bool operator==(const char *tag);
    bool operator!=(const char *tag);

protected:
    map<string, string> param;
    string name;
    string text;
    void *data;
};

class blConf;
class blXmlParser
{
public:
    blXmlParser(void);
    ~blXmlParser();
    void Reset(void);
    void Parse(void);
    inline void SetConfigurationObject(blConf *conf) { this->conf = conf; };

    void ParseError(const string &what);
    virtual void ParseElementOpen(blXmlTag *tag) { };
    virtual void ParseElementClose(blXmlTag *tag) { };

    XML_Parser p;
    blConf *conf;
    FILE *fh;
    uint8_t *buffer;
    long page_size;
    vector<blXmlTag *> stack;
};

class blXmlParseException : public blException
{
public:
    explicit blXmlParseException(const char *what,
        uint32_t row, uint32_t col, uint8_t byte)
        : blException(EINVAL, what), row(row), col(col), byte(byte)
        { };
    virtual ~blXmlParseException() throw() { };

    uint32_t row;
    uint32_t col;
    uint8_t byte;
};

class blXmlKeyNotFound : public blException
{
public:
    explicit blXmlKeyNotFound(const char *key)
        : blException(key)
        { };
    virtual ~blXmlKeyNotFound() throw() { };
};

class blConf
{
public:
    blConf(const char *filename, blXmlParser *parser,
        int argc = 0, char *argv[] = NULL);
    virtual ~blConf();

    virtual void Reload(void) { parser->Reset(); };

    inline const char *GetFilename(void) const { return filename.c_str(); };

protected:
    string filename;
    blXmlParser *parser;
    int argc;
    char **argv;
};

#endif // _BLCONF_H
// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
