// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _BLSOCKET_H
#define _BLSOCKET_H

using namespace std;

#define blSocketRetry   80000

class blSocketTimeout : public blException
{
public:
    explicit blSocketTimeout(void)
        : blException("blSocketTimeout") { };
};

class blSocketConnecting : public blException
{
public:
    explicit blSocketConnecting(void)
        : blException("blSocketConnecting") { };
};

class blSocketHangup : public blException
{
public:
    explicit blSocketHangup(void)
        : blException("blSocketHangup") { };
};

class blSocket
{
public:
    enum State
    {
        Init,
        Accepting,
        Accepted,
        Connecting,
        Connected,
    };

    enum Flags
    {
        None,
        WaitAll = 0x1,
    };

    blSocket();
    blSocket(int sd, struct sockaddr_in &sa);
    virtual ~blSocket();

    virtual void Create(void);
    virtual void Close(void);
    int GetDescriptor(void) { return sd; };

    void SetTimeout(time_t tv_sec) {
        timeout = tv_sec;
    };
    void SetWaitAll(bool enable = true) {
        if (enable)
            flags = WaitAll;
        else
            flags = None;
    };

    void Read(size_t &length, uint8_t *buffer);
    void Write(size_t &length, uint8_t *buffer);

protected:
    int sd;
    struct sockaddr_in sa;
    State state;
    Flags flags;
    time_t timeout;
    struct timeval tv_active;
    size_t bytes_read;
    size_t bytes_wrote;
};

class blSocketAccept : public blSocket
{
public:
    blSocketAccept(const string &addr, in_port_t port);

    blSocket *Accept(void);
};

class blSocketConnect : public blSocket
{
public:
    blSocketConnect(const string &host, in_port_t port);

    virtual void Close(void);
    void Connect(void);
};

#endif // _BLSOCKET_H
// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
