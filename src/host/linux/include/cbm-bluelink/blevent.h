// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _BLEVENT_H
#define _BLEVENT_H

using namespace std;

// Reserved event IDs
#define blEVENT_QUIT            0x0000
#define blEVENT_RELOAD          0x0001
#define blEVENT_TIMER           0x0002
#define blEVENT_DEVICE          0x0003
#define blEVENT_GPIB            0x0004
#define blEVENT_USER            0x1000

// Broadcast event client type
#define _BL_EVENT_BROADCAST     ((blEventClient *)-1)

// No event in queue
#define _BL_EVENT_NONE          ((blEvent *)NULL)

typedef unsigned long csevent_id_t;
typedef unsigned long csevent_flag_t;

class blEventClient;
class blEvent
{
public:
    enum Flags
    {
        None = 0x00,
        Exclusive = 0x01,
        HighPriority = 0x02,
        Sticky = 0x04
    };

    blEvent(csevent_id_t id, csevent_flag_t flags = blEvent::None);
    virtual ~blEvent() { };

    virtual blEvent *Clone(void);

    inline csevent_id_t GetId(void) const { return id; };
    inline csevent_flag_t GetFlags(void) const { return flags; };
    inline blEventClient *GetSource(void) const { return src; };
    inline blEventClient *GetTarget(void) const { return dst; };
    inline void SetSource(blEventClient *src) { this->src = src; };
    inline void SetTarget(blEventClient *dst) { this->dst = dst; };

    inline bool IsExclusive(void) {
        return (bool)(flags & blEvent::Exclusive);
    };
    inline bool IsHighPriority(void) {
        return (bool)(flags & blEvent::HighPriority);
    };
    inline bool IsSticky(void) { return (bool)(flags & blEvent::Sticky); };

    inline void SetExclusive(bool enable = true) {
        if (enable) flags |= blEvent::Exclusive;
        else flags &= ~blEvent::Exclusive;
    };
    inline void SetHighPriority(bool enable = true) {
        if (enable) flags |= blEvent::HighPriority;
        else flags &= ~blEvent::HighPriority;
    };
    inline void SetSticky(bool enable = true) {
        if (enable) flags |= blEvent::Sticky;
        else flags &= ~blEvent::Sticky;
    };

    void *GetUserData(void) { return user_data; };
    void SetUserData(void *user_data) { this->user_data = user_data; };

protected:
    csevent_id_t id;
    csevent_flag_t flags;
    blEventClient *src;
    blEventClient *dst;
    void *user_data;
};

class blEventDevice : public blEvent
{
public:
    blEventDevice(const string &type);

    virtual blEvent *Clone(void);

    bool GetValue(const string &key, string &value);
    void SetValue(const string &key, const string &value) {
        key_value[key] = value;
    };

protected:
    map<string, string> key_value;
};

class blEventClient
{
public:
    blEventClient();
    virtual ~blEventClient();

    void EventPush(blEvent *event, blEventClient *src);
    void EventDispatch(blEvent *event, blEventClient *dst);
    inline void EventBroadcast(blEvent *event) {
        EventDispatch(event, _BL_EVENT_BROADCAST);
    };
    bool IsEventsEnabled(void) { return event_enable; };
    inline void EventsEnable(bool enable = true) { event_enable = enable; };

protected:
    blEvent *EventPop(void);
    blEvent *EventPopWait(time_t wait_ms = 0);

    pthread_mutex_t event_queue_mutex;
    pthread_cond_t event_condition;
    pthread_mutex_t event_condition_mutex;

    bool event_enable;

    vector<blEvent *> event_queue;

    static vector<blEventClient *> event_client;
    static pthread_mutex_t *event_client_mutex;
};

#endif // _BLEVENT_H
// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
