// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _BLUTIL_H
#define _BLUTIL_H

class blCriticalSection
{
public:
    blCriticalSection();
    virtual ~blCriticalSection();

    static void Lock(void);
    static void Unlock(void);

protected:
    static blCriticalSection *instance;
    static pthread_mutex_t *mutex;
};

class blRegEx
{
public:
    blRegEx(const char *expr, size_t nmatch = 0, int flags = REG_EXTENDED);
    virtual ~blRegEx();

    int Execute(const char *subject);
    const char *GetMatch(size_t match);

protected:
    regex_t regex;
    regmatch_t *match;
    size_t nmatch;
    char **matches;
};

long blGetPageSize(void);

int blExecute(const string &command);
int blExecute(const string &command, vector<string> &output);

void blHexDump(FILE *fh, const void *data, uint32_t length);

uid_t blGetUserId(const string &user);
void blGetUserName(uid_t uid, string &name);

gid_t blGetGroupId(const string &group);
void blGetGroupName(gid_t gid, string &name);

void blSHA1(const string &filename, uint8_t *digest);

void blHexToBinary(const string &hex, uint8_t *bin, size_t length);
void blBinaryToHex(const uint8_t *bin, string &hex, size_t length);
void blBinaryToHex(const uint8_t *bin, char *hex, size_t length);

#endif // _BLUTIL_H
// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
