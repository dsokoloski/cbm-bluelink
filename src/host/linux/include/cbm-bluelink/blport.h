// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _BLPORT_H
#define _BLPORT_H

class blPortOpenException : public blException
{
public:
    explicit blPortOpenException(int e, const char *s)
        : blException(e, s) { };
};

class blPortDeviceExistsException : public blException
{
public:
    explicit blPortDeviceExistsException(void)
        : blException("device already exists") { };
};

class blPortSelectForReadException : public blException
{
public:
    explicit blPortSelectForReadException(const char *s)
        : blException(s) { };
};

class blPortSelectForReadTimeoutException : public blException
{
public:
    explicit blPortSelectForReadTimeoutException(const char *s)
        : blException(s) { };
};

class blPortSelectForWriteException : public blException
{
public:
    explicit blPortSelectForWriteException(const char *s)
        : blException(s) { };
};

class blPortReadException : public blException
{
public:
    explicit blPortReadException(const char *s)
        : blException(s) { };
};

class blPortReadZeroLengthException : public blException
{
public:
    explicit blPortReadZeroLengthException(const char *s)
        : blException(s) { };
};

class blPortWriteException : public blException
{
public:
    explicit blPortWriteException(const char *s)
        : blException(s) { };
};

class blPortProtocolException : public blException
{
public:
    explicit blPortProtocolException(const char *s)
        : blException(s) { };
};

class blPortPacket
{
public:
    blPortPacket(uint8_t op, uint8_t paddr, uint8_t saddr,
        uint8_t length, const uint8_t *data, bool free_data = false);
    virtual ~blPortPacket();

    struct bl_pkt pkt;
    const uint8_t *data;
    bool free_data;
};

class blPort : public blThread
{
public:
    blPort(const string &id, size_t stack_size, bool debug = false);
    virtual ~blPort();

    virtual void *Entry(void);

    string GetId(void) { return id; };

    void AddDevice(blDevice *device);
    blDevice *FindDevice(uint8_t id);

protected:
    int pd;
    string id;
    bool debug;
    uint8_t firmware_version;
    uint8_t hardware_version;
    struct bl_pkt pkt;
    map<uint8_t, blDevice *> device;
    queue<blPortPacket *> pkt_queue;
    stringstream debug_stream;
    stringstream filename;

    virtual void Open(void) = 0;

    void Read(void *data, ssize_t &length);
    void Write(const void *data, ssize_t &length);

    void ProcessPacket(void);
};

class blPortSerial : public blPort
{
public:
    blPortSerial(const string &id, size_t stack_size,
        bool debug, const string &device, uint32_t baud);
    virtual ~blPortSerial() { };

protected:
    string device;
    uint32_t baud;

    virtual void Open(void);
};

class blPortBluetooth : public blPort
{
public:
    blPortBluetooth(const string &id, size_t stack_size,
        bool debug, const string &bdaddr, uint8_t channel);
    virtual ~blPortBluetooth() { };

protected:
    string bdaddr;
    uint8_t channel;

    virtual void Open(void);
};

#endif // _BLPORT_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
