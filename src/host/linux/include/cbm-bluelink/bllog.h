// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _BLLOG_H
#define _BLLOG_H

using namespace std;

#define _BL_MAX_TIMESTAMP       64

class blLog
{
public:
    enum Type
    {
        StdOut,
        LogFile,
        Syslog
    };

    enum Level
    {
        Info = 0x01,
        Warning = 0x02,
        Error = 0x04,
        Debug = 0x08,

        Everything = (Info | Warning | Error | Debug)
    };

    blLog();
    blLog(const char *filename);
    blLog(const char *ident, int option, int facility);

    Type GetType(void) { return type; };
    FILE *GetStream(void) { return fh; };
    bool operator!=(blLog *log) { return bool(log != this); };

    virtual ~blLog();

    static void Log(Level level, const string &message);
    static void Log(Level level, const char *format, ...);
    static void Log(Level level, const char *format, va_list ap);
    static void LogException(Level level, blException &e);
    static void LogException(blDebugException &e);

    static void SetMask(uint32_t mask) { blLog::logger_mask = mask; };

protected:
    Type type;
    const char *filename;
    FILE *fh;
    const char *ident;
    int option;
    int facility;

    void Initialize(void);

    static vector<blLog *> logger;
    static pthread_mutex_t *logger_mutex;
    static uint32_t logger_mask;
    static char timestamp[_BL_MAX_TIMESTAMP];
};

#endif // _BLLOG_H
// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
