// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _BLDEVICE_H
#define _BLDEVICE_H

using namespace std;

#define _BL_DEVICE_VER  0x20130603

#ifndef _BL_INTERNAL

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/ioctl.h>

#include <stdexcept>
#include <string>
#include <vector>
#include <map>

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <syslog.h>
#include <signal.h>
#include <expat.h>
#include <regex.h>
#include <pwd.h>
#include <grp.h>

#include <netinet/in.h>
#include <netdb.h>
#include <net/if.h>
#include <linux/sockios.h>

#include <cbm-bluelink/blexception.h>
#include <cbm-bluelink/bllog.h>
#include <cbm-bluelink/blconf.h>
#include <cbm-bluelink/blevent.h>
#include <cbm-bluelink/blthread.h>
#include <cbm-bluelink/bltimer.h>
#include <cbm-bluelink/blutil.h>
#include <cbm-bluelink/blthread.h>
#include <cbm-bluelink/blsocket.h>

#endif // !_BL_INTERNAL

#define blDeviceInit(class_name) \
    extern "C" { \
    blDevice *blDeviceInit(uint8_t id, \
        blEventClient *parent, size_t stack_size) { \
        class_name *p = new class_name(id, parent, stack_size); \
        if (p == NULL) return NULL; \
        return dynamic_cast<blDevice *>(p); \
    } }

struct blDeviceStateValue
{
    size_t length;
    uint8_t *value;
};

class blDevice : public blThread
{
public:
    blDevice(uint8_t id, blEventClient *parent, size_t stack_size);
    virtual ~blDevice();

    virtual void *Entry(void) = 0;

    inline uint8_t GetId(void) { return id; };

    void Log(blLog::Level level, const string &message);
    void Log(blLog::Level level, const char *format, ...);

    virtual void SetConfigurationFile(const string &conf_filename) { };

    void SetStateFile(const string &state_file);

    virtual void LoadState(void);
    virtual void SaveState(void);

    bool GetStateVar(const string &key, unsigned long &value);
    bool GetStateVar(const string &key, float &value);
    bool GetStateVar(const string &key, string &value);
    bool GetStateVar(const string &key, size_t &length, uint8_t *value);

    void SetStateVar(const string &key, const unsigned long &value);
    void SetStateVar(const string &key, const float &value);
    void SetStateVar(const string &key, const string &value);
    void SetStateVar(const string &key, size_t length, const uint8_t *value);

protected:
    void SetStateVar(const string &key, struct blDeviceStateValue *var);

    uint8_t id;
    blEventClient *parent;
    FILE *fh_state;
    map<string, struct blDeviceStateValue *> state;
};

#ifdef _BL_INTERNAL

class blDeviceLoader
{
public:
    blDeviceLoader(const string &so_name,
        uint8_t id, blEventClient *parent, size_t stack_size);
    virtual ~blDeviceLoader();

    inline blDevice *GetDevice(void) { return device; };

protected:
    string so_name;
    void *so_handle;
    blDevice *device;
};

#endif // _BL_INTERNAL

#endif // _BLDEVICE_H
// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
