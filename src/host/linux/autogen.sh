#!/bin/sh

find $(pwd) -name configure.ac | xargs touch

# Regenerate configuration files
autoheader || exit 1
libtoolize -ci || exit 1
aclocal -I m4 --install || exit 1
automake --foreign --include-deps --add-missing --copy || exit 1
autoreconf -i || exit 1

