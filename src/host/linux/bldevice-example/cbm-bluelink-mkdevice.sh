#!/bin/bash
# Generate device skeleton from example source.

PREFIX=$(dirname $0)

# Ask for short-name:
if [ $# -gt 0 ]; then BUFFER=$1; shift 1; fi
read -e -i "${BUFFER}" -p "Enter a short name (ex: bldevice-example): " SHORT_NAME
# Ask for long-name (descriptive name):
if [ $# -gt 0 ]; then BUFFER=$1; shift 1; fi
read -e -i "${BUFFER}" -p "Enter a long name (ex: Example, or \"Foo Bar\"): " LONG_NAME

# Sanitize names:
SHORT_NAME=$(echo "${SHORT_NAME}" | sed -e 's/[[:space:]/\*()]*//g')
SHORT_NAME_AM=$(echo ${SHORT_NAME} | sed -e 's/-/_/g')
LONG_NAME=$(echo "${LONG_NAME}" | sed -e 's/["\*()]*//g')
LONG_NAME_CLASS=$(echo "${LONG_NAME}" | sed -e 's/[[:space:]]*//g')

if [ -d "${SHORT_NAME}" ]; then
    echo "./${SHORT_NAME}: exists.  Please remove manually."
    exit 1
fi

echo "Creating device: ${SHORT_NAME} (${LONG_NAME})..."
mkdir -vp "${SHORT_NAME}" || exit 1

FILES="configure.ac bldevice-example.conf.in bldevice-example.spec.in Makefile.am"
SOURCES="bldevice-example.cpp"

for F in $FILES; do
    OUTF=$(echo $F | sed -e "s/bldevice-example/${SHORT_NAME}/g")
    sed \
        -e "s/bldevice-example/${SHORT_NAME}/g" \
        -e "s/bldevice_example/${SHORT_NAME_AM}/g" \
        -e "s/example/${LONG_NAME}/g" \
        -e "s/Example/${LONG_NAME}/g" ${PREFIX}/${F} > "${SHORT_NAME}/${OUTF}"
done

for F in $SOURCES; do
    OUTF=$(echo $F | sed -e "s/bldevice-example/${SHORT_NAME}/g")
    sed \
        -e "s/bldevice-example/${SHORT_NAME}/g" \
        -e "s/bldevice_example/${SHORT_NAME_AM}/g" \
        -e "s/example/${LONG_NAME_CLASS}/g" \
        -e "s/Example/${LONG_NAME_CLASS}/g" ${PREFIX}/${F} > "${SHORT_NAME}/${OUTF}"
done

cp "${PREFIX}/autogen.sh" ${SHORT_NAME}
chmod 755 ${SHORT_NAME}/autogen.sh

echo -e "\nTo generate the appropriate build files:"
echo "# cd ${SHORT_NAME}"
echo "# ./autogen.sh"
echo "# ./configure"

exit 0

