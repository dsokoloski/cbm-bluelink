// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "aconfig.h"
#endif

#include <cbm-bluelink.h>
#include <cbm-bluelink/bldevice.h>

class blDeviceConf;
class blDeviceXmlParser : public blXmlParser
{
public:
    virtual void ParseElementOpen(blXmlTag *tag);
    virtual void ParseElementClose(blXmlTag *tag);
};

class blDeviceExample;
class blDeviceConf : public blConf
{
public:
    blDeviceConf(blDeviceExample *parent,
        const char *filename, blDeviceXmlParser *parser)
        : blConf(filename, parser), parent(parent) { };

    virtual void Reload(void);

protected:
    friend class blDeviceXmlParser;

    blDeviceExample *parent;
};

void blDeviceConf::Reload(void)
{
    blConf::Reload();
    parser->Parse();
}

class blDeviceExample : public blDevice
{
public:
    blDeviceExample(uint8_t id,
        blEventClient *parent, size_t stack_size);
    virtual ~blDeviceExample();

    virtual void SetConfigurationFile(const string &conf_filename);

    virtual void *Entry(void);

protected:
    friend class blDeviceXmlParser;

    blDeviceConf *conf;
};

blDeviceExample::blDeviceExample(uint8_t id,
    blEventClient *parent, size_t stack_size)
    : blDevice(id, parent, stack_size), conf(NULL)
{
    Log(blLog::Debug, "initialized: stack size: %ld", stack_size);
}

blDeviceExample::~blDeviceExample()
{
    Join();

    if (conf) delete conf;
}

void blDeviceExample::SetConfigurationFile(const string &conf_filename)
{
    if (conf == NULL) {
        blDeviceXmlParser *parser = new blDeviceXmlParser();
        conf = new blDeviceConf(this, conf_filename.c_str(), parser);
        parser->SetConfigurationObject(dynamic_cast<blConf *>(conf));
        conf->Reload();
    }
}

void *blDeviceExample::Entry(void)
{
    Log(blLog::Info, "running.");

    unsigned long loops = 0ul;
    GetStateVar("loops", loops);
    Log(blLog::Debug, "loops: %lu", loops);

    blTimer *timer = new blTimer(500, 3, 3, this);
    timer->Start();

    for (bool run = true; run; loops++) {
        blEvent *event = EventPopWait();

        switch (event->GetId()) {
        case blEVENT_QUIT:
            Log(blLog::Info, "terminated.");
            run = false;
            break;

        case blEVENT_TIMER:
            Log(blLog::Debug, "tick: %lu",
                static_cast<blEventTimer *>(event)->GetTimer()->GetId());
            break;
        }

        delete event;
    }

    delete timer;

    SetStateVar("loops", loops);
    Log(blLog::Debug, "loops: %lu", loops);

    return NULL;
}

void blDeviceXmlParser::ParseElementOpen(blXmlTag *tag)
{
    blDeviceConf *_conf = static_cast<blDeviceConf *>(conf);
}

void blDeviceXmlParser::ParseElementClose(blXmlTag *tag)
{
    string text = tag->GetText();
    blDeviceConf *_conf = static_cast<blDeviceConf *>(conf);

    if ((*tag) == "test-tag") {
        if (!stack.size() || (*stack.back()) != "device-plugin")
            ParseError("unexpected tag: " + tag->GetName());
        if (!text.size())
            ParseError("missing value for tag: " + tag->GetName());

        blLog::Log(blLog::Debug, "%s: %s",
            tag->GetName().c_str(), text.c_str());
    }
}

blDeviceInit(blDeviceExample);

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
