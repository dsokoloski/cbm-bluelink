// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _BLMAIN_H
#define _BLMAIN_H

#define _BL_CONF_VERSION        1

#ifndef _BL_MAIN_CONF
#define _BL_MAIN_CONF           "/etc/cbm-bluelink.conf"
#endif

#ifndef _BL_DEVICE_CONF
#define _BL_DEVICE_CONF         "/etc/cbm-bluelink.d"
#endif

#ifndef _BL_DEVICE_STATE
#define _BL_DEVICE_STATE        "/var/state/cbm-bluelink"
#endif

#ifndef _BL_PID_FILE
#define _BL_PID_FILE            "/var/run/cbm-bluelink/cbm-bluelinkd.pid"
#endif

#ifndef PACKAGE_VERSION
#define _BL_VERSION             "0.0"
#else
#define _BL_VERSION             PACKAGE_VERSION
#endif

#define blEXIT_SUCCESS          0
#define blEXIT_INVALID_OPTION   1
#define blEXIT_XML_PARSE_ERROR  2
#define blEXIT_UNHANDLED_EX     3

class blSignalHandler : public blThread
{
public:
    blSignalHandler(blEventClient *parent, const sigset_t &signal_set)
        : blThread(), parent(parent), signal_set(signal_set) { };
    virtual ~blSignalHandler() {
        pthread_kill(id, SIGTERM);
        Join();
    };

    virtual void *Entry(void);
    void Reaper(void);

protected:
    blEventClient *parent;
    sigset_t signal_set;
};

class blMainConf;
class blMainXmlParser : public blXmlParser
{
public:
    blMainXmlParser(void);

    virtual void ParseElementOpen(blXmlTag *tag);
    virtual void ParseElementClose(blXmlTag *tag);
};

class blMain;
class blMainConf : public blConf
{
public:
    blMainConf(blMain *parent, const char *filename,
        blMainXmlParser *parser, int argc, char *argv[]);
    virtual ~blMainConf();

    virtual void Reload(void);

protected:
    friend class blMainXmlParser;

    blMain *parent;
    int version;
    string device_confd;
};

class blMain : public blEventClient
{
public:
    blMain(int argc, char *argv[]);
    virtual ~blMain();

    void Run(void);
    void Usage(bool version = false);

protected:
    friend class blMainXmlParser;

    blLog *log_stdout;
    blLog *log_syslog;
    blLog *log_logfile;
    blMainConf *conf;
    blSignalHandler *sig_handler;
    blThreadTimer *timer_thread;
    map<string, blPort *> port;
    vector<blDeviceLoader *> device_loader;
    //map<blDevice *, vector<string> > device_event_filter;

    //void ParseEventFilter(blDevice *device, const string &text);
    void ValidateConfiguration(void);
    //void DispatchDeviceEvent(blEventDevice *event);
};

class blUsageException : public blException
{
public:
    explicit blUsageException(void)
        : blException("blUsageException") { };
};

class blInvalidOptionException : public blException
{
public:
    explicit blInvalidOptionException(void)
        : blException("blInvalidOptionException") { };
};

#endif // _BLMAIN_H
// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
