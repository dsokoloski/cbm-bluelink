// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "aconfig.h"
#endif

#include <stdexcept>
#include <string>
#include <vector>
#include <map>

#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <regex.h>
#include <pwd.h>
#include <grp.h>

#include <cbm-bluelink/blexception.h>
#include <cbm-bluelink/bllog.h>
#include <cbm-bluelink/blutil.h>
#include <cbm-bluelink/blevent.h>
#include <cbm-bluelink/blthread.h>
#include <cbm-bluelink/bltimer.h>

blThreadTimer *blThreadTimer::instance = NULL;
pthread_mutex_t *blThreadTimer::vector_mutex = NULL;
vector<blTimer *> blThreadTimer::timer_vector;

blTimer::blTimer(cstimer_id_t id,
    time_t value, time_t interval, blEventClient *target)
    : running(false), id(id), value(value),
    interval(interval), target(target)
{
    timer_mutex = new pthread_mutex_t;
    pthread_mutex_init(timer_mutex, NULL);

    blThreadTimer::GetInstance()->AddTimer(this);

    blLog::Log(blLog::Debug,
        "Created timer: id: %lu, value: %ld, interval: %ld",
        id, value, interval);
}

blTimer::~blTimer()
{
    Stop();
    blThreadTimer::GetInstance()->RemoveTimer(this);
    pthread_mutex_destroy(timer_mutex);
}

void blTimer::Start(void)
{
    pthread_mutex_lock(timer_mutex);
    running = true;
    pthread_mutex_unlock(timer_mutex);
}

void blTimer::Stop(void)
{
    pthread_mutex_lock(timer_mutex);
    running = false;
    pthread_mutex_unlock(timer_mutex);
}

void blTimer::SetValue(time_t value)
{
    pthread_mutex_lock(timer_mutex);
    this->value = value;
    pthread_mutex_unlock(timer_mutex);
    blLog::Log(blLog::Debug,
        "Set timer value: id: %lu, value: %ld, interval: %ld",
        id, value, interval);
}

void blTimer::SetInterval(time_t interval)
{
    pthread_mutex_lock(timer_mutex);
    this->interval = interval;
    pthread_mutex_unlock(timer_mutex);
    blLog::Log(blLog::Debug,
        "Set timer interval: id: %lu, value: %ld, interval: %ld",
        id, value, interval);
}

void blTimer::Extend(time_t value)
{
    pthread_mutex_lock(timer_mutex);
    this->value += value;
    pthread_mutex_unlock(timer_mutex);
    blLog::Log(blLog::Debug,
        "Extend timer value: id: %lu, value: %ld (+%ld), interval: %ld",
        id, this->value, value, interval);
}

time_t blTimer::GetInterval(void)
{
    return interval;
}

time_t blTimer::GetRemaining(void)
{
    time_t _remaining = 0;
    pthread_mutex_lock(timer_mutex);
    _remaining = value;
    pthread_mutex_unlock(timer_mutex);
    return _remaining;
}

blThreadTimer::blThreadTimer(blEventClient *parent, const sigset_t &signal_set)
    : blThread(), parent(parent), signal_set(signal_set)
{
    if (instance != NULL)
        throw blException(EEXIST, "blThreadTimer");

    if (vector_mutex == NULL) {
        vector_mutex = new pthread_mutex_t;
        pthread_mutex_init(vector_mutex, NULL);
        instance = this;
    }

    memset(&sev, 0, sizeof(struct sigevent));
    sev.sigev_notify = SIGEV_SIGNAL;
    sev.sigev_signo = SIGRTMIN;

    if (timer_create(CLOCK_REALTIME, &sev, &timer_id) < 0)
        throw blException(errno, "timer_create");

    it_spec.it_value.tv_sec = 1;
    it_spec.it_value.tv_nsec = 0;
    it_spec.it_interval.tv_sec = 1;
    it_spec.it_interval.tv_nsec = 0;
}

blThreadTimer::~blThreadTimer()
{
    Join();

    if (instance != this) return;
    timer_delete(timer_id);
    pthread_mutex_destroy(vector_mutex);
    delete vector_mutex;
    vector_mutex = NULL;
}

void *blThreadTimer::Entry(void)
{
    int sig;
    siginfo_t si;
    struct timespec timeout;

    timeout.tv_sec = 1;
    timeout.tv_nsec = 0;

    if (timer_settime(timer_id, 0, &it_spec, NULL) , 0)
        throw blException(errno, "timer_settime");

    blLog::Log(blLog::Debug, "Timer thread started.");

    for ( ;; ) {
        blEvent *event = EventPop();

        if (event != _BL_EVENT_NONE) {
            switch (event->GetId()) {
            case blEVENT_QUIT:
                blLog::Log(blLog::Debug, "Timer thread terminated.");
                delete event;
                return NULL;

            default:
                blLog::Log(blLog::Debug,
                    "Timer: unhandled event: %u", event->GetId());
                delete event;
            }
        }
                
        sig = sigtimedwait(&signal_set, &si, &timeout);
        if (sig < 0) {
            if (errno == EINTR || errno == EAGAIN)
                continue;

            blLog::Log(blLog::Error,
                "Timer: sigtimedwait: %s", strerror(errno));
            EventBroadcast(new blEvent(blEVENT_QUIT,
                blEvent::Sticky | blEvent::HighPriority));

            return NULL;
        }

        if (sig == sev.sigev_signo) Tick();
        else blLog::Log(blLog::Warning,
            "Timer: unhandled signal: %s", strsignal(sig));
    }

    return NULL;
}

void blThreadTimer::AddTimer(blTimer *timer)
{
    pthread_mutex_lock(vector_mutex);
    timer_vector.push_back(timer);
    pthread_mutex_unlock(vector_mutex);
}

void blThreadTimer::RemoveTimer(blTimer *timer)
{
    pthread_mutex_lock(vector_mutex);
    for (vector<blTimer *>::iterator i = timer_vector.begin();
        i != timer_vector.end(); i++) {
        if ((*i) != timer) continue;
        timer_vector.erase(i);
        break;
    }
    pthread_mutex_unlock(vector_mutex);
}

void blThreadTimer::Tick(void)
{
    pthread_mutex_lock(vector_mutex);
    for (vector<blTimer *>::iterator i = timer_vector.begin();
        i != timer_vector.end(); i++) {
        pthread_mutex_lock((*i)->timer_mutex);
        if ((*i)->running) {
            if (--(*i)->value <= 0) {
                blEventClient *target = (*i)->target;
                if (target == NULL) target = parent;
                EventDispatch(new blEventTimer((*i)), target);

                (*i)->value = (*i)->interval;
                if ((*i)->value > 0) (*i)->running = true;
            }
        }
        pthread_mutex_unlock((*i)->timer_mutex);
    }
    pthread_mutex_unlock(vector_mutex);
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
