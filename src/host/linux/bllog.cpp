// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "aconfig.h"
#endif

#include <stdexcept>
#include <string>
#include <vector>

#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <pthread.h>
#include <time.h>

#include <cbm-bluelink/blexception.h>
#include <cbm-bluelink/bllog.h>

vector<blLog *> blLog::logger;
pthread_mutex_t *blLog::logger_mutex = NULL;
uint32_t blLog::logger_mask = blLog::Everything;
char blLog::timestamp[_BL_MAX_TIMESTAMP];

blLog::blLog()
    : type(blLog::StdOut),
    filename(NULL), fh(NULL),
    ident(NULL), option(-1), facility(-1)
{
    Initialize();
}

blLog::blLog(const char *filename)
    : type(blLog::LogFile),
    filename(filename), fh(NULL),
    ident(NULL), option(-1), facility(-1)
{
    fh = fopen(filename, "a+");
    if (fh == NULL) throw blException(errno, "fopen");
    Initialize();
}

blLog::blLog(const char *ident, int option, int facility)
    : type(blLog::Syslog),
    filename(NULL), fh(NULL),
    ident(ident), option(option), facility(facility)
{
    size_t syslog_count = 0;

    if (blLog::logger_mutex != NULL) {
        vector<blLog *>::iterator i;
        pthread_mutex_lock(blLog::logger_mutex);
        for (i = blLog::logger.begin(); i != blLog::logger.end(); i++) {
            if ((*i)->GetType() == blLog::Syslog) syslog_count++;
        }
        pthread_mutex_unlock(blLog::logger_mutex);
    }
    if (syslog_count != 0) throw blException("Syslog logger already exists.");
    openlog(ident, option, facility);
    Initialize();
}

blLog::~blLog()
{
    size_t syslog_count = 0;

    if (blLog::logger_mutex != NULL) {
        vector<blLog *>::iterator i;
        pthread_mutex_lock(blLog::logger_mutex);
        for (i = blLog::logger.begin(); i != blLog::logger.end(); i++) {
            if ((*i) != this) continue;
            blLog::logger.erase(i);
            break;
        }
        for (i = blLog::logger.begin(); i != blLog::logger.end(); i++) {
            if ((*i)->GetType() == blLog::Syslog) syslog_count++;
        }
        size_t logger_count = (size_t)blLog::logger.size();
        pthread_mutex_unlock(blLog::logger_mutex);
        if (logger_count == 0) {
            pthread_mutex_destroy(blLog::logger_mutex);
            delete blLog::logger_mutex;
            blLog::logger_mutex = NULL;
        }
    }

    switch (type) {
    case blLog::StdOut:
        break;
    case blLog::LogFile:
        if (fh) fclose(fh);
        break;
    case blLog::Syslog:
        if (syslog_count == 0) closelog();
        break;
    }
}

void blLog::Log(Level level, const string &message)
{
    blLog::Log(level, message.c_str());
}

void blLog::Log(Level level, const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    blLog::Log(level, format, ap);

    va_end(ap);
}

void blLog::Log(Level level, const char *format, va_list ap)
{
    if (!(level & blLog::logger_mask) ||
        blLog::logger_mutex == NULL) return;

    pthread_mutex_lock(blLog::logger_mutex);

    blLog *handler = blLog::logger.back();
    if (!handler) {
        // XXX: Should *never* be...
        pthread_mutex_unlock(blLog::logger_mutex);
        return;
    }

    if (handler->GetType() == blLog::StdOut ||
        handler->GetType() == blLog::LogFile) {
        FILE *stream = NULL;
        if (handler->GetType() == blLog::LogFile)
            stream = handler->GetStream();
        else {
            if ((level & (blLog::Info | blLog::Warning)))
                stream = stdout;
            else
                stream = stderr;
        }
        time_t now = time(NULL);
        struct tm tm_now;
        localtime_r(&now, &tm_now);
        if ((handler->GetType() == blLog::LogFile ||
            (blLog::Debug & logger_mask)) &&
            strftime(timestamp, _BL_MAX_TIMESTAMP,
            "[%d/%b/%Y:%T %z]", &tm_now) > 0) {
            fputs(timestamp, stream);
            fputc(' ', stream);
        }
        if ((level & blLog::Warning))
            fputs("[Warning]: ", stream);
        else if ((level & blLog::Error))
            fputs("[Error]: ", stream);
        else if ((level & blLog::Debug))
            fputs("[Debug]: ", stream);

        vfprintf(stream, format, ap);
        fputc('\n', stream);
    }
    else if (handler->GetType() == blLog::Syslog) {
        int priority;
        if ((level & blLog::Warning))
            priority = LOG_WARNING;
        else if ((level & blLog::Error))
            priority = LOG_ERR;
        else if ((level & blLog::Debug))
            priority = LOG_DEBUG;
        else
            priority = LOG_INFO;

        vsyslog(priority, format, ap);
    }

    pthread_mutex_unlock(blLog::logger_mutex);
}

void blLog::LogException(Level level, blException &e)
{
}

void blLog::LogException(blDebugException &e)
{
}

void blLog::Initialize(void)
{
    if (blLog::logger_mutex == NULL) {
        blLog::logger_mutex = new pthread_mutex_t;
        pthread_mutex_init(logger_mutex, NULL);
    }
    pthread_mutex_lock(blLog::logger_mutex);
    blLog::logger.push_back(this);
    pthread_mutex_unlock(blLog::logger_mutex);
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
