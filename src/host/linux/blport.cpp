// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "aconfig.h"
#endif

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <errno.h>
#include <syslog.h>

#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <queue>
#include <sstream>

#include <expat.h>
#include <pthread.h>
#include <fcntl.h>
#include <termios.h>
#include <regex.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>

#include <cbm-bluelink.h>

#include <cbm-bluelink/blexception.h>
#include <cbm-bluelink/bllog.h>
#include <cbm-bluelink/blevent.h>
#include <cbm-bluelink/blthread.h>
#include <cbm-bluelink/bltimer.h>
#include <cbm-bluelink/bldevice.h>
#include <cbm-bluelink/blutil.h>
#include <cbm-bluelink/blport.h>

#ifdef _BL_DEBUG
#include "testprg.h"
#endif

static uint8_t c = 0;

#define LIST_LEN 36
static uint8_t list[LIST_LEN] = {
    0x01, 0x04,         // Starting memory address
    0x21, 0x04,         // Pointer to next statement
    0x00, 0x00,         // Line number
    0x12, '"',          // Reverse on
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    '"', ' ',
    '0', '0', ' ', '2', 'A', ' ',
    0x12,               // Reverse off
    0x00,               // End of line

    0x00, 0x00          // End of program
};

blPortPacket::blPortPacket(uint8_t op,
    uint8_t paddr = 0, uint8_t saddr = 0,
    uint8_t length = 0, const uint8_t *data = NULL,
    bool free_data)
{
    pkt.op = op | BLOP_ID_MASK;
    pkt.paddr = paddr;
    pkt.saddr = saddr;
    pkt.length = length;
    this->data = data;
    this->free_data = free_data;
}

blPortPacket::~blPortPacket()
{
    if (data != NULL && free_data) delete [] data;
}

blPort::blPort(const string &id, size_t stack_size, bool debug)
    : blThread(stack_size), pd(-1), id(id), debug(debug),
    firmware_version(0xFF), hardware_version(0xFF)
{
    memset(&pkt, 0, sizeof(struct bl_pkt));
}

blPort::~blPort()
{
    Join();

    if (pd != -1) close(pd);
    for (map<uint8_t, blDevice *>::iterator i = device.begin();
        i != device.end(); i++) delete i->second;
    while (!pkt_queue.empty()) {
        delete pkt_queue.front();
        pkt_queue.pop();
    }
}

void blPort::AddDevice(blDevice *device)
{
    map<uint8_t, blDevice *>::iterator i;
    i = this->device.find(device->GetId());
    if (i != this->device.end())
        throw blPortDeviceExistsException();
    this->device[device->GetId()] = device;
}

blDevice *blPort::FindDevice(uint8_t id)
{
    map<uint8_t, blDevice *>::iterator i;
    i = device.find(id);
    if (i == device.end()) return NULL;
    return i->second;
}

void *blPort::Entry(void)
{
    bool run = true;
    ssize_t pkt_data_length = 0;

    blTimer *retry_timer = new blTimer(1, 3, 3, this);
    retry_timer->Start();

    for (map<uint8_t, blDevice *>::iterator i = device.begin();
        i != device.end(); i++) i->second->Start();

    while (run) {
        blEvent *event;
        ssize_t length = 1;

        if (pd != -1 ) {
            try {
                while (!pkt_queue.empty()) {
                    length = sizeof(struct bl_pkt);
                    Write((const void *)&pkt_queue.front()->pkt, length);
                    if (pkt_queue.front()->pkt.length > 0 &&
                        pkt_queue.front()->data != NULL) {
                        length = pkt_queue.front()->pkt.length;
                        Write((const void *)pkt_queue.front()->data, length);
                        blLog::Log(blLog::Debug, "%s: Pkt queue wrote: %d",
                            id.c_str(), length);
                    }
                    delete pkt_queue.front();
                    pkt_queue.pop();
                }
            } catch (blPortSelectForWriteException &e) {
                blLog::Log(blLog::Error, "%s: error waiting to write: %s",
                    id.c_str(), e.estring.c_str());
            } catch (blPortWriteException &e) {
                blLog::Log(blLog::Error, "%s: error writing: %s",
                    id.c_str(), e.estring.c_str());
            }
        }

        if (pd != -1 ) {
            try {
                uint8_t *pkt_ptr = (uint8_t *)&pkt;

                length = 1;
                Read((void *)(pkt_ptr + pkt_data_length), length);
                //::blHexDump(stderr, pkt_ptr + pkt_data_length, length);

                if (pkt_data_length == 0 && !(pkt.op & BLOP_ID_MASK)) {
                    blLog::Log(blLog::Debug,
                        "%s: Flushing invalid first byte: 0x%02x",
                        id.c_str(), pkt.op);
                }
                else {
                    if (++pkt_data_length == sizeof(struct bl_pkt)) {
                        pkt_data_length = 0;
                        ProcessPacket();
                    }
                }
            } catch (blPortSelectForReadException &e) {
                blLog::Log(blLog::Warning, "%s: Select for read exception: %s",
                    id.c_str(), e.estring.c_str());
            } catch (blPortSelectForReadTimeoutException &e) {
            } catch (blPortReadException &e) {
                blLog::Log(blLog::Warning, "%s: Read exception: %s",
                    id.c_str(), e.estring.c_str());
            } catch (blPortReadZeroLengthException &e) {
            } catch (blPortProtocolException &e) {
                blLog::Log(blLog::Warning, "%s: Protocol exception: %s",
                    id.c_str(), e.estring.c_str());
            }
        
            event = EventPop();
            if (event == _BL_EVENT_NONE) continue;
        }
        else event = EventPopWait();

        switch (event->GetId()) {
        case blEVENT_QUIT:
            run = false;

            break;
        case blEVENT_TIMER:
            try {
                if (pd == -1)
                    Open();
                else if (
                    firmware_version == 0xFF ||
                    hardware_version == 0xFF) {
                    pkt_queue.push(new blPortPacket(BLOP_VERSION));
                    pkt_queue.push(new blPortPacket(BLOP_DEBUG, (uint8_t)debug));
                    for (map<uint8_t, blDevice *>::iterator i = device.begin();
                        i != device.end(); i++) {
                            pkt_queue.push(
                                new blPortPacket(BLOP_REGISTER, i->second->GetId()));
                    }
                }
                else
                    pkt_queue.push(new blPortPacket(BLOP_STATUS));
            } catch (blPortOpenException &e) {
                blLog::Log(blLog::Warning, "%s: Open port: %s",
                    id.c_str(), strerror(e.eint));
            }

        case blEVENT_GPIB:

            break;
        default:
            blLog::Log(blLog::Debug,
                "Unhandled event: %u", event->GetId());

            break;
        }

        delete event;
    }

    delete retry_timer;

    return NULL;
}

void blPort::Read(void *data, ssize_t &length)
{
    int rc;
    fd_set fds;
    struct timeval tv;
    ssize_t bytes_read = 0;

    FD_ZERO(&fds);
    FD_SET(pd, &fds);
    tv.tv_sec = 1;
    tv.tv_usec = 0;

    rc = select(pd + 1, &fds, NULL, NULL, &tv);
    switch (rc) {
    case -1:
        throw blPortSelectForReadException(strerror(errno));
    case 0:
        throw blPortSelectForReadTimeoutException("timed-out");
    }

    if (!FD_ISSET(pd, &fds))
        throw blPortSelectForReadException("unexpected");

    bytes_read = read(pd, data, length);
    switch (bytes_read) {
    case -1:
        length = 0;
        if (errno == EIO) {
            firmware_version = 0xFF;
            hardware_version = 0xFF;
            close(pd);
            pd = -1;
        }
        throw blPortReadException(strerror(errno));
    case 0:
        length = 0;
        throw blPortReadZeroLengthException("zero-length read");
    }

    //::blHexDump(stderr, data, bytes_read);

    if (bytes_read != length) {
        length = bytes_read;
        throw blPortReadException("short read");
    }

    length = bytes_read;
}

void blPort::Write(const void *data, ssize_t &length)
{
    int rc;
    fd_set fds;
    struct timeval tv;
    ssize_t bytes_wrote = 0;

    //::blHexDump(stderr, data, length);

    FD_ZERO(&fds);
    FD_SET(pd, &fds);
    tv.tv_sec = 0;
    tv.tv_usec = 100;

    rc = select(pd + 1, NULL, &fds, NULL, &tv);
    switch (rc) {
    case -1:
        throw blPortSelectForWriteException(strerror(errno));
    case 0:
        throw blPortSelectForWriteException("timed-out");
    }

    if (!FD_ISSET(pd, &fds))
        throw blPortSelectForWriteException("unexpected");

    bytes_wrote = write(pd, data, length);
    switch (bytes_wrote) {
    case -1:
        length = 0;
        if (errno == EIO) {
            firmware_version = 0xFF;
            hardware_version = 0xFF;
            close(pd);
            pd = -1;
        }
        throw blPortWriteException(strerror(errno));
    case 0:
        length = 0;
        throw blPortWriteException("zero-length write");
    }

    if (bytes_wrote != length) {
        length = bytes_wrote;
        throw blPortWriteException("short write");
    }
}

void blPort::ProcessPacket(void)
{
    switch ((pkt.op &
        ~(BLOP_ID_MASK | BLOP_FLAG_EOF | BLOP_FLAG_RES1))) {
    case BLOP_READ:
        filename.str("");
        for (uint8_t i = 0; i < pkt.length; i++) {
            char c;
            ssize_t length = 1;
            Read((void *)&c, length);
            filename << c;
        }

        if (filename.str().length() == 0) {
            pkt_queue.push(
                new blPortPacket(BLOP_READ | BLOP_FLAG_EOF, 0, 0,
                    1, (const uint8_t *)&c));
            c++;
            break;
        }

        blLog::Log(blLog::Debug, "%s: Read, filename: %s",
            id.c_str(), filename.str().c_str());

        if (filename.str() == "$") {
            pkt_queue.push(
                new blPortPacket(BLOP_READ, 0, 0,
                    LIST_LEN - 1, (const uint8_t *)list));
            pkt_queue.push(
                new blPortPacket(BLOP_READ | BLOP_FLAG_EOF, 0, 0,
                    1, (const uint8_t *)(list + LIST_LEN - 1)));
        }
#ifdef _BL_DEBUG
        else if (filename.str() == "TEST") {

            uint32_t length = testprg_len - 1, v = 76;
            for (uint32_t i = 0; length > 0; ) {
                //printf("%d %% %d = %d, i = %d\n", v, length, v % length, i);
                pkt_queue.push(
                    new blPortPacket(BLOP_READ, 0, 0,
                        v, (const uint8_t *)testprg + i));
                length -= v % length;
                i += v;

                if (length < v) {
                    //printf("%d < %d = %d, i = %d\n", v, length, 0, i);
                    pkt_queue.push(
                        new blPortPacket(BLOP_READ, 0, 0,
                            length, (const uint8_t *)testprg + i));
                    break;
                }
            }

            pkt_queue.push(
                new blPortPacket(BLOP_READ | BLOP_FLAG_EOF, 0, 0,
                    1, (const uint8_t *)(testprg + testprg_len - 1)));
        }
#endif
        break;

    case BLOP_WRITE:
        break;

    case BLOP_RESET:
        if (firmware_version != 0xFF) {
            firmware_version = 0xFF;
            hardware_version = 0xFF;
            blLog::Log(blLog::Info, "%s: Interface clear", id.c_str());
        }

        break;
    case BLOP_VERSION:
        if (pkt.paddr == 0x00 || pkt.saddr == 0x00 ||
            pkt.paddr == 0xFF || pkt.saddr == 0xFF)
            throw blPortProtocolException("invalid version data");
        if (pkt.length != 0)
            throw blPortProtocolException("invalid packet data");

        firmware_version = pkt.paddr;
        hardware_version = pkt.saddr;

        blLog::Log(blLog::Info,
            "%s: Firmware and hardware version 0x%02hhx:0x%02hx",
            id.c_str(), firmware_version, hardware_version);

        //::blHexDump(stderr, (const void *)&pkt, length);

        break;
    case BLOP_STATUS:
        if (pkt.length != 0)
            throw blPortProtocolException("invalid packet data");

        blLog::Log(blLog::Info,
            "%s: Controller %sdetected",
            id.c_str(), (pkt.paddr & BL_STATUS_CTRL) ? "" : "not ");

        //::blHexDump(stderr, (const void *)&pkt, length);

        break;

    case BLOP_DEBUG:
        for (uint8_t i = 0; i < pkt.length; i++) {
            char c;
            ssize_t length = 1;
            Read((void *)&c, length);
            debug_stream << c;
        }
        if (pkt.op & BLOP_FLAG_EOF) {
            blLog::Log(blLog::Debug, "%s: %s",
                id.c_str(), debug_stream.str().c_str());
            debug_stream.str("");
        }
            
        break;

    case BLOP_ERROR:
        if (pkt.paddr < BLERR_MAX) {
            blLog::Log(blLog::Error,
                "%s: %s (0x%02X:0x%02X)",
                id.c_str(), BLERR_string[pkt.paddr],
                pkt.paddr, pkt.saddr);
        }
        else {
            blLog::Log(blLog::Error,
                "%s: Error detected: 0x%02X:0x%02X",
                id.c_str(), pkt.paddr, pkt.saddr);
        }
        break;

    default:
        throw blPortProtocolException("unexpected op code");
    }
}

blPortSerial::blPortSerial(const string &id, size_t stack_size,
    bool debug, const string &device, uint32_t baud)
    : blPort(id, stack_size, debug), device(device), baud(baud) { }

void blPortSerial::Open(void)
{
    struct termios tty;

    pd = open(device.c_str(), O_RDWR | O_NOCTTY | O_SYNC);
    if (pd < 0)
        throw blPortOpenException(errno, device.c_str());

    memset(&tty, 0, sizeof(struct termios));
    if (tcgetattr(pd, &tty) != 0)
        throw blPortOpenException(errno, device.c_str());

    cfsetospeed(&tty, baud);
    cfsetispeed(&tty, baud);

    // 8-bit characters
    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;
    // Ignore break signal
    tty.c_iflag &= ~IGNBRK;
    // No signaling chars, no echo,and no canonical processing
    tty.c_lflag = 0;
    // No remapping, no delays
    tty.c_oflag = 0;
    // Read doesn't block
    tty.c_cc[VMIN]  = 0;
    // 0.5 seconds read timeout
    tty.c_cc[VTIME] = 5;
    // Shut off XON/XOFF control
    tty.c_iflag &= ~(IXON | IXOFF | IXANY);
    // Ignore modem controls, enable reading
    tty.c_cflag |= (CLOCAL | CREAD);
    // Disable parity
    tty.c_cflag &= ~(PARENB | PARODD);
    // Stop bit
    tty.c_cflag &= ~CSTOPB;
    // Enable RTS/CTS 
    tty.c_cflag |= CRTSCTS;

    if (tcsetattr(pd, TCSANOW, &tty) != 0)
        throw blPortOpenException(errno, device.c_str());
}

blPortBluetooth::blPortBluetooth(const string &id, size_t stack_size,
    bool debug, const string &bdaddr, uint8_t channel)
    : blPort(id, stack_size, debug), bdaddr(bdaddr), channel(channel) { }

void blPortBluetooth::Open(void)
{
    struct sockaddr_rc addr;

    memset(&addr, 0, sizeof(struct sockaddr_rc));
    addr.rc_family = AF_BLUETOOTH;
    addr.rc_channel = channel;
    str2ba(bdaddr.c_str(), &addr.rc_bdaddr);

    pd = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
    if (pd == -1) {
        blLog::Log(blLog::Warning, "%s: socket: %s", bdaddr.c_str(),
            strerror(errno));
        throw blPortOpenException(errno, bdaddr.c_str());
    }

    if (connect(pd, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
        close(pd); pd = -1;
        throw blPortOpenException(errno, bdaddr.c_str());
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
