// CBM/BlueLink: Linux Host Daemon
// Copyright (C) 2013 Darryl Sokoloski <http://cbm-bluelink.com/>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "aconfig.h"
#endif

#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <queue>
#include <sstream>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>

#include <unistd.h>
#include <getopt.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <syslog.h>
#include <signal.h>
#include <expat.h>
#include <limits.h>
#include <dirent.h>
#include <regex.h>
#include <pwd.h>
#include <grp.h>

#define OPENSSL_THREAD_DEFINES
#include <openssl/opensslconf.h>
#ifndef OPENSSL_THREADS
#error "OpenSSL missing thread support"
#endif
#include <openssl/crypto.h>

#include <cbm-bluelink.h>

#include <cbm-bluelink/blexception.h>
#include <cbm-bluelink/bllog.h>
#include <cbm-bluelink/blconf.h>
#include <cbm-bluelink/blevent.h>
#include <cbm-bluelink/blutil.h>
#include <cbm-bluelink/blthread.h>
#include <cbm-bluelink/bltimer.h>
#include <cbm-bluelink/bldevice.h>
#include <cbm-bluelink/blport.h>

#include "blmain.h"

static pthread_mutex_t **blCryptoMutex = NULL;

static void cs_crypto_lock(int mode, int n, const char *file, int line)
{
    if (blCryptoMutex == NULL) {
        blLog::Log(blLog::Error, "libcrypto mutexes not initialized!");
        return;
    }

    if (mode & CRYPTO_LOCK)
        pthread_mutex_lock(blCryptoMutex[n]);
    else
        pthread_mutex_unlock(blCryptoMutex[n]);
}

void *blSignalHandler::Entry(void)
{
    int sig;
    siginfo_t si;

    blLog::Log(blLog::Debug, "Signal handler started.");

    for ( ;; ) {
        sig = sigwaitinfo(&signal_set, &si);
        if (sig < 0) {
            blLog::Log(blLog::Error, "sigwaitinfo: %s", strerror(errno));
            if (errno == EINTR) {
                usleep(100 * 1000);
                continue;
            }
            EventBroadcast(new blEvent(blEVENT_QUIT,
                blEvent::Sticky | blEvent::HighPriority));
            return NULL;
        }
        blLog::Log(blLog::Debug, "Signal received: %s", strsignal(sig));
        switch (sig) {
        case SIGINT:
        case SIGTERM:
            EventBroadcast(new blEvent(blEVENT_QUIT,
                blEvent::Sticky | blEvent::HighPriority));
            return NULL;

        case SIGHUP:
            EventBroadcast(new blEvent(blEVENT_RELOAD));
            break;

        case SIGCHLD:
            Reaper();
            break;

        default:
            blLog::Log(blLog::Warning,
                "Unhandled signal: %s", strsignal(sig));
        }
    }

    return NULL;
}

void blSignalHandler::Reaper()
{
    pid_t pid;
    int status;
    while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
        if (WIFEXITED(status)) {
            blLog::Log(blLog::Debug,
                "Process exited with code: %d: %d",
                pid, WEXITSTATUS(status));
        } else if (WIFSIGNALED(status)) {
            blLog::Log(blLog::Debug,
                "Process exited by signal: %d: %s",
                pid, strsignal(WTERMSIG(status)));
        }
        else
            blLog::Log(blLog::Warning,
                "Process exited abnormally: %d", pid);
    }
}

blMainXmlParser::blMainXmlParser(void)
    : blXmlParser() { }

void blMainXmlParser::ParseElementOpen(blXmlTag *tag)
{
    blMainConf *_conf = static_cast<blMainConf *>(conf);

    if ((*tag) == "blconf") {
        if (stack.size() != 0)
            ParseError("unexpected tag: " + tag->GetName());
        if (!tag->ParamExists("version"))
            ParseError("version parameter missing");

        _conf->version = atoi(tag->GetParamValue("version").c_str());
        blLog::Log(blLog::Debug,
            "Configuration version: %d", _conf->version);
        if (_conf->version > _BL_CONF_VERSION)
            ParseError("unsupported version, too new");
    }
    else if ((*tag) == "ports") {
        if (!stack.size() || (*stack.back()) != "blconf")
            ParseError("unexpected tag: " + tag->GetName());
    }
    else if ((*tag) == "serial") {
        if (!stack.size() || (*stack.back()) != "ports")
            ParseError("unexpected tag: " + tag->GetName());
        if (!tag->ParamExists("id"))
            ParseError("id parameter missing");
        if (!tag->ParamExists("device"))
            ParseError("device parameter missing");
        if (!tag->ParamExists("baud"))
            ParseError("baud parameter missing");

        bool debug = false;
        if (tag->ParamExists("debug") && strcasecmp(
            tag->GetParamValue("debug").c_str(), "true") == 0)
            debug = true;

        map<string, blPort *>::iterator i;
        i = _conf->parent->port.find(tag->GetParamValue("id"));
        if (i != _conf->parent->port.end())
            ParseError("duplicate serial port: " + tag->GetParamValue("id"));

        blPortSerial *port = NULL;
        uint32_t baud = (uint32_t)atol(
            tag->GetParamValue("baud").c_str());

        port = new blPortSerial(tag->GetParamValue("id"),
                _BL_THREAD_STACK_SIZE * 2, debug,
                tag->GetParamValue("device"), baud);
        _conf->parent->port[tag->GetParamValue("id")] = port;

        blLog::Log(blLog::Debug,
            "Port: %s (%s), baud: %u",
            tag->GetParamValue("id").c_str(),
            tag->GetParamValue("device").c_str(), baud);
        tag->SetData((void *)port);
    }
    else if ((*tag) == "bluetooth") {
        if (!stack.size() || (*stack.back()) != "ports")
            ParseError("unexpected tag: " + tag->GetName());
        if (!tag->ParamExists("id"))
            ParseError("id parameter missing");
        if (!tag->ParamExists("address"))
            ParseError("address parameter missing");
        if (!tag->ParamExists("channel"))
            ParseError("channel parameter missing");

        bool debug = false;
        if (tag->ParamExists("debug") && strcasecmp(
            tag->GetParamValue("debug").c_str(), "true") == 0)
            debug = true;

        map<string, blPort *>::iterator i;
        i = _conf->parent->port.find(tag->GetParamValue("id"));
        if (i != _conf->parent->port.end())
            ParseError("duplicate bluetooth port: " + tag->GetParamValue("id"));

        blPortBluetooth *port = NULL;

        port = new blPortBluetooth(
                tag->GetParamValue("id"),
                _BL_THREAD_STACK_SIZE * 2, debug,
                tag->GetParamValue("address"),
                (uint8_t)atoi(tag->GetParamValue("channel").c_str())
        );
        _conf->parent->port[tag->GetParamValue("id")] = port;

        blLog::Log(blLog::Debug,
            "Port: %s (%s)",
            tag->GetParamValue("id").c_str(),
            tag->GetParamValue("address").c_str());
        tag->SetData((void *)port);
    }
    else if ((*tag == "device")) {
        size_t stack_size = _BL_THREAD_STACK_SIZE;

        if (!stack.size() || ((*stack.back()) != "serial" &&
            (*stack.back()) != "bluetooth"))
            ParseError("unexpected tag: " + tag->GetName());
        if (!tag->ParamExists("id"))
            ParseError("id parameter missing");
        if (!tag->ParamExists("plugin"))
            ParseError("plugin parameter missing");
        if (!tag->ParamExists("conf"))
            ParseError("conf parameter missing");
        if (tag->ParamExists("stack-size")) {
            stack_size = (size_t)atol(
                tag->GetParamValue("stack-size").c_str());
            if (stack_size < PTHREAD_STACK_MIN)
                stack_size = PTHREAD_STACK_MIN;
            else if (stack_size % ::blGetPageSize())
                stack_size += (stack_size % ::blGetPageSize());
        }

        uint8_t id;
        id = (uint8_t)atoi(tag->GetParamValue("id").c_str());

        // Device IDs between 4 - 30 (inclusive) are valid,
        // with the exception of #10 which is reserved for the
        // CBM/BlueLink adapter.
        if (id < 4 || id > BL_GPIB_DEVICES - 2 || id == 10)
            ParseError("invalid device id: " + tag->GetParamValue("id"));

        blPort *port = reinterpret_cast<blPort *>
            (stack.back()->GetData());
        if (port == NULL) ParseError("unexpected error");

        if (port->FindDevice(id) != NULL)
            ParseError("duplicate device: " + tag->GetParamValue("id"));

        blDeviceLoader *device = NULL;

        try {
            device = new blDeviceLoader(
                tag->GetParamValue("plugin"),
                id, _conf->parent, stack_size);
        } catch (blException &e) {
            blLog::Log(blLog::Error, "Device loader failed: %s",
                e.estring.c_str());
        }

        if (device != NULL) {
            try {
                string filename = string(_BL_DEVICE_CONF) +
                    string("/") + tag->GetParamValue("conf");
                device->GetDevice()->SetConfigurationFile(filename);
                tag->SetData(device->GetDevice());

                blLog::Log(blLog::Debug,
                    "Device #%s (%s), stack size: %ld",
                    tag->GetParamValue("id").c_str(),
                    tag->GetParamValue("plugin").c_str(), stack_size);

                _conf->parent->device_loader.push_back(device);
                port->AddDevice(device->GetDevice());

            } catch (blPortDeviceExistsException &e) {
                blLog::Log(blLog::Error,
                    "Device #%s already exists",
                    tag->GetParamValue("id").c_str());
                delete device->GetDevice();
            } catch (blException &e) {
                blLog::Log(blLog::Error,
                    "Device #%s configuration error: %s: %s",
                    tag->GetParamValue("id").c_str(),
                    e.estring.c_str(), e.what());
                delete device;
            }
        }
    }
}

void blMainXmlParser::ParseElementClose(blXmlTag *tag)
{
    string text = tag->GetText();
    blMainConf *_conf __attribute__((unused));
    _conf = static_cast<blMainConf *>(conf);
/*
    if ((*tag) == "state-file") {
        if (!stack.size() || (*stack.back()) != "device-plugin")
            ParseError("unexpected tag: " + tag->GetName());
        if (!text.size())
            ParseError("missing value for tag: " + tag->GetName());

        blDevice *device = reinterpret_cast<blDevice *>
            (stack.back()->GetData());
        if (device != NULL)
            device->SetStateFile(text);
    }
    else if ((*tag) == "event-filter") {
        if (!stack.size() || (*stack.back()) != "device-plugin")
            ParseError("unexpected tag: " + tag->GetName());
        if (!text.size())
            ParseError("missing value for tag: " + tag->GetName());

        blDevice *device = reinterpret_cast<blDevice *>
            (stack.back()->GetData());
        if (device != NULL)
            _conf->parent->ParseEventFilter(device, text);
    }
*/
}

blMainConf::blMainConf(blMain *parent,
    const char *filename, blMainXmlParser *parser,
    int argc, char *argv[])
    : blConf(filename, parser, argc, argv),
    parent(parent), version(-1), device_confd(_BL_DEVICE_CONF) { }

blMainConf::~blMainConf() { }

blMain::blMain(int argc, char *argv[])
    : blEventClient(), log_syslog(NULL), log_logfile(NULL)
{
    bool debug = false;
    string conf_filename = _BL_MAIN_CONF;
    string log_file;
    sigset_t signal_set;

    log_stdout = new blLog();
    log_stdout->SetMask(blLog::Info | blLog::Warning | blLog::Error);

    int rc;
    static struct option options[] =
    {
        { "version", 0, 0, 'V' },
        { "config", 1, 0, 'c' },
        { "debug", 0, 0, 'd' },
        { "log", 1, 0, 'l' },
        { "help", 0, 0, 'h' },

        { NULL, 0, 0, 0 }
    };

    for (optind = 1;; ) {
        int o = 0;
        if ((rc = getopt_long(argc, argv,
            "Vc:dl:h?", options, &o)) == -1) break;
        switch (rc) {
        case 'V':
            Usage(true);
        case 'c':
            conf_filename = optarg;
            break;
        case 'd':
            debug = true;
            log_stdout->SetMask(
                blLog::Info | blLog::Warning | blLog::Error | blLog::Debug);
            break;
        case 'l':
            log_file = optarg;
            break;
        case '?':
            blLog::Log(blLog::Info,
                "Try %s --help for more information.", argv[0]);
            throw blInvalidOptionException();
        case 'h':
            Usage();
            break;
        }
    }

    if (!debug) {
        if (daemon(1, 0) != 0)
            throw blException(errno, "daemon");
        log_syslog = new blLog("cbm-bluelinkd", LOG_PID, LOG_DAEMON);

        FILE *h_pid = fopen(_BL_PID_FILE, "w+");
        if (h_pid == NULL) {
            blLog::Log(blLog::Warning, "Error saving PID file: %s",
                _BL_PID_FILE);
        }
        else {
            if (fprintf(h_pid, "%d\n", getpid()) <= 0) {
                blLog::Log(blLog::Warning, "Error saving PID file: %s",
                    _BL_PID_FILE);
            }
            fclose(h_pid);
        }
    }

    int crypto_locks = CRYPTO_num_locks();
    if (crypto_locks > 0) {
        blCryptoMutex = new pthread_mutex_t *[crypto_locks];
        for (int i = 0; i < crypto_locks; i++) {
            blCryptoMutex[i] = new pthread_mutex_t;
            pthread_mutex_init(blCryptoMutex[i], NULL);
        }
        CRYPTO_set_locking_callback(cs_crypto_lock);
        blLog::Log(blLog::Debug,
            "Initialized %d libcrypto locks.", crypto_locks);
    }

    sigemptyset(&signal_set);
    blLog::Log(blLog::Debug,
        "Real-time signals: %d", SIGRTMAX - SIGRTMIN);
    for (int sigrt = SIGRTMIN; sigrt <= SIGRTMAX; sigrt++)
        sigaddset(&signal_set, sigrt);

    timer_thread = new blThreadTimer(this, signal_set);

    blMainXmlParser *parser = new blMainXmlParser();
    conf = new blMainConf(this, conf_filename.c_str(), parser, argc, argv);
    parser->SetConfigurationObject(dynamic_cast<blConf *>(conf));

    conf->Reload();
    ValidateConfiguration();

    sigfillset(&signal_set);
    sigdelset(&signal_set, SIGPROF);

    if ((rc = pthread_sigmask(SIG_BLOCK, &signal_set, NULL)) != 0)
        throw blException(rc, "pthread_sigmask");

    sigemptyset(&signal_set);
    sigaddset(&signal_set, SIGINT);
    sigaddset(&signal_set, SIGHUP);
    sigaddset(&signal_set, SIGTERM);
    sigaddset(&signal_set, SIGPIPE);
    sigaddset(&signal_set, SIGCHLD);
    sigaddset(&signal_set, SIGALRM);
    sigaddset(&signal_set, SIGUSR1);
    sigaddset(&signal_set, SIGUSR2);

    sig_handler = new blSignalHandler(this, signal_set);
    sig_handler->EventsEnable(false);
    sig_handler->Start();

    timer_thread->Start();

    for (map<string, blPort *>::iterator i = port.begin();
        i != port.end(); i++) {
        i->second->Start();
    }

    blLog::Log(blLog::Info,
        PACKAGE_NAME " v" _BL_VERSION " initialized.");
}

blMain::~blMain()
{
    for (map<string, blPort *>::iterator i = port.begin();
        i != port.end(); i++) {
        delete i->second;
    }

    for (vector<blDeviceLoader *>::iterator i = device_loader.begin();
        i != device_loader.end(); i++) delete (*i);

    if (sig_handler) delete sig_handler;
    if (timer_thread) delete timer_thread;
    if (conf) delete conf;
    blLog::Log(blLog::Info, "Terminated.");
    if (log_logfile) delete log_logfile;
    if (log_syslog) delete log_syslog;
    if (log_stdout) delete log_stdout;
}

void blMainConf::Reload(void)
{
    blLog::Log(blLog::Debug, "Reload configuration.");
    blConf::Reload();
    parser->Parse();
}
#if 0
void blMain::ParseEventFilter(blDevice *device, const string &text)
{
    size_t prev = 0;
    size_t next = text.find('|');
    vector<string> substr;

    while (next != string::npos) {
        string atom = text.substr(prev, next - prev);
        substr.push_back(atom);
        prev = ++next;
        next = text.find('|', prev);
    }

    if (text.length() > prev) {
        string atom = text.substr(prev);
        substr.push_back(atom);
    }

    for (vector<string>::iterator i = substr.begin();
        i != substr.end(); i++) {
        size_t length = (*i).length();
        if (length == 0) continue;
        size_t head = (*i).find_first_not_of(' ');
        size_t tail = (*i).find_last_not_of(' ');
        if (head == string::npos) head = 0;
        string atom = (*i).substr(head, tail + 1 - head);
        if (strcasecmp(atom.c_str(), device->GetId().c_str()) == 0) {
            blLog::Log(blLog::Warning,
                "You can not add a device to it's own event filter: %s",
                atom.c_str());
            continue;
        }
        device_event_filter[device].push_back(atom);
    }
}
#endif
void blMain::ValidateConfiguration(void)
{
#if 0
    for (map<blDevice *,
        vector<string> >::iterator i = device_event_filter.begin();
        i != device_event_filter.end(); i++) {
        for (vector<string>::iterator j = i->second.begin();
            j != i->second.end(); j++) {
            map<string, blDeviceLoader *>::iterator p;
            p = device.find((*j));
            if (p != device.end()) continue;
            blLog::Log(blLog::Warning,
                "Event filter device not found: %s", (*j).c_str());
        }
    }
#endif
}
/*
void blMain::DispatchDeviceEvent(blEventDevice *event)
{
    blDevice *device = static_cast<blDevice *>(event->GetSource());
    event->SetValue("event_source", device->GetId());
    for (map<blDevice *, vector<string> >::iterator i = device_event_filter.begin();
        i != device_event_filter.end(); i++) {
        for (vector<string>::iterator j = i->second.begin();
            j != i->second.end(); j++) {
            if (strcasecmp(device->GetId().c_str(), (*j).c_str()))
                continue;
            map<string, blDeviceLoader *>::iterator device_loader;
            device_loader = this->device.find(i->first->GetId());
            if (device_loader == this->device.end()) continue;
            EventDispatch(event->Clone(), device_loader->second->GetDevice());
            break;
        }
    }
}
*/
void blMain::Run(void)
{
    bool run = true;

    while (run) {
        blEvent *event = EventPopWait();

        switch (event->GetId()) {
        case blEVENT_QUIT:
            blLog::Log(blLog::Debug, "Terminating...");
            run = false;

            break;
#if 0
        case blEVENT_DEVICE:
            DispatchDeviceEvent(static_cast<blEventDevice *>(event));

            break;
#endif
        case blEVENT_RELOAD:
            //conf->Reload();

            break;
        default:
            blLog::Log(blLog::Debug,
                "Unhandled event: %u", event->GetId());

            break;
        }

        delete event;
    }
}

void blMain::Usage(bool version)
{
    blLog::Log(blLog::Info, PACKAGE_NAME " v%s", _BL_VERSION);
    blLog::Log(blLog::Info,
        "Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.com>");
    if (version) {
        blLog::Log(blLog::Info,
            "  This program comes with ABSOLUTELY NO WARRANTY.");
        blLog::Log(blLog::Info,
            "  This is free software, and you are welcome to redistribute it");
        blLog::Log(blLog::Info,
            "  under certain conditions according to the GNU General Public");
        blLog::Log(blLog::Info,
            "  License version 3, or (at your option) any later version.");
#ifdef PACKAGE_BUGREPORT
        blLog::Log(blLog::Info, "Report bugs to: %s", PACKAGE_BUGREPORT);
#endif
#ifdef PACKAGE_URL
        blLog::Log(blLog::Info, "For more information, see: %s", PACKAGE_URL);
#endif
    }
    else {
        blLog::Log(blLog::Info,
            "  -V, --version");
        blLog::Log(blLog::Info,
            "    Display program version and license information.");
        blLog::Log(blLog::Info,
            "  -c <file>, --config <file>");
        blLog::Log(blLog::Info,
            "    Specify an alternate configuration file.");
        blLog::Log(blLog::Info,
            "    Default: %s", _BL_MAIN_CONF);
        blLog::Log(blLog::Info,
            "  -d, --debug");
        blLog::Log(blLog::Info,
            "    Enable debugging messages and remain in the foreground.");
    }

    throw blUsageException();
}

int main(int argc, char *argv[])
{
    blMain *cs_main = NULL;
    int rc = blEXIT_SUCCESS;

    try {
        cs_main = new blMain(argc, argv);

        cs_main->Run();

    } catch (blUsageException &e) {
    } catch (blInvalidOptionException &e) {
        rc = blEXIT_INVALID_OPTION;
    } catch (blXmlParseException &e) {
        blLog::Log(blLog::Error,
            "XML parse error, %s on line: %u, column: %u, byte: 0x%02x",
            e.estring.c_str(), e.row, e.col, e.byte);
        rc = blEXIT_XML_PARSE_ERROR;
    } catch (blException &e) {
        blLog::Log(blLog::Error,
            "%s: %s.", e.estring.c_str(), e.what());
        rc = blEXIT_UNHANDLED_EX;
    }

    if (cs_main) delete cs_main;

    return rc;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
