#!/bin/bash
# Shortcut script for building and testing the RPM package.

[ -z "$TARGET_ARCH" ] && TARGET_ARCH=x86_64
[ -z "$TARGET_HOST" ] && TARGET_HOST=root@gw.lan

RELEASE=$(egrep -i '^release:' cbm-bluelink.spec.in |\
	sed -e 's/^release:[[:space:]]*\([0-9]*\).*/\1/i')
if [ -z "RELEASE" ]; then
	echo "No release found in: cbm-bluelink.spec.in"
	exit 1
fi

sed -i -e "s/^release: $RELEASE/Release: $[ $RELEASE + 1 ]/i" \
	cbm-bluelink.spec.in

rm -f $HOME/rpmbuild/RPMS/$TARGET_ARCH/cbm-bluelink-*.rpm

[ ! -f configure ] && ./autogen.sh

./configure && \
	make dist-gzip && \
	mv cbm-bluelink-1.0.tar.gz ~/rpmbuild/SOURCES/ && \
	rpmbuild -ba cbm-bluelink.spec && \
	ssh $TARGET_HOST rm -f cbm-bluelink*.rpm && \
	scp $HOME/rpmbuild/RPMS/$TARGET_ARCH/cbm-bluelink-*.rpm $TARGET_HOST: &&
	ssh $TARGET_HOST yum --assumeyes localinstall cbm-bluelink-*.rpm
exit $?
