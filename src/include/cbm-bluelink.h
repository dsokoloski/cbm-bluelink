// Common project-wide declarations
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#ifndef _BLUELINK_H
#define _BLUELINK_H

#define BL_GPIB_FILELEN		16
#define BL_GPIB_DEVICES		32

struct bl_pkt
{
    // Operation
    uint8_t op;
    // Primary address
    uint8_t paddr;
    // Secondary address
    uint8_t saddr;
    // Data length
    uint8_t length;
};

#define BLOP_ID_MASK        0xC0
#define BLOP_FLAG_EOF       0x10
#define BLOP_FLAG_RES1      0x20

#define BLOP_RESET          0x01
#define BLOP_VERSION        0x02
#define BLOP_STATUS         0x03
#define BL_STATUS_CTRL      0x01
#define BLOP_REGISTER       0x04
#define BLOP_OPEN           0x05
#define BLOP_LOAD           0x06
#define BLOP_SAVE           0x07
#define BLOP_CLOSE          0x08
#define BLOP_READ           0x09
#define BLOP_WRITE          0x0A
#define BLOP_RES1           0x0B
#define BLOP_RES2           0x0C
#define BLOP_RES3           0x0D
#define BLOP_DEBUG          0x0E
#define BLOP_ERROR          0x0F
#define BLOP_MAX            BLOP_ERROR

#define BLERR_INV_OP        0x01
#define BLERR_INV_DEVID     0x02
#define BLERR_GPIB_READ     0x03
#define BLERR_GPIB_WRITE    0x04
#define BLERR_MAX           0x05

#ifdef _BL_INTERNAL
const char *BLERR_string[] = {
    "Unknown error",
    "Invalid operation ID",
    "Invalid device ID",
    "GPIB read error",
    "GPIB write error",

    NULL
};
#endif // _BL_INTERNAL

#endif // _BLUELINK_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
