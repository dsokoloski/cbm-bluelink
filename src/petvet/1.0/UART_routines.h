/*
    UART_routines.h
    Serial/UART Routines in the PETvet device
    Copyright (C) 2012 Michael Hill

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact the author at bitfixer@bitfixer.com
    http://bitfixer.com
    
*/
#ifndef _UART_ROUTINES_H
#define _UART_ROUTINES_H

#define CHAR 1
#define INT 2
#define LONG 3

void uart0_disable();
void uart0_init(unsigned int ubrr);
unsigned char receiveByte( void );
void transmitByte( unsigned char data );
void transmitHex( unsigned char dataType, unsigned long data );
void transmitString_F(char* string);
void transmitString(char* string);

#endif // _UART_ROUTINES_H
