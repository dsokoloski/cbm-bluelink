Everything you need to build a new version of the PETvet firmware is here, including changing ROM images and memory maps.

I have not included the ROM files for the PET, they are downloadable elsewhere. The specific ROMs you need for the BASIC 2.0 and 4.0 sets are specified in convertroms.php.

convertroms.php is a script which takes a group of ROM files and converts them into a format to allow the PETvet to use selectable sets of ROMs.
To run convertroms.php you will need a php interpreter installed. You can download php from www.php.net.

To compile new firmware from scratch, you will need to download AVR Studio from Atmel.

Good luck and please let me know if you have any questions.
- Mike Hill
bitfixer@bitfixer.com

