/*
    PETramrom.c
    Main program for the PETvet RAM/ROM replacement and diagnostic device
    Copyright (C) 2012 Michael Hill

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact the author at bitfixer@bitfixer.com
    http://bitfixer.com
*/

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/pgmspace.h>
#include "UART_routines.h"
//#include "romdata_basic2.h"
//#include "romdata_basic4.h"
//#include "memmap.h"
//#include "ROMset.h"
#include "romsets.h"

#define MRAM_SEL    0x08
// MRAM_SEL will no longer be used for selecting the chip
#define SRAM_EN     0x08

#define ADDREN      0x04
#define RDY         0x02
#define SRAMSEL     0x01
#define EN245       0x01

#define RW          0x02
#define MRAM_WE     0x01

#define MEM_RANGE   1
#define MEM_SCREEN  2
#define EXIT        3

unsigned char get_diag_option();
void display_memory_range();
void display_screen_memory();

unsigned char get_data2(unsigned int addr);
unsigned int memmap_offset;
unsigned setname[10];

//#define DEBUG 0

void put_data(unsigned int addr, unsigned char data)
{
    unsigned int temp;
    unsigned char c,a,d;
    unsigned char cpart,apart,dpart;
    
    temp = addr;
    // get a bits (6)
    temp = temp & 0xFC00;
    temp = temp >> 8;
    a = temp;
    
    temp = addr;
    // get d bits (6)
    temp = temp & 0x03F0;
    temp = temp >> 2;
    d = temp;
    
    temp = addr;
    // get c bits (4)
    temp = temp & 0x000F;
    temp = temp << 4;
    c = temp;
    
    //apart = a | 0x01;
    
    // porta: include address bits, lower WE for mapping ram
    apart = a & 0xFE;
    dpart = d;
    //cpart = MRAM_SEL | 0x03 | c;
    
    // portc: enable SRAM, RDY high, isolate data bus
    //cpart = SRAMSEL | RDY | c;
    cpart = EN245 | RDY | SRAM_EN | c;
    
    // release data lines
    DDRB = 0x00;
    
    // enable ram chip, disable mapping RAM
    PORTC = cpart | ADDREN;
    PORTD = dpart;
    
    // set ram to write, complete address
    PORTA = apart;
    
    // enable data lines
    DDRB = 0xFF;
    
    // output data
    PORTB = data;
    
    // wait
    asm("nop    \n\t");
    asm("nop    \n\t");
    
    // disable write
    //PORTA = apart | 0x02;
    PORTA = apart | RW;
    
    // release data lines
    DDRB = 0x00;
    
    // disable ram chip
    //PORTC = MRAM_SEL | c | ADDREN;
    PORTC = EN245 | RDY | c | ADDREN;
}

void loadROM(unsigned char setting)
{
    unsigned int offset;
    unsigned int a,b;
    unsigned char data;
    
    unsigned char setnum;
    //unsigned char setname[10];
    unsigned char numroms;
    unsigned int flashoffset;
    unsigned char temp1,temp2;
    
    unsigned int romflashaddr;
    unsigned int romdestaddr;
    unsigned int romlength;
    unsigned int thisaddr;
    
    // set PORTA to output
    DDRA = 0xFF;
    // set PORTD to output
    DDRD = 0xFF;
    // set data to input
    DDRB = 0x00;
    DDRC = 0xFF;
    
    //uart0_init(255);
    flashoffset = 0;
    // load ROMset info
    // skip over previous ROM sets
    
    for (a = 0; a < setting; a++)
    {
        //transmitString("skipping ROM set\r\n");
        // read number of roms
        numroms = pgm_read_byte_near(romdata + 11 + flashoffset);
        //transmitHex(CHAR, numroms);
        // increment offset by the size of this header
        flashoffset += (12 + 2*numroms + 2);
        //transmitString("\r\n");
        //transmitHex(INT, flashoffset);
    }
    
    
    setnum = pgm_read_byte_near(romdata + flashoffset);
    for (a = 0; a < 10; a++)
    {
        setname[a] = pgm_read_byte_near(romdata + flashoffset + a + 1);
    }
    numroms = pgm_read_byte_near(romdata + flashoffset + 11);
    
    // get memory map address
    temp1 = pgm_read_byte_near(romdata + flashoffset + 12 + 2*numroms);
    temp2 = pgm_read_byte_near(romdata + flashoffset + 13 + 2*numroms);
    
    memmap_offset = temp1;
    memmap_offset = memmap_offset << 8;
    memmap_offset = memmap_offset | temp2;
    
    //transmitHex(INT, memmap_offset);
    
    for (a = 0; a < numroms; a++)
    {
        // get rom address
        temp1 = pgm_read_byte_near(romdata + flashoffset + 12 + (a*2));
        temp2 = pgm_read_byte_near(romdata + flashoffset + 13 + (a*2));
        
        romflashaddr = temp1;
        romflashaddr = romflashaddr << 8;
        romflashaddr = romflashaddr | temp2;
        
        /*
        transmitString("\r\n");
        transmitHex(INT, romflashaddr);
        transmitString("\r\n");
        */
        
        // now get rom information
        temp1 = pgm_read_byte_near(romdata + romflashaddr);
        temp2 = pgm_read_byte_near(romdata + romflashaddr+1);
        
        romdestaddr = temp1;
        romdestaddr = romdestaddr << 8;
        romdestaddr = romdestaddr | temp2;
        
        /*
        transmitString("\r\n");
        transmitHex(INT, romdestaddr);
        transmitString("\r\n");
        */
        
        // now get rom length
        temp1 = pgm_read_byte_near(romdata + romflashaddr+2);
        temp2 = pgm_read_byte_near(romdata + romflashaddr+3);
        
        romlength = temp1;
        romlength = romlength << 8;
        romlength = romlength | temp2;
        
        /*
        transmitString("rom length: ");
        transmitHex(INT, romlength);
        transmitString("\r\n");
        */
        
        // now write rom contents
        for (b = 0; b < romlength; b++)
        {
            data = pgm_read_byte_near(romdata + romflashaddr + 4 + b);
            put_data(romdestaddr + b, data);
        }
    }
    
    // ===== DEBUG
    
    /*
    transmitString("set number \r\n");
    transmitHex(CHAR, setnum);
    transmitString("\r\nsetname :");
    transmitString(setname);
    transmitString("\r\nnumroms: ");
    transmitHex(CHAR, numroms);
    */

    //while(1){}
    // end DEBUG
    
    /*
    offset = 0;
    // b rom
    for (a = 0xb000; a < 0xc000; a++)
    {
        data = pgm_read_byte_near(brom_basic4 + offset);
        offset++;
        put_data(a, data);
    }
    */

    /*
    if (setting == 0)
    {
    
        offset = 0;
        // b rom
        for (a = 0xb000; a < 0xc000; a++)
        {
            //data = pgm_read_byte_near(brom_basic4 + offset);
            data = 0x00;
            offset++;
            put_data(a, data);
        }

        offset = 0;
        // c rom
        for (a = 0xc000; a < 0xd000; a++)
        {
            data = pgm_read_byte_near(crom_basic2 + offset);
            offset++;
            put_data(a, data);
        }
        
        offset = 0;
        // d rom
        for (a = 0xd000; a < 0xe000; a++)
        {
            data = pgm_read_byte_near(drom_basic2 + offset);
            offset++;
            put_data(a, data);
        }
        
        offset = 0;
        // e rom
        for (a = 0xe000; a < 0xe800; a++)
        {
            data = pgm_read_byte_near(erom_basic2 + offset);
            offset++;
            put_data(a, data);
        }
        
        //offset = 0;
        a = 0xf000;
        for (offset = 0; offset < 4096; offset++)
        {
            data = pgm_read_byte_near(f_rom_basic2 + offset);
            put_data(a, data);
            a++;
        }
    
    }
    else
    {
        offset = 0;
        // b rom
        for (a = 0xb000; a < 0xc000; a++)
        {
            data = pgm_read_byte_near(brom_basic4 + offset);
            offset++;
            put_data(a, data);
        }
    
        offset = 0;
        // c rom
        for (a = 0xc000; a < 0xd000; a++)
        {
            data = pgm_read_byte_near(crom_basic4 + offset);
            offset++;
            put_data(a, data);
        }
        
        offset = 0;
        // d rom
        for (a = 0xd000; a < 0xe000; a++)
        {
            data = pgm_read_byte_near(drom_basic4 + offset);
            offset++;
            put_data(a, data);
        }
        
        offset = 0;
        // e rom
        for (a = 0xe000; a < 0xe800; a++)
        {
            data = pgm_read_byte_near(erom_basic4 + offset);
            offset++;
            put_data(a, data);
        }
        
        //offset = 0;
        a = 0xf000;
        for (offset = 0; offset < 4096; offset++)
        {
            data = pgm_read_byte_near(f_rom_basic4 + offset);
            put_data(a, data);
            a++;
        }
    }
    */


}

void checkROM(unsigned char setting)
{
    unsigned int offset;
    unsigned int a,b;
    unsigned char data,dd;
    
    unsigned char setnum;
    unsigned char setname[10];
    unsigned char numroms;
    unsigned int flashoffset;
    unsigned char temp1,temp2;
    
    unsigned int romflashaddr;
    unsigned int romdestaddr;
    unsigned int romlength;
    unsigned int thisaddr;
    
    // set PORTA to output
    DDRA = 0xFF;
    // set PORTD to output
    DDRD = 0xFF;
    // set data to input
    DDRB = 0x00;
    DDRC = 0xFF;
    
    //uart0_init(255);
    flashoffset = 0;
    // load ROMset info
    // skip over previous ROM sets
    
    for (a = 0; a < setting; a++)
    {
        //transmitString("skipping ROM set\r\n");
        // read number of roms
        numroms = pgm_read_byte_near(romdata + 11 + flashoffset);
        //transmitHex(CHAR, numroms);
        // increment offset by the size of this header
        flashoffset += (12 + 2*numroms + 2);
        //transmitString("\r\n");
        //transmitHex(INT, flashoffset);
    }
    
    
    setnum = pgm_read_byte_near(romdata + flashoffset);
    for (a = 0; a < 10; a++)
    {
        setname[a] = pgm_read_byte_near(romdata + flashoffset + a + 1);
    }
    numroms = pgm_read_byte_near(romdata + flashoffset + 11);
    
    
    for (a = 0; a < numroms; a++)
    {
    
        // get rom address
        temp1 = pgm_read_byte_near(romdata + flashoffset + 12 + (a*2));
        temp2 = pgm_read_byte_near(romdata + flashoffset + 13 + (a*2));
        
        romflashaddr = temp1;
        romflashaddr = romflashaddr << 8;
        romflashaddr = romflashaddr | temp2;
        
        
        transmitString("\r\n");
        transmitHex(INT, romflashaddr);
        transmitString("\r\n");
        
        
        // now get rom information
        temp1 = pgm_read_byte_near(romdata + romflashaddr);
        temp2 = pgm_read_byte_near(romdata + romflashaddr+1);
        
        romdestaddr = temp1;
        romdestaddr = romdestaddr << 8;
        romdestaddr = romdestaddr | temp2;
    
        
        transmitString("\r\n");
        transmitHex(INT, romdestaddr);
        transmitString("\r\n");
        
        
        // now get rom length
        temp1 = pgm_read_byte_near(romdata + romflashaddr+2);
        temp2 = pgm_read_byte_near(romdata + romflashaddr+3);
        
        romlength = temp1;
        romlength = romlength << 8;
        romlength = romlength | temp2;
        
        
        transmitString("rom length: ");
        transmitHex(INT, romlength);
        transmitString("\r\n");
        
        
        // now write rom contents
        for (b = 0; b < romlength; b++)
        {
            data = pgm_read_byte_near(romdata + romflashaddr + 4 + b);
            //put_data(romdestaddr + b, data);
            
            dd = get_data2(romdestaddr + b);
            
            if (data != dd)
            {
                transmitString("mismatch at ");
                transmitHex(INT, romdestaddr + b);
                transmitString("should be ");
                transmitHex(CHAR, data);
                transmitString(" read ");
                transmitHex(CHAR, dd);
                transmitString("\r\n");
            }
        }
    }

}


void loadMappingRAM()
{
    unsigned int off;
    unsigned int addr;
    unsigned char mapbyte;
    unsigned char sramenable;
    unsigned char dd;
    
    // first set ram address to 0 and deselect the SRAM
    PORTD = 0x00;
    PORTC = ADDREN;
    DDRA = 0xFF;
    //PORTA = 0x02;
    PORTA = 0xFF;

    off = 0;
    for (addr = 0; addr < 256; addr += 2)
    //for (addr = 0; addr < 80; addr +=2)
    {
        dd = pgm_read_byte_near(romdata + memmap_offset + off);
        off++;
    
        // disable the SRAM
        PORTC = ADDREN;
        DDRC = 0xff;
        //DDRC = 0x01 | ADDREN | SRAM_EN;
        
        // porta: set MRAM to write
        DDRA = 0xff;
        //PORTA = addr | MRAM_WE;
        
        // drive the bits on portc - SRAM will be disabled if write protected
        PORTC = dd | ADDREN;
        
        //DDRC = 0x01 | ADDREN | MRAM_SEL;
        //PORTC = dd | ADDREN;
        PORTA = addr;
        
        asm("nop\n\t");
        asm("nop\n\t");
        asm("nop\n\t");
        
        // stop write on MRAM, raise read line on SRAM
        PORTA = addr | MRAM_WE;

        asm("nop\n\t");
        
        // disable SRAM again
        PORTC = ADDREN;
        
        PORTA = 0xFF;
    }
}

unsigned char get_data(unsigned int addr)
{
    unsigned int temp;
    unsigned char c,a,d;
    unsigned char cpart,apart,dpart;
    unsigned char inbyte;
    
    temp = addr;
    // get a bits (6)
    temp = temp & 0xFC00;
    temp = temp >> 8;
    a = temp;
    
    temp = addr;
    // get d bits (6)
    temp = temp & 0x03F0;
    temp = temp >> 2;
    d = temp;
    
    temp = addr;
    // get c bits (4)
    temp = temp & 0x000F;
    temp = temp << 4;
    c = temp;
    
    // output the address
    PORTD = d;
    PORTC = c | ADDREN;
    PORTA = a | MRAM_WE;
    
    /*
    // enable reading
    temp = PINC;
    temp = temp & 0x09;
    if (temp == 0x00)
    {
        
    }
    */
    PORTA = a;
    DDRC = 0xF0 | RDY | EN245 | SRAM_EN | ADDREN;
    PORTC = c | ADDREN | EN245 | SRAM_EN;
    
    
    // wait
    asm("nop\n\t");
    asm("nop\n\t");
    
    inbyte = PINB;
    
    PORTC = c | ADDREN;
    
    asm("nop\n\t");
    asm("nop\n\t");
    
    PORTA = a | MRAM_WE;
    DDRC = 0x0F | RDY | ADDREN;
    
    return inbyte;
}


unsigned char get_data2(unsigned int addr)
{
    unsigned int temp;
    unsigned char c,a,d;
    unsigned char cpart,apart,dpart;
    unsigned char inbyte;
    
    temp = addr;
    // get a bits (6)
    temp = temp & 0xFC00;
    temp = temp >> 8;
    a = temp;
    
    temp = addr;
    // get d bits (6)
    temp = temp & 0x03F0;
    temp = temp >> 2;
    d = temp;
    
    temp = addr;
    // get c bits (4)
    temp = temp & 0x000F;
    temp = temp << 4;
    c = temp;
    
    //apart = a | 0x03;
    //porta: enable write on mapping ram to disable output
    apart = a | MRAM_WE | RW;
    
    dpart = d;
    //portc: enable SRAM, isolate address bus
    //cpart = MRAM_SEL | SRAMSEL | ADDREN | c;
    cpart = EN245 | RDY | SRAM_EN | ADDREN | c;
    
    // enable ram chip, disable mapping RAM
    PORTC = cpart;
    PORTD = dpart;
    PORTA = apart;
    
    // wait
    asm("nop    \n\t");
    asm("nop    \n\t");
    inbyte = PINB;
    
    return inbyte;
}

int main(void)
{
    unsigned char sw1,sw2,sw3,sw4,switchsetting;
    
    // check for switch settings
    uart0_disable();

    DDRC = 0x00;
    PORTC = 0x00;
    
    // all PORTA lines set to input
    DDRA = 0x00;
    // all PORTD lines set to input
    DDRD = 0x00;
    
    PORTA = 0x00;
    PORTD = 0x00;
    PORTC = 0x00;
    
    // read values on A and D
    sw1 = PIND;
    sw2 = PINA;
    sw3 = PINC;
    
    DDRC = ADDREN;
    PORTC = ADDREN;
    
    // check for switch settings
    sw1 = sw1 & 0x02;
    sw2 = sw2 & 0x01;
    
    sw4 = sw3 & 0x08;
    sw3 = sw3 & 0x04;
    
    // invert high bit switch
    if (sw4 == 0)
    {
        sw4 = 0x08;
    }
    else
    {
        sw4 = 0x00;
    }
    
    // invert low bit switch
    if (sw1 == 0)
    {
        sw1 = 0x02;
    }
    else 
    {
        sw1 = 0x00;
    }

    switchsetting = (sw4 | sw3 | sw1) >> 1;

    //switchsetting = ((sw3 & 0x0C) | sw1 | sw2) >> 1;
    //switchsetting = ((sw3 & 0x0c) | sw1 | sw2);
    //switchsetting = 0x00;
    //switchsetting = 0xff;
    
    // disable the pullup resistors
    PORTA = 0x00;
    PORTD = 0x00;
    // set PORTA lines to output
    DDRA = 0xFF;
    
    unsigned int addr,off;
    unsigned char dd,data;
    
#ifdef DEBUG
    while(1) {
#endif
    
    // load values into ROM
    loadROM(switchsetting);
    
    // set data to input
    DDRB = 0x00;
    DDRA = 0xFF;
    DDRC = 0xFF;
    DDRD = 0xFF;
    
    // now write values to the mapping ram
    loadMappingRAM();
    
#ifdef DEBUG
    uart0_init(255);
    
    /*
    transmitString("\r\nMRAM\r\n");
    
    DDRC = ADDREN;
    
    int offset;
    offset = 0;
    for (addr = 1; addr <= 256; addr+=2)
    {
        // load this address onto PORTA
        PORTA = addr;
        
        asm("nop\r\n");
        asm("nop\r\n");
        asm("nop\r\n");
        asm("nop\r\n");
        asm("nop\r\n");
        asm("nop\r\n");
        
        dd = PINC;
        dd = dd & 0x09;
        
        data = pgm_read_byte_near(memmap + offset);
        offset++;
        
        if (data != dd)
        {
            transmitString("mram mismatch at addr: ");
            transmitHex(CHAR, (addr-1));
            transmitHex(CHAR, data);
            transmitHex(CHAR, dd);
            transmitString("\r\n");
        }
        
        //transmitHex(CHAR, dd);
        //transmitHex(CHAR, (addr-1));
        //transmitString("\r\n");
    }
    
    transmitString("done MRAM\r\n");
    */
    
    
    PORTA = RW;
    DDRC = 0xFF;
    
    transmitString("\r\nSRAM\r\n");
    
    /*
    // check the RAM
    unsigned int offset, a;
    offset = 0;
    // c rom
    for (a = 0xc000; a < 0xd000; a++)
    {
        data = pgm_read_byte_near(crom_basic2 + offset);
        dd = get_data2(a);
        offset++;
        //put_data(a, data);
        
        if (dd != data)
        {
            transmitString("mismatch at addr: ");
            transmitHex(INT, a);
            transmitHex(CHAR, data);
            transmitHex(CHAR, dd);
            transmitString("\r\n");
        }
    }
    transmitString("done C rom\r\n");
    
    offset = 0;
    // d rom
    for (a = 0xd000; a < 0xe000; a++)
    {
        data = pgm_read_byte_near(drom_basic2 + offset);
        dd = get_data2(a);
        offset++;
        //put_data(a, data);
        
        if (dd != data)
        {
            transmitString("mismatch at addr: ");
            transmitHex(INT, a);
            transmitHex(CHAR, data);
            transmitHex(CHAR, dd);
            transmitString("\r\n");
        }
    }
    
    transmitString("done D rom\r\n");
     
    offset = 0;
    // e rom
    for (a = 0xe000; a < 0xe800; a++)
    {
        data = pgm_read_byte_near(erom_basic2 + offset);
        dd = get_data2(a);
        offset++;
        //put_data(a, data);
        
        if (dd != data)
        {
            transmitString("mismatch at addr: ");
            transmitHex(INT, a);
            transmitHex(CHAR, data);
            transmitHex(CHAR, dd);
            transmitString("\r\n");
        }
    }
    
    transmitString("done E rom\r\n");
    
    //offset = 0;
    a = 0xf000;
    for (offset = 0; offset < 4096; offset++)
    {
        data = pgm_read_byte_near(f_rom_basic2 + offset);
        dd = get_data2(a);
        //put_data(a, data);
        a++;
        
        if (dd != data)
        {
            transmitString("mismatch at addr: ");
            transmitHex(INT, a);
            transmitHex(CHAR, data);
            transmitHex(CHAR, dd);
            transmitString("\r\n");
        }
    }
    
    transmitString("done F rom\r\n");
    */
    checkROM(switchsetting);
    
    
    }
    
    while(1){}
#endif
    
    // release the lines
    //DDRC = MRAM_SEL | ADDREN;
    DDRC = ADDREN;
    PORTC = 0x00;
    DDRD = 0x00;
    DDRB = 0x00;

    //PORTA = 0xFF;
    PORTA = 0x01;
    DDRA = 0x01;
    
    uart0_init(255);
                
    // display switch status
    transmitString("Switch 1 ");
    if (sw4 == 0x00)
    {
        transmitString("OFF");
    }
    else {
        transmitString("ON");
    }
    transmitString("\r\n");
    
    transmitString("Switch 2 ");
    if (sw3 == 0x00)
    {
        transmitString("OFF");
    }
    else {
        transmitString("ON");
    }
    transmitString("\r\n");
    
    transmitString("Switch 3 ");
    if (sw1 == 0x00)
    {
        transmitString("OFF");
    }
    else 
    {
        transmitString("ON");
    }
    transmitString("\r\n");
    
    unsigned int tt;
    unsigned int done_diagnostic;
    unsigned int option;
    int diag_start_addr;
    int diag_end_addr;
    
    // display the name of this set
    for (tt = 0; tt < 10; tt++)
    {
        transmitByte(setname[tt]);
    }
    transmitString("\r\n");
    
    while(1)
    {
        transmitString("PETvet ready.\r\n");
        transmitString("Press any key to halt CPU and enter diagnostic mode.\r\n");
        data = receiveByte();
        
        // lower the RDY signal to halt the CPU
        DDRC = ADDREN | RDY;
        done_diagnostic = 0;
        
        _delay_loop_2(65535);
        
        transmitString("Halting CPU..");
        
        for (tt = 0; tt < 10; tt++)
        {
            _delay_loop_2(65535);
        }
        
        PORTC = ADDREN;
        
        // control the address lines
        DDRD = 0xFF;
        DDRC = ADDREN | RDY | 0xF0;
        DDRA = 0xFD;
        
        // read out video ram and send over the serial port
        unsigned int a;
        unsigned int ll;
        
        
        while (done_diagnostic == 0)
        {
            display_menu();
            option = get_diag_option();
            
            if (option == MEM_RANGE)
            {
                transmitString("Display Memory Range\r\n");
                display_memory_range();
            }
            else if (option == MEM_SCREEN)
            {
                transmitString("Display Screen Memory\r\n");
                display_screen_memory();
            }
            else if (option == EXIT)
            {
                transmitString("Exiting Diagnostic mode..\r\n");
                done_diagnostic = 1;
            }
        }
        
        /*
        ll = 0;
        transmitString("\r\n");
        for (a = 32768; a < 33768; a++)
        {
            data = get_data(a);
            if (data < 0x20)
            {
                transmitByte(data + 0x40);
            }
            else
            {
                transmitByte(data);
            }

            
            ll++;
            if (ll >= 40)
            {
                transmitString("\r\n");
                ll = 0;
            }
        }
        */
        
        DDRD = 0x00;
        DDRA = MRAM_WE;
        
        for (tt = 0; tt < 10; tt++)
        {
            _delay_loop_2(65535);
        }
        
        PORTC = 0x00;
        DDRC = ADDREN;
        
    }
}

void display_menu()
{
    // show menu of options for PETvet diagnostic mode
    transmitString("PETvet Diagnostic Mode\r\n");
    transmitString("1. Display Memory Range\r\n");
    transmitString("2. Display PET Screen Memory\r\n");
    transmitString("3. Exit Diagnostic Mode\r\n");
}

unsigned char get_diag_option()
{
    unsigned char input;
    unsigned char got_input;

    got_input = 0;

    while(got_input == 0)
    {
        // get input for option
        transmitString("Enter selection: ");
        
        // get input
        input = receiveByte();
        
        if (input >= '1' && input <= '3')
        {
            input = (input - '1') + 1;
            return input;
        }
        
        transmitString("\r\n");
    }
}

unsigned int get_uint_val()
{
    // keep getting characters until we get a carriage return
    unsigned char buff[20];
    unsigned char done;
    unsigned char pos;
    unsigned char input;
    unsigned int value;
    
    done = 0;
    pos = 0;
    while(done == 0)
    {
        // get character
        input = receiveByte();
        
        // are we done?
        if (input == 0x0d)
        {
            transmitString("\r\n");
        
            buff[pos] = 0;
            done = 1;
        }
        else 
        {
            // echo character back
            transmitByte(input);
            buff[pos] = input;
            pos++;
        }
    }
    
    // convert to a value
    value = (unsigned int) atoi(buff);
    
    /*
    transmitString("you entered: ");
    transmitString(buff);
    transmitString("\r\n");
    transmitHex(INT, value);
    transmitString("\r\n");
    */
    
    return value;
}

void display_screen_memory()
{
    unsigned char ll;
    unsigned int a;
    unsigned char data;
    
    ll = 0;
    transmitString("\r\n");
    for (a = 32768; a < 33768; a++)
    {
        data = get_data(a);
        if (data < 0x20)
        {
            transmitByte(data + 0x40);
        }
        else
        {
            transmitByte(data);
        }

        
        ll++;
        if (ll >= 40)
        {
            transmitString("\r\n");
            ll = 0;
        }
    }
}

void display_memory_range()
{
    unsigned int pos,p;
    unsigned int curr_addr;
    unsigned int addrstart_align;
    unsigned int addrstart, addrend;
    unsigned char data;
    char buff[20];
    // get start address
    transmitString("Enter start address (decimal): ");
    addrstart = get_uint_val();
    
    transmitString("Enter end address (decimal): ");
    addrend = get_uint_val();
    
    pos = addrstart % 8;
    addrstart_align = addrstart - pos;
    //curr_addr = addrstart_align;
    curr_addr = addrstart;
    
    while (curr_addr <= addrend)
    {
        // output current address
        //sprintf(buff, "%d\t:", curr_addr);
        transmitHex(INT, curr_addr);
        transmitString(":");
        //transmitString(buff);
        for (p = 0; p < 8; p++)
        {
            // get current data
            data = get_data(curr_addr);
            
            // output current data
            transmitHex(CHAR, data);
            transmitString("\t");
            
            curr_addr++;
        }
        transmitString("\r\n");
    }
    
}
