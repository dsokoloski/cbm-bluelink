//
// Custom basic4 replacement code.
//
// Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#include <conio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "insults.h"

//static int c = 0;
//static int i = 0;

static const char intro1[] = "*** commodore basic 4.0 ***\r\n";
//static const char intro2[] = "*** arbiter4 1.4 active ***\r\n\r\n";
static const char intro3[] = " 32768 bytes free\r\n\r\n";
//static const char intro3[] = " 31743 bytes free\r\n\r\n";
static const char ready[] = "ready.\r\n";

//#define CURS_Y              0xD8
//#define CURS_X              0xC6

#define POKE(addr,val)     (*(unsigned char*) (addr) = (val))
#define POKEW(addr,val)    (*(unsigned*) (addr) = (val))
#define PEEK(addr)         (*(unsigned char*) (addr))
#define PEEKW(addr)        (*(unsigned*) (addr))

#define cscroll() __asm__ ("jsr $E3E8");

#define cputc_scroll(c) \
    if (wherex() == 79 && wherey() == 24) { \
        cscroll(); gotoxy(0, 24); \
    } \
    cputc(c);

#define cputc_newline() \
    if (wherey() == 24) { \
        cscroll(); \
        gotoxy(0, 24); \
        cputc((char)'\r'); \
    } \
    else { \
        cputc((char)'\r'); \
        cputc((char)'\n'); \
    } \

void human_type(const char *s)
{
    clock_t goal;

    while (*s) {
        cputc_scroll(*s++);

        // Busy wait up to 100ms * 3...
        goal = clock() + ((clock_t)rand() % 3) * (CLOCKS_PER_SEC / 10);
        while ((long)(goal - clock()) > 0);
    }

    cputc_newline();
}

void main(void)
{
    clrscr();
    cursor(1);

    cputs(intro1);
//    cputs(intro2);
    cputs(intro3);
    cputs(ready);

    srand(42);
   
    sleep(1);
//    clrscr();

    for ( ;; ) {
        human_type(insult[rand() % 28]);
        sleep((rand() % 3) + 1);
#if 0
        c = (int)cgetc();
        cputc((char)c);

        if (c == '\n') {
            cprintf("\r\n\r[%hhu] %s\n\r",
               PEEK(CURS_Y), insult[rand() % 28]);
            cputs(ready);
//            if (PEEK(CURS_Y) >= 25) {
 //               cscroll();
  //              POKE(CURS_Y, 24);
   //         }
        }
#endif
    }
}

/* vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
 */
