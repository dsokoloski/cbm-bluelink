;
; Customized "romable" startup code for cc65 (PET version)
;

	.export	      	_exit
        .export         __STARTUP__ : absolute = 1      ; Mark as startup
	.import		initlib, donelib, callirq
       	.import	       	zerobss, push0
	.import		_main
        .import         CLRCH, BSOUT
	.import		__INTERRUPTOR_COUNT__
	.import		__DATA_LOAD__, __DATA_RUN__, __DATA_SIZE__
	.import		__RAM_START__, __RAM_SIZE__
	.import		__STACKSIZE__
	.import		copydata
	.importzp       ST

        .include        "zeropage.inc"
	.include	"pet.inc"
	.include	"../cbm/cbm.inc"

; Missing zero-page data length
; TODO: Should be set to the application's ZP start address
;ZP_MAGICSZ	:= $55

;.segment	"RODATA"

; Missing zero-page values

;_zp_magic:
;	.byte $4c,$73,$c3,$22,$22,$00,$00,$ff,$00,$00,$00,$00,$00,$00,$00,$00
;	.byte $00,$00,$80,$16,$13,$00,$08,$12,$b3,$00,$00,$00,$00,$00,$00,$12
;	.byte $b3,$e9,$ce,$00,$00,$00,$00,$00,$01,$04,$03,$04,$03,$04,$03,$04
;	.byte $00,$80,$00,$00,$00,$80,$00,$00,$00,$00,$00,$00,$00,$00,$00,$04
;	.byte $ff,$ff,$ff,$ff,$24,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff
;	.byte $03,$4c,$ff,$00,$ff

; ------------------------------------------------------------------------
; Startup code

.segment       	"STARTUP"

Start:
	
; Fill in missing zero-page values
;	ldy     #0
;L0:	ldx     _zp_magic,y
;	stx     $00,y
;	iny
;	cpy     #<ZP_MAGICSZ
;	bne     L0

; Copy DATA_LOAD to DATA_RUN
;	ldy     #0
;L1:	lda     __DATA_LOAD__,y
;	sta     __DATA_RUN__,y
;	iny
;	cpy     #<__DATA_SIZE__
;	bne     L1

	jsr	copydata

; Switch to second charset. The routine that is called by BSOUT to switch the
; character set will use FNLEN as temporary storage - YUCK! Since the
; initmainargs routine, which parses the command line for arguments needs this
; information, we need to save and restore it here.
; Thanks to Stefan Haubenthal for this information!

        lda     FNLEN
        pha                     ; Save FNLEN
	lda	#14
;	sta	$E84C		; See PET FAQ
	jsr	BSOUT
        pla
        sta     FNLEN           ; Restore FNLEN

; Clear the BSS data

	jsr	zerobss

; Save system stuff and setup the stack

;       	tsx
;       	stx    	spsave 		; Save the system stack ptr

	lda     #<(__RAM_START__ + __RAM_SIZE__ + __STACKSIZE__)
	sta     sp
	lda     #>(__RAM_START__ + __RAM_SIZE__ + __STACKSIZE__)
	sta     sp+1            ; Set argument stack ptr

;	lda    	MEMSIZE
;	sta	sp
;	lda	MEMSIZE+1
;       	sta	sp+1   	 	; Set argument stack ptr

; If we have IRQ functions, chain our stub into the IRQ vector

	lda	#<__INTERRUPTOR_COUNT__
	beq	NoIRQ1
	lda	IRQVec
	ldx	IRQVec+1
	sta	IRQInd+1
	stx	IRQInd+2
	lda	#<IRQStub
	ldx	#>IRQStub
	sei
	sta	IRQVec
	stx	IRQVec+1
	cli

; Call module constructors

NoIRQ1: jsr     initlib

; Push arguments and call main()

_main_loop:
        jmp	_main
; Should never get here...
_exit:
	jmp	_main_loop

; ------------------------------------------------------------------------
; The IRQ vector jumps here, if condes routines are defined with type 2.

IRQStub:
	cld    				; Just to be sure
	jsr    	callirq                 ; Call the functions
	jmp    	IRQInd			; Jump to the saved IRQ vector

; ------------------------------------------------------------------------
; Data

.data

IRQInd: jmp	$0000

.segment        "ZPSAVE"

zpsave:	.res	zpspace

.bss

spsave:	.res	1
mmusave:.res	1

