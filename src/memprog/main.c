// ROM reader, NVSRAM writer
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h> 
#include <util/delay.h>
#include <util/atomic.h>

#include "ports.h"
#include "uart.h"
#include "util.h"
#include "xmodem.h"

#include "edit4b80_patched.h"
//#include "edit4b80.h"
//#include "arbiter4.h"

static uint8_t buffer[XM_DATA_SIZE];

void send_memory(uint16_t length)
{
    uint16_t addr;
    uint8_t i = 0, block = 1;

    // Enable input on data lines
    DATA_07_DDR = 0;

    LED1_PORT |= _BV(LED1_BIT);

    if (xmodem_send_start(0) != 0) {
        xmodem_send_cancel(0);
        goto __send_memory_exit;
    }

    // Transfer loop...
    for (addr = 0; addr < length; addr++) {
        uint8_t byte = 0;

        ADDR_07_PORT = (uint8_t)addr;

        if (addr & 0x100)
            ADDR_8_PORT |= _BV(ADDR_8_BIT);
        else
            ADDR_8_PORT &= ~_BV(ADDR_8_BIT);
        if (addr & 0x200)
            ADDR_9_PORT |= _BV(ADDR_9_BIT);
        else
            ADDR_9_PORT &= ~_BV(ADDR_9_BIT);
        if (addr & 0x400)
            ADDR_10_PORT |= _BV(ADDR_10_BIT);
        else
            ADDR_10_PORT &= ~_BV(ADDR_10_BIT);
        if (addr & 0x800)
            ADDR_11_PORT |= _BV(ADDR_11_BIT);
        else
            ADDR_11_PORT &= ~_BV(ADDR_11_BIT);

        __asm__("nop    \n\t");
        __asm__("nop    \n\t");
        __asm__("nop    \n\t");
        __asm__("nop    \n\t");
        CSEL_PORT &= ~_BV(CSEL_BIT);
        OSEL_PORT &= ~_BV(OSEL_BIT);
        __asm__("nop    \n\t");
        __asm__("nop    \n\t");
        __asm__("nop    \n\t");
        __asm__("nop    \n\t");

        byte = DATA_07_PIN;

        CSEL_PORT |= _BV(CSEL_BIT);
        OSEL_PORT |= _BV(OSEL_BIT);

        buffer[i++] = byte;

        if (i == XM_DATA_SIZE) {
            LED1_PORT ^= _BV(LED1_BIT);
            if (xmodem_send_block(0, block++, i, buffer) != 0) {
                xmodem_send_cancel(0);
                goto __send_memory_exit;
            }
            i = 0;
        }
    }

    if (i > 0 && i != XM_DATA_SIZE)
        xmodem_send_block(0, block, i, buffer);

    xmodem_send_end(0);
__send_memory_exit:
    LED1_PORT &= ~_BV(LED1_BIT);
}

void write_memory(uint16_t length, const uint8_t *data)
{
    uint16_t addr;

    // Enable output on data lines
    DATA_07_DDR |= (
            _BV(DATA_0_BIT) | _BV(DATA_1_BIT) | _BV(DATA_2_BIT) |
            _BV(DATA_3_BIT) | _BV(DATA_4_BIT) | _BV(DATA_5_BIT) |
            _BV(DATA_6_BIT) | _BV(DATA_7_BIT)
    );

    LED1_PORT |= _BV(LED1_BIT);

    CSEL_PORT &= ~_BV(CSEL_BIT);
    __asm__("nop    \n\t");
    __asm__("nop    \n\t");
    __asm__("nop    \n\t");
    __asm__("nop    \n\t");

    for (addr = 0; addr < length; addr++) {
        ADDR_07_PORT = (uint8_t)addr;

        if (addr & 0x100)
            ADDR_8_PORT |= _BV(ADDR_8_BIT);
        else
            ADDR_8_PORT &= ~_BV(ADDR_8_BIT);
        if (addr & 0x200)
            ADDR_9_PORT |= _BV(ADDR_9_BIT);
        else
            ADDR_9_PORT &= ~_BV(ADDR_9_BIT);
        if (addr & 0x400)
            ADDR_10_PORT |= _BV(ADDR_10_BIT);
        else
            ADDR_10_PORT &= ~_BV(ADDR_10_BIT);
        if (addr & 0x800)
            ADDR_11_PORT |= _BV(ADDR_11_BIT);
        else
            ADDR_11_PORT &= ~_BV(ADDR_11_BIT);

        DATA_07_PORT = pgm_read_byte(&data[addr]);
        //DATA_07_PORT = data[addr];

        __asm__("nop    \n\t");
        __asm__("nop    \n\t");
        __asm__("nop    \n\t");
        __asm__("nop    \n\t");
        WSEL_PORT &= ~_BV(WSEL_BIT);
        __asm__("nop    \n\t");
        __asm__("nop    \n\t");
        __asm__("nop    \n\t");
        __asm__("nop    \n\t");

        WSEL_PORT |= _BV(WSEL_BIT);

        LED1_PORT ^= _BV(LED1_BIT);
    }

    CSEL_PORT |= _BV(CSEL_BIT);
    LED1_PORT &= ~_BV(LED1_BIT);
}

int main(void)
{
    uint8_t byte;
    char bits[9] = { '-', '-', '-', '-', '-', '-', '-', '-', '\0' };

    // Copy and clear MCU status register
    byte = MCUSR; MCUSR = 0;

    // JTAG reset
    if (byte & _BV(JTRF)) bits[0] = 'j';
    // Watchdog reset
    if (byte & _BV(WDRF)) bits[1] = 'w';
    // Brown-out reset
    if (byte & _BV(BORF)) bits[2] = 'b';
    // External reset
    if (byte & _BV(EXTRF)) bits[3] = 'e';
    // Power-on reset
    if (byte & _BV(PORF)) bits[4] = 'p';

    // Disable global interrupts
    cli();

    // Ensure clock prescaler is off
    CLKPR = _BV(CLKPCE), CLKPR = 0;

    // Enable output on LED1 pin and turn it on
    LED1_DDR |= _BV(LED1_BIT);
    LED1_PORT |= _BV(LED1_BIT);

    // Enable output on chip select line and drive it high
    CSEL_DDR |= _BV(CSEL_BIT);
    CSEL_PORT |= _BV(CSEL_BIT);

    // Enable output on output select line and drive it high
    OSEL_DDR |= _BV(OSEL_BIT);
    OSEL_PORT |= _BV(OSEL_BIT);

    // Enable output on write select line and drive it high
    WSEL_DDR |= _BV(WSEL_BIT);
    WSEL_PORT |= _BV(WSEL_BIT);

    // Enable output on address lines and drive them low
    ADDR_07_DDR |= (
            _BV(ADDR_0_BIT) | _BV(ADDR_1_BIT) | _BV(ADDR_2_BIT) |
            _BV(ADDR_3_BIT) | _BV(ADDR_4_BIT) | _BV(ADDR_5_BIT) |
            _BV(ADDR_6_BIT) | _BV(ADDR_7_BIT)
    );
    ADDR_8_DDR |= _BV(ADDR_8_BIT);
    ADDR_9_DDR |= _BV(ADDR_9_BIT);
    ADDR_10_DDR |= _BV(ADDR_10_BIT);
    ADDR_11_DDR |= _BV(ADDR_11_BIT);

    ADDR_07_PORT = 0;
    ADDR_8_PORT &= ~_BV(ADDR_8_BIT);
    ADDR_9_PORT &= ~_BV(ADDR_9_BIT);
    ADDR_10_PORT &= ~_BV(ADDR_10_BIT);
    ADDR_11_PORT &= ~_BV(ADDR_11_BIT);

    // Set UART0 baud and bits for debug output
    uart_enable(0, UART0_BAUD);

    // Hello, world!
    uart_write_string(0, "\r\nHello, World!\r\n");
    uart_write_string(0, "MCUSR [");
    uart_write_string(0, bits);
    uart_write_string(0, "]\r\n");

    // Enable interrupts
    sei();

    _delay_ms(2000);

    // Application loop...
    for ( ;; ) {
        //write_memory(arbiter4_len, arbiter4);
        //send_memory(arbiter4_len);

        write_memory(edit4b80_len, edit4b80);
        send_memory(edit4b80_len);

        //send_memory(2048);
        //send_memory(4096);

        _delay_ms(100);
    }

    return 0;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
