// Utilitiy macros and routines
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#ifndef _UTIL_H
#define _UTIL_H

uint8_t reverse_bits(uint8_t byte);

#endif // _UTIL_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
