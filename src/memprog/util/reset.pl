#!/usr/bin/perl -w

use Device::SerialPort;

if ($#ARGV == -1) {
	$device = "/dev/ttyUSB0";
} else {
	($device)=@ARGV;
}

$sp = new Device::SerialPort($device) || die "Error opening $device: $!\n";
$sp->pulse_dtr_on(100);
$sp->pulse_rts_off(100);

