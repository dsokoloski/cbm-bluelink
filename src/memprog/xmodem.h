// XMODEM routines
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#ifndef _XMODEM_H
#define _XMODEM_H

#define XM_SOH          0x01    // Start of heading
#define XM_EOT          0x04    // End of transfer
#define XM_ACK          0x06    // Acknowledge
#define XM_NAK          0x15    // Negative acknowledge
#define XM_CAN          0x18    // Cancel
#define XM_SUB          0x1A    // Substitute (pad character)
#define XM_DATA_SIZE    0x80    // 128 bytes
                        // 1 byte prefix, 2 bytes block, 2 byte checksum
#define XM_PACKET_SIZE  (XM_DATA_SIZE + 0x04)
//#define XM_PACKET_SIZE  (XM_DATA_SIZE + 0x05)

#define XM_ATTEMPTS     10      // Number of attempts to send a block

uint8_t xmodem_send_start(uint8_t un);
uint8_t xmodem_send_block(uint8_t un,
    uint8_t block, uint8_t length, const uint8_t *data);
void xmodem_send_end(uint8_t un);
void xmodem_send_cancel(uint8_t un);
uint16_t xmodem_checksum(uint8_t *data);

#endif // _XMODEM_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
