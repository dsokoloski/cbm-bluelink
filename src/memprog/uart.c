// UART routines
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#include <stdint.h>

#include <avr/io.h>
#include <avr/power.h>

#include "uart.h"

void uart_enable(uint8_t un, uint32_t baud)
{
    switch (un) {
    case 0:
        power_usart0_enable();

        UBRR0L = (uint8_t)(F_CPU / (baud * 16UL) - 1UL);
        UBRR0H = (F_CPU / (baud * 16L) - 1) >> 8;
        UCSR0C = ((1 << UCSZ01) | (1 << UCSZ00));
        UCSR0B = ((1 << RXEN0) | (1 << TXEN0));

        break;
    case 1:
        power_usart1_enable();

        UBRR1L = (uint8_t)(F_CPU / (baud * 16UL) - 1UL);
        UBRR1H = (F_CPU / (baud * 16UL) - 1) >> 8;
        UCSR1C = ((1 << UCSZ11) | (1 << UCSZ10));
        UCSR1B = ((1 << RXEN1) | (1 << TXEN1));

        break;
    }
}

void uart_disable(uint8_t un)
{
    switch (un) {
    case 0:
        power_usart0_disable();

        break;
    case 1:
        power_usart1_disable();

        break;
    }
}

uint8_t uart_read(uint8_t un, uint8_t *byte)
{
    uint8_t status = 0x00;

    switch (un) {
    case 0:
        if ((UCSR0A & (1 << RXC0))) {
            status = UCSR0A;
            *byte = UDR0;
        }

        break;
    case 1:
        if ((UCSR1A & (1 << RXC1))) {
            status = UCSR1A;
            *byte = UDR1;
        }

        break;
    }

    return status;
}

uint8_t uart_read_wait(uint8_t un, uint8_t *byte)
{
    switch (un) {
    case 0:
        while (!(UCSR0A & (1 << RXC0)));
        return uart_read(un, byte);

        break;
    case 1:
        while (!(UCSR1A & (1 << RXC1)));
        return uart_read(un, byte);

        break;
    }

    return 0x00;
}

uint8_t uart_read_ready(uint8_t un)
{
    switch (un) {
    case 0:
        return (UCSR0A & (1 << RXC0));

    case 1:
        return (UCSR1A & (1 << RXC1));
    }

    return 0x00;
}

void uart_write(uint8_t un, uint8_t byte)
{
    switch (un) {
    case 0:
        loop_until_bit_is_set(UCSR0A, UDRE0);
        UDR0 = byte;

        break;
    case 1:
        loop_until_bit_is_set(UCSR1A, UDRE1);
        UDR1 = byte;

        break;
    }
}

void uart_write_data(uint8_t un, long length, const uint8_t *data)
{
    long i;

    switch (un) {
    case 0:
        for (i = 0; i < length; i++) {
            loop_until_bit_is_set(UCSR0A, UDRE0);
            UDR0 = data[i];
        }

        break;
    case 1:
        for (i = 0; i < length; i++) {
            loop_until_bit_is_set(UCSR1A, UDRE1);
            UDR1 = data[i];
        }

        break;
    }
}

void uart_write_string(uint8_t un, char *s)
{
    while (*s) uart_write(un, *s++);
}

void uart_write_binchar(uint8_t un, uint8_t byte)
{
    for (int i = 7; i >= 0; i--) {
        if ((byte & (1 << (uint8_t)i)))
            uart_write(un, '1');
        else
            uart_write(un, '0');
    }
}

void uart_write_binchar_inv(uint8_t un, uint8_t byte)
{
    for (int i = 7; i >= 0; i--) {
        if ((byte & (1 << (uint8_t)i)))
            uart_write(un, '0');
        else
            uart_write(un, '1');
    }
}

void uart_write_binval(uint8_t un, uint8_t *byte, int length)
{
    for (int c = 0; c < length; c++) {
        for (int i = 7; i >= 0; i--) {
            if ((byte[c] & (1 << (uint8_t)i)))
                uart_write(un, '1');
            else
                uart_write(un, '0');
        }

        uart_write(un, ' ');
    }
}

void uart_flush(uint8_t un)
{
    uint8_t byte __attribute__((unused));

    switch (un) {
    case 0:
        while ((UCSR0A & (1 << RXC0)))
            byte = UCSR0A;

        break;
    case 1:
        while ((UCSR1A & (1 << RXC1)))
            byte = UCSR1A;

        break;
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
