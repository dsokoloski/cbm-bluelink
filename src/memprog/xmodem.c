// XMODEM routines
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#include <stdint.h>

#include "xmodem.h"
#include "uart.h"

static uint8_t xmodem_packet[XM_PACKET_SIZE];

uint8_t xmodem_send_start(uint8_t un)
{
    uint8_t byte = 0, attempts = 0;

    uart_write_string(un, "\r\nStart XMODEM receive...\r\n");
    uart_flush(un);

    for (attempts = 0; attempts < XM_ATTEMPTS; attempts++) {
        uart_read_wait(un, &byte);

        if (byte == XM_NAK) break;
    }

    if (attempts == XM_ATTEMPTS) {
        uart_write_string(un, "\r\nTransfer aborted.\r\n");
        return 1;
    }

    return 0;
}

uint8_t xmodem_send_block(uint8_t un,
    uint8_t block, uint8_t length, const uint8_t *data)
{
    uint8_t i, attempts = 0;
    //uint16_t checksum;
    uint8_t checksum = 0;

    if (length > XM_DATA_SIZE) return 1;

    xmodem_packet[0] = XM_SOH;
    xmodem_packet[1] = block;
    xmodem_packet[2] = ~block;

    for (i = 0; i < length; i++) {
        xmodem_packet[3 + i] = data[i];
        checksum += data[i];
    }

    for ( ; i < XM_DATA_SIZE; i++) {
        xmodem_packet[3 + i] = XM_SUB;
        checksum += XM_SUB;
    }

    xmodem_packet[3 + i] = checksum;
#if 0
    checksum = xmodem_checksum(xmodem_packet);
    xmodem_packet[i] = checksum << 8;
    xmodem_packet[i + 1] = xmodem_packet[i] | checksum;
#endif
    for (attempts = 0; attempts < XM_ATTEMPTS; attempts++) {
        uart_write_data(un, XM_PACKET_SIZE, xmodem_packet);
        uart_read_wait(un, &i);

        switch (i) {
        case XM_ACK:
            return 0;

        case XM_CAN:
            return 1;

        case XM_NAK:
        default:
            break;
        }
    }

    return 1;
}

void xmodem_send_end(uint8_t un)
{
    uint8_t attempts, byte;

    for (attempts = 0; attempts < XM_ATTEMPTS; attempts++) {
        uart_write(un, XM_EOT);
        uart_read_wait(un, &byte);
        if (byte == XM_ACK) break;
    }
}

void xmodem_send_cancel(uint8_t un)
{
    uart_write(un, XM_CAN);
    uart_write(un, XM_CAN);
    uart_write(un, XM_CAN);
    uart_flush(un);
}

uint16_t xmodem_checksum(uint8_t *data)
{
    uint8_t i;
    uint16_t crc = 0;
    uint8_t length = XM_DATA_SIZE;

    while (--length >= 0) {
        crc = crc ^ (int) *data++ << 8;
        i = 8;
        do {
            if (crc & 0x8000)
                crc = crc << 1 ^ 0x1021;
            else
                crc = crc << 1;
        } while(--i);
    }

    return crc;
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
