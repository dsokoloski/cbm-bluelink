#!/bin/bash

PORT=/dev/ttyUSB0

# Fuses
avrdude -c buspirate -p m644p -P $PORT -b 115200 -U lfuse:w:0xE2:m -U hfuse:w:0xDC:m -U efuse:w:0xFD:m

# Bootloader
avrdude -c buspirate -p m644p -P $PORT -b 115200 -U flash:w:ADABoot_644p.hex:i
