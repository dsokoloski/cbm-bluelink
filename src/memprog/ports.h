// Project port and address declarations
// Copyright (C) 2013 Darryl Sokoloski <dsokoloski@cbm-bluelink.ca>

#ifndef _PORTS_H
#define _PORTS_H

#define LED1_DDR        DDRB
#define LED1_PORT       PORTB
#define LED1_BIT        PB0

#define CSEL_DDR        DDRD
#define CSEL_PORT       PORTD
#define CSEL_PIN        PIND
#define CSEL_BIT        PD7

#define OSEL_DDR        DDRD
#define OSEL_PORT       PORTD
#define OSEL_PIN        PIND
#define OSEL_BIT        PD6

#define WSEL_DDR        DDRD
#define WSEL_PORT       PORTD
#define WSEL_PIN        PIND
#define WSEL_BIT        PD5

#define ADDR_07_DDR     DDRA
#define ADDR_07_PORT    PORTA
#define ADDR_07_PIN     PINA
#define ADDR_0_BIT      PA0
#define ADDR_1_BIT      PA1
#define ADDR_2_BIT      PA2
#define ADDR_3_BIT      PA3
#define ADDR_4_BIT      PA4
#define ADDR_5_BIT      PA5
#define ADDR_6_BIT      PA6
#define ADDR_7_BIT      PA7

#define ADDR_8_DDR      DDRB
#define ADDR_8_PORT     PORTB
#define ADDR_8_PIN      PINB
#define ADDR_8_BIT      PB1

#define ADDR_9_DDR      DDRB
#define ADDR_9_PORT     PORTB
#define ADDR_9_PIN      PINB
#define ADDR_9_BIT      PB2

#define ADDR_10_DDR     DDRB
#define ADDR_10_PORT    PORTB
#define ADDR_10_PIN     PINB
#define ADDR_10_BIT     PB3

#define ADDR_11_DDR     DDRB
#define ADDR_11_PORT    PORTB
#define ADDR_11_PIN     PINB
#define ADDR_11_BIT     PB4

#define DATA_07_DDR     DDRC
#define DATA_07_PORT    PORTC
#define DATA_07_PIN     PINC
#define DATA_0_BIT      PC0
#define DATA_1_BIT      PC1
#define DATA_2_BIT      PC2
#define DATA_3_BIT      PC3
#define DATA_4_BIT      PC4
#define DATA_5_BIT      PC5
#define DATA_6_BIT      PC6
#define DATA_7_BIT      PC7

#endif // _PORTS_H

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
