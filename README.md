CBM/BlueLink
============

IEEE-488 (GPIB) to FTDI and Bluetooth serial adapter hardware and software for vintage 8-bit CBM computers.

Overview
--------

CBM/BlueLink is a hardware and software solution which connects a CBM 8-bit computer to virtually any other device (PC/smartphone) via RS232/FTDI serial or Bluetooth.  Pseudo-devices can then be mapped to virtual disk drives, printers, or any other user-defined software device by loading plugins.

Hardware
--------

The current version of the adapter directly connects to the computer's IEEE-488 port.

Software
--------
